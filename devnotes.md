
== done

- fix slot naming issue
- ace slots reversed
- aufbrummen reversed
- send color/first turn errors to ALL clients
- missing spectators!
- interact sound (card move)
- interact error sound (invalid move)
- knock sound
- winner sound
- loser sound
- fix lobby error reset
- client: clear ready error on scene change
- add version matching to auth
- hide other option buttons if spectator selected
- auto assign joiner options
- fix: bring back headsup display
- unready on change
- drunterschieben buggy?
- fix chat: create player base class, put cmd/rpc/onclientstart there, make chatui client-only and monobehaviour!
- add sounds
- add card drop sound
- add your turn sound (cat)
- add icq chat sound (uh oh)
- knock & restart buttons should look the same.
- create actual ingame menu
- move chat ui to lobby, donotdestroy
- fix: spectators can knock
- fix: knock invisible/knock still possible with space
- add ddol soundmanager to make icq sound work in lobby
- chat: blabla is typing...
- spectators cant see hands from main stack
- fix server-only knocking
- lock taken dropdown options
- better stack move hand animation
- knock sometimes not in sync with the button?
- enable/disable/hide/show knock button
- show all available option icons
- fix volume fader?
- revert/redo and stage win -> allow knock
- server autorestart timeout: still knock if already won but middle not served
- wait 3 seconds to unready, then start

- save name/ip settings in playerprefs
- fix join flicker
- fix crashes when clients connect WHILE people get/are ready?!?
- no need for spectator ready: lobby autoready spectators
- fred's soundpack easteregg
- settings:
 - name
 - preferred color
 - master volume
- fix mousedragoffset
- better snap positioning
- can serve middle? ace to empty ace!
- redesign main menu, restrict naming (length & letters)
- fix klopfen nach return muss bleiben wenn der pickup davor nicht die mitte bedienen konnte
- fix status box readablility
- build proper popup system
- remove server turn message
- fix clientside lobby refresh on endgame
- renew lobby ui
- game > game stack slip (check: slip stack under 1 card)
- fix first assignment
- main/drop empty, but 13 not? -> end turn

== tocheck


== wip

- fix selbst aufbrummen wtf?

- both player called end turn subsequently -> call draw
- give up button?

- all menus: enter to confirm, escape to go back
- renew ingame ui
- renew chat ui
- clear disconnecting people from typing list!

- properly handle player re-connects with a timeout?
- add connection timeouts
- add table textures
- fix hand animation flicker
- catch port already in use

== todo gameplay

- esc / right-click returns card if possible
- spectatorknock
- optionally enable spectator highlights
- optionally show hands to spectators
- add knock timeouts
- add punishment for too many wrong knocks
- add punishment for too many "look under"s
- fehlersound fuer spieler wenn ausgeklopft

== todo menu, lobby, ui, audio, general

- add touch feature stuff for chat ui in android
- vote kick
- make server only ui more clean
- enable chat in server only desktop client (not dedi though, dedi has the console log)
- add dedi server console commands, maybe just show console in desktop client when server only?
- (add lobby settings)
- (add more player options)

==============
ui color table
https://flatuicolors.com/palette/cn

Prestige    #2f3542     scrollbar handles, player rows
gris        #57606f     ui outer
bay wharf   #747d8c     ui inner, disabled button
twinkle     #ced6e0     normal button
peace       #a4b0be     active button
city l      #dfe4ea     hover button
anti-flash white #f1f2f6 textfield/toggle
