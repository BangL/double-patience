
## Vorbereitung
_[Nur zur Erklärung, erfolgt automatisch]_  
Jeder der beiden einander gegenübersitzenden Spieler nimmt ein Spiel Whistkarten
(das ist ein Paket französischer Karten zu 52 Blatt), mischt es, und zählt verdeckt die ersten 13 Karten ab.
Er legt die abgezählten Karten, nun offen, als _Ersatzpäckchen_ (Schikanös-Päckchen) rechts,
vor sich auf den Tisch. Die übrigen Karten links, verdeckt.
Dazwischen bleibt genug Platz für den _Ablegestoß_.

Die folgenden vier Karten werden, vor dem ersten Spielzug, vom verdeckten Stapel des ersten Spielers gezogen und in einer Spalte untereinander offen davor gelegt.
Analog dazu wird, bevor der zweite Spieler beginnt, genau das Selbe auf der gegenüberliegenden Seite aufgebaut.
Zwischen den beiden Spalten muss Platz für die acht Asse bleiben (2x4).

## Spielverlauf
Der erste Spieler zieht die übrigen Karten seiner 3 Stapel, eine nach der anderen ab, wobei er jedes Blatt entweder den auf die Asse in aufsteigender Folge zu bildenden Familien anzufügt,
oder sie in abwechselnder Farbe (Rot/Schwarz) und absteigender Folge auf die _Vorratspäckchen_ unterzubringen, welche auf den je vier Karten zu beiden Seiten der _Assreihe_ gebildet werden.

Karten, die weder auf die _Assreihe_ noch auf die acht _Vorratspäckchen_ passen, können auf das Ersatzpäckchen oder den angefangenen Stoß des Gegners gelegt werden, sobald sie in der Farbe passen.
Gleichgültig ob in auf- oder absteigender Folge.

Die erste Karte, die der erste Spieler nicht in dieser Weise unterbringen kann, legt er als Anfang zu seinem Stoß _(Ablegestoß)_ vor sich hin.
Es folgt der zweite Spieler, welcher ebenso verfährt.

Die Asse werden beim Fortgang des Spiels, sobald sie erscheinen, auf die für sie bestimmten Plätze gelegt und bilden die Grundkarten für Familien von aufsteigendem Wert, die mit den Königen schließen,
also ♥ A – ♥ 2 – ♥ 3 – ... – ♥ J – ♥ Q – ♥ K.

Jede erreichbare Karte muss auf die _Assreihe_ übertragen werden, sobald sie sich an diese anschließen kann.
Wird dies nicht getan, und der aktuelle Spieler macht einen anderen Spielzug bzw. nimmt eine andere Karte auf, kann der Gegenspieler klopfen, sollte er dieses bemerken.
Der klopfende Spieler muss daraufhin zeigen, womit die "Mitte" hätte "bedient" werden können.
Konnte der Beweis erfolgen, wird der letzte Spielzug zurückgenommen bzw. die aufgenommene Karte zurückgelegt, und der Spieler der geklopft hat, ist umgehend dran.

## Spielende
Es gewinnt derjenige, welcher zuerst mit seinen 3 Stapeln am Ende ist.
