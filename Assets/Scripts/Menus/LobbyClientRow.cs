﻿using Assets.Scripts.UIElements;
using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Menus {

    internal class LobbyClientRow : MonoBehaviour {
        private VisualElement parent;
        private VisualElement element;
        private DSRoomClient roomPlayer;

        public VisualElement background;
        public Label clientNameLabel;
        public Toggle firstToggle;
        public BetterEnumField backColorDropdown;
        public Image backColorImage;
        public BetterEnumField clientTypeDropdown;
        public Image clientTypeImage;
        public Image readyImage;

        public string backgroundName;
        public string clientNameLabelName;
        public string firstToggleName;
        public string backColorDropdownName;
        public string backColorImageName;
        public string clientTypeDropdownName;
        public string clientTypeImageName;
        public string readyImageName;

        private void Start() {
        }

        // bound to ui
        public void FirstChange(bool newValue) {
            if (!CanChange()) return;
            if (IsHost()) {
                roomPlayer.SetSyncedFirstTurn(newValue);
            } else {
                roomPlayer.CmdSetFirstTurn(newValue);
            }
        }

        // bound to ui
        public void ColorChange(BackColor newValue)
        {
            if (!CanChange()) return;
            if (IsHost())
            {
                roomPlayer.SetSyncedBackColor(newValue);
            } else
            {
                roomPlayer.CmdSetBackColor(newValue);
            }
        }

        // bound to ui
        public void TypeChange(BetterNetworkClientType newValue)
        {
            if (!CanChange()) return;
            if (IsHost())
            {
                roomPlayer.SetClientType(newValue);
            } else
            {
                roomPlayer.CmdSetClientType(newValue);
            }
        }

        internal void Init(VisualElement parent, DSRoomClient roomPlayer) {
            this.parent = parent;
            this.roomPlayer = roomPlayer;

            element = Resources.Load<VisualTreeAsset>("LobbyClient").Instantiate();

            background = element.Q<VisualElement>(backgroundName);
            clientNameLabel = element.Q<Label>(clientNameLabelName);
            firstToggle = element.Q<Toggle>(firstToggleName);
            backColorDropdown = element.Q<BetterEnumField>(backColorDropdownName);
            backColorImage = element.Q<Image>(backColorImageName);
            clientTypeDropdown = element.Q<BetterEnumField>(clientTypeDropdownName);
            clientTypeImage = element.Q<Image>(clientTypeImageName);
            readyImage = element.Q<Image>(readyImageName);

            parent.Add(element);

            firstToggle.RegisterValueChangedCallback((e) => FirstChange(e.newValue));
            backColorDropdown.RegisterValueChangedCallback((e) => ColorChange((BackColor)e.newValue));
            clientTypeDropdown.RegisterValueChangedCallback((e) => TypeChange((BetterNetworkClientType)e.newValue));

            clientNameLabel.text = roomPlayer.GetClientName();
            OnFirstChanged(roomPlayer.IsFirst());
            OnColorChanged(roomPlayer.GetBackColor());
            OnTypeChanged(roomPlayer.GetClientType());
            OnReadyChanged(false);
        }

        internal void OnFirstChanged(bool check) {
            firstToggle.value = check;
        }

        internal void OnColorChanged(BackColor color)
        {
            backColorImage.sprite = DSResourceManager.singleton.GetResource<Sprite>(color.ToString());
            backColorDropdown.value = color;
        }

        internal void OnTypeChanged(BetterNetworkClientType type)
        {
            clientTypeImage.sprite = DSResourceManager.singleton.GetResource<Sprite>(type.Equals(BetterNetworkClientType.Player) ? "player" : "spectator");
            clientTypeDropdown.value = type;
            background.style.backgroundColor = new StyleColor(type.Equals(BetterNetworkClientType.Player) ? DSColor.Prestige : DSColor.Gris);
        }

        internal void OnReadyChanged(bool ready) {
            readyImage.visible = ready && roomPlayer.GetClientType().Equals(BetterNetworkClientType.Player);
        }

        [Client]
        internal void OnLobbySettingsChanged(LobbyValidationResult validationResult, List<BackColor> usedColors) {
            bool canChange = CanChange();
            bool isPlayer = roomPlayer.GetClientType() == BetterNetworkClientType.Player;

            // enable/disable type dropdown
            clientTypeDropdown.SetEnabled(canChange);

            // enable/disable player option
            clientTypeDropdown.SetItemEnabled(BetterNetworkClientType.Player, roomPlayer.GetClientType().Equals(BetterNetworkClientType.Player) || validationResult.HasFlag(LobbyValidationResult.PlayerCountNotEnough));

            // enable/disable show/hide player controls
            firstToggle.visible = isPlayer;
            backColorImage.visible = isPlayer;
            backColorDropdown.visible = isPlayer;

            if (isPlayer) {
                firstToggle.SetEnabled(canChange && (roomPlayer.IsFirst() || (canChange && (validationResult.HasFlag(LobbyValidationResult.PlayerCountNotEnough) || !validationResult.HasFlag(LobbyValidationResult.FirstCountGood)))));

                backColorDropdown.SetEnabled(canChange);

                // enable/disable color dropdown options
                Enum.GetValues(typeof(BackColor)).Cast<BackColor>().ToList()
                    .ForEach(x => {
                        backColorDropdown.SetItemEnabled(x, roomPlayer.GetBackColor().Equals(x) || !usedColors.Contains(x));
                    });
            }

            // ready button
            if (roomPlayer.isLocalPlayer)
                LobbyUI.Instance.CanReady(isPlayer);
        }

        private bool IsHost() {
            return NetworkClient.localPlayer.GetComponent<DSRoomClient>().isServer;
        }

        private bool CanChange() {
            return NetworkClient.localPlayer != null
                && roomPlayer != null
                && (roomPlayer.isLocalPlayer || IsHost());
        }

        private void OnDestroy() {
            parent.Remove(element);
        }
    }
}