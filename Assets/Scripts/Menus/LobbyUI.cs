using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Menus {

    [DisallowMultipleComponent]
    [RequireComponent(typeof(UIDocument))]
    public class LobbyUI : NetworkBehaviour {
        internal static LobbyUI Instance;

        private ListView clientPanel;
        private VisualElement clientPanelContainer;
        private Label errorLabel;
        private Button backButton;
        private Button readyButton;

        public GameObject clientRowPrefab;
        public string clientPanelName;
        public string errorLabelName;
        public string backButtonName;
        public string readyButtonName;
        public UnityEngine.UI.Image backgroundImage;

        private void Awake() {
            if (Instance != null)
                Destroy(Instance.gameObject);
            Instance = this;
        }

        private void Start() {
            LoadResources();
            backgroundImage.sprite = DSResourceManager.singleton.GetResource<Sprite>(((TableTextures)PlayerPrefs.GetInt("TableTexture", (int)TableTextures.WoodTable)).ToString());

            VisualElement root = GetComponent<UIDocument>().rootVisualElement;

            clientPanel = root.Q<ListView>(clientPanelName);
            clientPanelContainer = clientPanel.Q("unity-content-container");
            errorLabel = root.Q<Label>(errorLabelName);

            backButton = root.Q<Button>(backButtonName);
            readyButton = root.Q<Button>(readyButtonName);

            backButton.clicked += OnBackButtonClicked;
            readyButton.clicked += OnReadyButtonClicked;

            if (clientCache.Count > 0) // late load for getting back from lobby
            {
                OnRoomClientListChanged(clientCache.Values);
                clientCache.Clear();
            }
        }

        internal void LoadResources() {
            // table textures
            foreach (TableTextures tableTexture in Enum.GetValues(typeof(TableTextures)))
            {
                string textureName = tableTexture.ToString();
                DSResourceManager.singleton.PrecacheResource<Sprite>(textureName, textureName);
            }
            // back sprites
            foreach (BackColor backColor in Enum.GetValues(typeof(BackColor))) {
                string backName = backColor.ToString();
                DSResourceManager.singleton.PrecacheResource<Sprite>("CardBacks/BackColor_" + backName, backName);
            }
            DSResourceManager.singleton.PrecacheResource<Sprite>("player", "player");
            DSResourceManager.singleton.PrecacheResource<Sprite>("spectator", "spectator");
        }

        private void Update() {
            // FIXME: this doesnt have to be on each frame, does it?

            // update back button text
            if (NetworkServer.active && NetworkClient.isConnected) {
                // Host
                backButton.text = "Stop Host";
            } else if (NetworkClient.isConnected) {
                // Client
                backButton.text = "Disconnect";
            } else if (NetworkServer.active) {
                // Server Only
                backButton.text = "Stop Server";
            }
        }

        private void OnBackButtonClicked() {
            // stop host if host mode
            if (NetworkServer.active && NetworkClient.isConnected) {
                DSNetworkRoomManager.singleton.StopHost();
            }
            // stop client if client-only
            else if (NetworkClient.isConnected) {
                DSNetworkRoomManager.singleton.StopClient();
            }
            // stop server if server-only
            else if (NetworkServer.active) {
                DSNetworkRoomManager.singleton.StopServer();
            }
        }

        private void OnReadyButtonClicked() {
            readyButton.SetEnabled(false); // prevent double exec

            if (NetworkClient.active && NetworkClient.localPlayer.TryGetComponent(out DSRoomClient client))
                client.CmdChangeReadyState(!client.readyToBegin);
        }

        internal void SetError(string err) {
            if (errorLabel != null)
                errorLabel.text = err ?? string.Empty;
        }

        private readonly Dictionary<string, LobbyClientRow> clientRows = new();
        private readonly Dictionary<string, DSRoomClient> clientCache = new();

        internal void OnRoomClientListChanged(IEnumerable<DSRoomClient> roomSlots) {

            // add new clients
            foreach (DSRoomClient roomPlayer in roomSlots) {
                if (roomPlayer != null) {
                    string playerName = roomPlayer.GetClientName();
                    if (clientPanelContainer == null) {
                        if (!clientCache.ContainsKey(playerName)) {
                            clientCache.Add(playerName, roomPlayer);
                        }
                    } else {
                        if (!clientRows.ContainsKey(playerName)) {
                            // client joined
                            GameObject rowObject = Instantiate(clientRowPrefab, transform, false);
                            LobbyClientRow playerRow = rowObject.GetComponent<LobbyClientRow>();
                            playerRow.Init(clientPanelContainer, roomPlayer);
                            clientRows.Add(playerName, playerRow);
                        }
                    }
                }
            }
            // remove clients that left
            foreach (string playerName in clientRows.Keys.ToList()) {
                if (roomSlots.Where((x) => x.GetClientName().Equals(playerName)).FirstOrDefault() == null) {
                    if (clientPanelContainer == null) {
                        if (!clientCache.ContainsKey(playerName)) {
                            clientCache.Remove(playerName);
                        }
                    } else {
                        // client left
                        LobbyClientRow row = clientRows[playerName];
                        clientRows.Remove(playerName);
                        Destroy(row.gameObject);
                    }
                }
            }
        }

        internal void OnRoomClientReadyStateChanged(string playerName, bool ready) {
            if (!clientRows.ContainsKey(playerName))
                return;

            if (NetworkClient.active && playerName.Equals(NetworkClient.localPlayer.GetComponent<DSRoomClient>().GetClientName())) {
                readyButton.text = ready ? "Unready" : "Ready";
                readyButton.SetEnabled(true);
            }

            clientRows[playerName].OnReadyChanged(ready);
        }

        internal void OnRoomClientTypeChanged(string playerName, BetterNetworkClientType newState) {
            if (!clientRows.ContainsKey(playerName))
                return;

            clientRows[playerName].OnTypeChanged(newState);
        }

        internal void OnRoomClientColorChanged(string playerName, BackColor newState) {
            if (!clientRows.ContainsKey(playerName))
                return;

            clientRows[playerName].OnColorChanged(newState);
        }

        internal void OnRoomClientFirstChanged(string playerName, bool newState) {
            if (!clientRows.ContainsKey(playerName))
                return;

            clientRows[playerName].OnFirstChanged(newState);
        }

        [ClientRpc]
        internal void OnLobbySettingsChanged() {
            // revalidate all rows
            (var result, var usedColors) = DSNetworkRoomManager.singleton.ValidateLobby();
            clientRows.Values.ToList().ForEach((x) => x.OnLobbySettingsChanged(result, usedColors));
        }

        [Client]
        internal void CanReady(bool isPlayer)
        {
            readyButton.SetEnabled(isPlayer);
        }
    }
}