﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

internal class MainMenu : MenuBase, IMenu {
    internal static MainMenu singleton;

    private string clientName;
    private BackColor prefBackColor;
    private bool prefFirst;
    private string networkAddress;
    private string errorMessage;
    private TableTextures tableTexture;

    public AudioMixer mixer;
    public Image backgroundImage;

    private void Awake() {
        if (singleton != null)
            Destroy(singleton.gameObject);
        singleton = this;
    }

    public override void Start() {
        base.Start();

        LoadPrefs();
        
        LoadResources();
        backgroundImage.sprite = DSResourceManager.singleton.GetResource<Sprite>(((TableTextures)PlayerPrefs.GetInt("TableTexture", (int)TableTextures.WoodTable)).ToString());

        StartPage();
    }

    internal void LoadResources()
    {
        // table textures
        foreach (TableTextures tableTexture in Enum.GetValues(typeof(TableTextures)))
        {
            string textureName = tableTexture.ToString();
            DSResourceManager.singleton.PrecacheResource<Sprite>(textureName, textureName);
        }
    }

    public List<MenuPage> GetPages() {
        return new() {
            new() { page = MainMenuPages.MainMenu, canShow = () => CanServe() },
            new() { page = MainMenuPages.JoinMenu },
            new() { page = MainMenuPages.HostMenu, canShow = () => CanServe() },
        };
    }

    public List<IMenuEntry> GetPage(Enum page) {
        return page switch {
            MainMenuPages.JoinMenu => GetStartPage(false),
            MainMenuPages.HostMenu => GetStartPage(true),
            MainMenuPages.SettingsMain => GetSettingsMain(),
            MainMenuPages.SettingsAudio => GetSettingsAudio(),
            MainMenuPages.ClientStatus => GetClientStatus(),
            MainMenuPages.HostStatus => GetHostStatus(),
            _ => GetMainPage(),
        };
    }

    #region Pages

    private List<IMenuEntry> GetMainPage() {
        return new() {
            new MenuHeader("Double-Solitaire"),
            new MenuSpacer(),

            GetErrorLabel(),
            // join game
            new MenuButton("Join Game", () => {
                SwitchPage(MainMenuPages.JoinMenu);
            }),
            // host game
            new MenuButton("Host Game", () => {
                SwitchPage(MainMenuPages.HostMenu);
            }, canShow: () => CanServe()),
            //// server only
            //new MenuButton("Server Only", () => {
            //    DSNetworkRoomManager.singleton.StartServer();
            //}, canShow: () => CanServe()),

            new MenuSpacer(),

            // settings
            new MenuButton("Settings", () => {
                SwitchPage(MainMenuPages.SettingsMain);
            }),
            // quit game
            GetQuitGameButton(),
        };
    }

    private List<IMenuEntry> GetStartPage(bool isHost) {
        List<IMenuEntry> result = new() {
            new MenuHeader(isHost ? "Host Game" : "Join Game"),

            new MenuSpacer(),

            GetErrorLabel(),
            // Client Name
            GetClientNameTextField(),
            // Network Address
            new MenuTextField("Address", 128,
                get: () => networkAddress,
                set: (e) => networkAddress = e.newValue,
                canShow: () => !isHost
            ),
            // Connect Button
            new MenuButton("Connect", () => {
                errorMessage = null;
                SavePrefs();
                DSNetworkRoomManager.singleton.StartClient();
                SwitchPage(MainMenuPages.ClientStatus);
            }, canShow: () => !isHost),
            // Start Button
            new MenuButton("Start", () => {
                errorMessage = null;
                SavePrefs();
                DSNetworkRoomManager.singleton.StartHost();
                SwitchPage(MainMenuPages.HostStatus);
            }, canShow: () => isHost),

            new MenuSpacer(),

            // back / quit game
            CanServe() ? GetBackButton(MainMenuPages.MainMenu) : GetQuitGameButton()
        };

        return result;
    }

    private List<IMenuEntry> GetClientStatus() {
        return new() {
            new MenuHeader("Join Game"),

            new MenuSpacer(),

            GetErrorLabel(),
            new MenuLabel($"Connecting to {DSNetworkRoomManager.singleton.networkAddress}..."),

            new MenuSpacer(),

            new MenuButton("Cancel", () => {
                errorMessage = null;
                DSNetworkRoomManager.singleton.StopClient();
                SwitchPage(MainMenuPages.JoinMenu);
            }, true),
        };
    }

    private List<IMenuEntry> GetHostStatus() {
        return new() {
            new MenuHeader("Host Game"),

            new MenuSpacer(),

            GetErrorLabel(),
            new MenuLabel($"Starting host..."),

            new MenuSpacer(),

            //new MenuButton("Cancel", () => DSNetworkRoomManager.singleton.StopHost()), // caused crashes, no need, just close when in
        };
    }

    private List<IMenuEntry> GetSettingsMain() {
        List<IMenuEntry> result = new() {
            new MenuHeader("Settings"),

            new MenuSpacer(),

            GetErrorLabel(),
            // Client Name
            GetClientNameTextField(),
            // pref first
            new MenuToggle("First",
                get: () => prefFirst,
                set: (e) => prefFirst = e.newValue
            ),
            // pref back color
            new MenuEnum("Back Color",
                get: () => prefBackColor,
                set: (e) => prefBackColor = (BackColor)e.newValue
            ),

            new MenuSpacer(),
            
            // table texture
            new MenuEnum("Table Texture",
                get: () => tableTexture,
                set: (e) => tableTexture = (TableTextures)e.newValue
            ),

            new MenuSpacer(),

            // audio menu
            new MenuButton("Audio", () => {
                SwitchPage(MainMenuPages.SettingsAudio);
            }),
            // back button
            GetBackButton(MainMenuPages.MainMenu, () => SavePrefs())
        };

        return result;
    }

    private List<IMenuEntry> GetSettingsAudio() {
        List<IMenuEntry> result = new() {
            new MenuHeader("Audio"),
            new MenuSpacer(),

            GetErrorLabel(),
            // master volume
            new MenuSlider("Master Volume", -80f, 0f,
                get: () => {
                    mixer.GetFloat("MasterVolume", out float master);
                    return master;
                },
                set: (e) => {
                    mixer.SetFloat("MasterVolume", e.newValue);
                    DSAudioManager.PlaySound(GameSound.Error);
                    PlayerPrefs.SetFloat("MasterVolume", e.newValue);
                }
            ),

            new MenuSpacer(),

            // back button
            GetBackButton(MainMenuPages.SettingsMain, () => PlayerPrefs.Save())
        };

        return result;
    }

    #endregion Pages

    #region Shared Controls

    internal MenuError GetErrorLabel() {
        return new MenuError(errorMessage);
    }

    private MenuTextField GetClientNameTextField() {
        return new MenuTextField("Name", 32,
            get: () => clientName,
            set: (e) => clientName = e.newValue
        );
    }

    private MenuButton GetBackButton(MainMenuPages backPage, Action before = null) {
        return new MenuButton("Back", () => {
            before?.Invoke();
            errorMessage = null;
            SwitchPage(backPage);
        }, true);
    }

    private MenuButton GetQuitGameButton() {
        return new MenuButton("Quit Game", () => {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }, true);
    }

    #endregion Shared Controls

    #region Helpers

    private bool CanServe() {
        return (Application.platform != RuntimePlatform.WebGLPlayer) && (Application.platform != RuntimePlatform.Android);
    }

    private void LoadPrefs() {
        clientName = PlayerPrefs.GetString("ClientName", "Player");
        prefBackColor = (BackColor)PlayerPrefs.GetInt("PrefBackColor", (int)BackColor.Black);
        prefFirst = PlayerPrefs.GetInt("PrefFirst", 1) == 1;
        networkAddress = PlayerPrefs.GetString("NetworkAddress", "localhost");
        tableTexture = (TableTextures)PlayerPrefs.GetInt("TableTexture", (int)TableTextures.WoodTable);
    }

    private void SavePrefs() {
        DSNetworkRoomManager manager = DSNetworkRoomManager.singleton;
        DSNetworkAuthenticator auth = manager.GetComponent<DSNetworkAuthenticator>();

        // set auth client name
        auth.SetClientName(clientName);
        PlayerPrefs.SetString("ClientName", clientName);

        // preferred back color
        auth.SetPrefBackColor(prefBackColor);
        PlayerPrefs.SetInt("PrefBackColor", (int)prefBackColor);

        // prefers first
        auth.SetPrefFirst(prefFirst);
        PlayerPrefs.SetInt("PrefFirst", prefFirst ? 1 : 0);

        // set address
        manager.networkAddress = networkAddress;
        PlayerPrefs.SetString("NetworkAddress", networkAddress);

        // set table texture
        backgroundImage.sprite = DSResourceManager.singleton.GetResource<Sprite>(tableTexture.ToString());
        PlayerPrefs.SetInt("TableTexture", (int)tableTexture);

        PlayerPrefs.Save();
    }

    internal void SetError(string message = null) {
        errorMessage = message;
        ReloadPage();
    }

    #endregion Helpers

    #region Konami

    private readonly List<string> keyStrokeHistory = new();
    private bool konamiTriggered = false;

    private void Update() {
        if (!konamiTriggered) {
            KeyCode keyPressed = DetectKeyPressed();
            AddKeyStrokeToHistory(keyPressed.ToString());
            if (GetKeyStrokeHistory().Equals("UpArrow,UpArrow,DownArrow,DownArrow,LeftArrow,RightArrow,LeftArrow,RightArrow,B,A")) {
                ClearKeyStrokeHistory();
                konamiTriggered = true;
                DSAudioManager.singleton.GoAlt();
            }
        }
    }

    private string GetKeyStrokeHistory() {
        return string.Join(",", keyStrokeHistory.ToArray());
    }

    private void ClearKeyStrokeHistory() {
        keyStrokeHistory.Clear();
    }

    private void AddKeyStrokeToHistory(string keyStroke) {
        if (!keyStroke.Equals("None")) {
            keyStrokeHistory.Add(keyStroke);
            if (keyStrokeHistory.Count > 10) {
                keyStrokeHistory.RemoveAt(0);
            }
        }
    }

    private KeyCode DetectKeyPressed() {
        foreach (KeyCode key in Enum.GetValues(typeof(KeyCode))) {
            if (Input.GetKeyDown(key)) {
                return key;
            }
        }
        return KeyCode.None;
    }

    #endregion Konami
}