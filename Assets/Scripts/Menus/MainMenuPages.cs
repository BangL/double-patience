﻿public enum MainMenuPages {
    MainMenu,
    JoinMenu,
    HostMenu,
    ClientStatus,
    HostStatus,
    SettingsMain,
    SettingsAudio,
}