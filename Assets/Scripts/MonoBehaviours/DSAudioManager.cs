using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class DSAudioManager : MonoBehaviour {
    internal static DSAudioManager singleton;

    [Header("Audio")]
    public AudioMixer mixer;

    public List<DSSound> sounds = new();
    public List<DSSound> soundsAlt = new();
    private readonly Dictionary<string, DSSound> indexedSounds = new();

    private void Awake() {
        if (singleton != null)
            Destroy(singleton.gameObject);
        singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        // index sounds
        foreach (DSSound sound in sounds)
            indexedSounds.Add(sound.gameObject.name, sound);

        // int master volume from user settings
        mixer.SetFloat("MasterVolume", PlayerPrefs.GetFloat("MasterVolume", 0f)); // get user settings
    }

    internal static void PlaySound(GameSound sound) {
        if (singleton.indexedSounds.ContainsKey(sound.ToString())) {
            Debug.Log($"play sound \"{sound}\"");
            singleton.indexedSounds[sound.ToString()].Play();
        } else {
            Debug.LogWarning("missing sound: " + sound.ToString());
        }
    }

    internal void GoAlt() {
        indexedSounds.Clear();

        foreach (DSSound sound in soundsAlt)
            indexedSounds.Add(sound.gameObject.name, sound);

        PlaySound(GameSound.YourTurn);
    }
}