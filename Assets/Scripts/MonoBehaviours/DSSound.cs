using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class DSSound : MonoBehaviour {
    private List<AudioSource> audioSources;

    private void Start() {
        audioSources = GetComponents<AudioSource>().ToList();
    }

    internal void Play() {
        if (audioSources != null) {
            if (audioSources.Count > 1) {
                System.Random random = new();
                int i = random.Next(1, audioSources.Count);
                audioSources[i - 1].Play();
            } else {
                audioSources[0].Play();
            }
        } else {
            Debug.LogWarning($"No audio sources ({name})");
        }
    }
}