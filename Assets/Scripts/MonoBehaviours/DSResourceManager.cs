using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Double-Solitaire/DS Resource Manager")]
[DisallowMultipleComponent]
public class DSResourceManager : MonoBehaviour {
    internal static DSResourceManager singleton;

    private readonly Dictionary<string, Object> resources = new();

    private void Awake() {
        singleton = this;
    }

    internal void PrecacheResource<T>(string path, string alias) where T : Object {
        T resource = Resources.Load<T>(path);
        resources.Add(alias, resource);
    }

    internal T GetResource<T>(string id) where T : Object {
        return (T)resources[id];
    }

    public static string GetCardSpriteAlias(Suit suit, Value value) {
        if (suit.Equals(Suit.Joker) && value.Equals(Value.Joker)) {
            return "Joker";
        }
        if (suit.Equals(Suit.Joker) && value.Equals(Value.JollyJoker)) {
            return "JollyJoker";
        }
        return suit.ToString() + ((int)value).ToString("D2");
    }
}