﻿using Mirror;
using UnityEngine;

/// <summary>
/// Used for the mouse click/hover stuff and also to show player mouse highlight positions
///
/// Each cardstack will have 2 of these!
/// 1 with isBottom false, 1 with isBottom true.
/// Only "Game" stacks will actually use both though.
/// </summary>
[AddComponentMenu("Double-Solitaire/DS Card Stack Collider")]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class DSCardStackColliderBehaviour : MonoBehaviour {

    /// <summary>
    /// used to hold the collider where dragging stops
    /// </summary>
    private static DSCardStackColliderBehaviour dragTarget;

    /// <summary>
    /// used to prevent OnMouseDrag triggering same event multiple times
    /// </summary>
    private bool dragging = false;

    private float dragStart = 0;

    public bool isBottomCollider;

    public DSCardStackBehaviour stack;

    private bool IsActive() {
        return GetComponent<BoxCollider2D>().enabled && NetworkClient.active;
    }

    private void OnMouseEnter() {
        if (!IsActive()) return;
        dragTarget = this;

        StartHighlight();
    }

    private void OnMouseExit() {
        if (!IsActive()) return;
        dragTarget = null;

        StopHighlight();
    }

    private void OnMouseDrag() {
        if (dragging || !IsActive()) return;
        dragging = true;
        dragStart = Time.time;

        DSIngameClient player = DSGameManager.GetLocalPlayer();
        if (!player.CanInteract()) return;

        if (!player.HasHand()) {
            // request to pickup card
            PickupCard(player);
        }
    }

    private void OnMouseUp() {
        if (!IsActive()) return;
        dragging = false;

        DSIngameClient player = DSGameManager.GetLocalPlayer();
        if (!player.CanInteract()) return;

        bool hasHand = player.HasHand();

        if (!hasHand && dragTarget != null && ((Time.time - dragStart) > DSTweakData.ANIMATION_SPEED)) {
            // request to pickup card
            dragTarget.PickupCard(player);
            //} else if (hasHand && dragTarget == null) {
            //    // clear hand
            //    DropCard(player);
        } else if (hasHand && dragTarget != null) {
            bool myTurn = DSGameManager.IsCurrentPlayer(player);
            bool isKnock = DSGameManager.singleton.knockMode;

            if (myTurn && !isKnock) {
                // request to drop card
                dragTarget.DropCard(player);
            } else if (!myTurn && isKnock) {
                // show knock reason
                dragTarget.ConfirmKnock(player);
            }
        }
    }

    private void PickupCard(DSIngameClient player) {
        stack.CmdPickupCard(player.gameObject, isBottomCollider);
        if (stack.GetStackType().HasFlag(StackType.PlayerMain)) {
            stack.CmdUnhighlight(false, player.GetChatColor(DSTweakData.HIGHLIGHT_ALPHA));
        }
    }

    private void DropCard(DSIngameClient player) {
        stack.CmdDropCard(player.gameObject, isBottomCollider);
    }

    private void ConfirmKnock(DSIngameClient player) {
        stack.CmdKnockConfirm(player.gameObject);
    }

    #region Highlighting

    private void StartHighlight() {
        DSIngameClient player = DSGameManager.GetLocalPlayer();

        if (!ValidateHighlight()) return;

        stack.CmdHighlight(isBottomCollider, player.GetChatColor(DSTweakData.HIGHLIGHT_ALPHA));
    }

    private void StopHighlight() {
        DSIngameClient player = DSGameManager.GetLocalPlayer();

        stack.CmdUnhighlight(isBottomCollider, player.GetChatColor(DSTweakData.HIGHLIGHT_ALPHA));
    }

    private bool ValidateHighlight() {
        DSIngameClient player = DSGameManager.GetLocalPlayer();
        if (!player.CanInteract()) return false;
        bool hasHand = player.HasHand();

        // cancel if invalid pickup
        if (!hasHand && !stack.ValidatePickup(player.gameObject, isBottomCollider, true)) return false;

        // cancel if invalid move
        if (hasHand && stack.ValidateDrop(player.gameObject, isBottomCollider, true).HasFlag(ValidateDropResult.Cancel)) return false;

        return true;
    }

    #endregion Highlighting
}