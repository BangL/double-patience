﻿using Assets.Scripts;
using Assets.Scripts.Menus;
using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[AddComponentMenu("Double-Solitaire/DS Room Client")]
public class DSRoomClient : BetterNetworkRoomClient, IDSClient {
    private DSAuthData authData;

    #region Sync Vars

    [SyncVar] private string syncedPlayerName;

    [SyncVar(hook = nameof(SyncedBackColorChanged))]
    private BackColor syncedBackColor = BackColor.Black;

    [SyncVar(hook = nameof(SyncedFirstTurnChanged))]
    private bool syncedFirstTurn = false;

    #endregion Sync Vars

    #region Server Setters

    [Server]
    internal void SetSyncedPlayerName(string playerName) {
        syncedPlayerName = playerName;
    }

    [Server]
    public override void OnServerClientTypeChanged() {
        DSNetworkRoomManager.singleton.OnLobbySettingsChanged();

        // auto assign player options
        if (GetClientType().Equals(BetterNetworkClientType.Player)) {
            // revalidate with player brought defaults, also get new color counts
            (LobbyValidationResult result, List<BackColor> usedBackColors) = DSNetworkRoomManager.singleton.ValidateLobby(this);

            // first?
            if (syncedFirstTurn && (result.HasFlag(LobbyValidationResult.FirstCountTooMany) || result.HasFlag(LobbyValidationResult.FirstCountGood)))
                SetSyncedFirstTurn(false);
            if (!syncedFirstTurn && result.HasFlag(LobbyValidationResult.FirstCountNotEnough))
                SetSyncedFirstTurn(true);

            // auto pick next unused color
            if (usedBackColors.Contains(syncedBackColor))
                SetSyncedBackColor(Enum.GetValues(typeof(BackColor)).Cast<BackColor>().FirstOrDefault((x) => !usedBackColors.Contains(x)));
        }
    }

    [Server]
    internal void SetSyncedBackColor(BackColor color) {
        syncedBackColor = color;
        DSNetworkRoomManager.singleton.OnLobbySettingsChanged();
    }

    [Server]
    internal void SetSyncedFirstTurn(bool value) {
        syncedFirstTurn = value;
        DSNetworkRoomManager.singleton.OnLobbySettingsChanged();
    }

    #endregion Server Setters

    #region Commands

    [Command]
    internal void CmdSetClientType(BetterNetworkClientType playerType) {
        SetClientType(playerType);
    }

    [Command]
    internal void CmdSetBackColor(BackColor color) {
        SetSyncedBackColor(color);
    }

    [Command]
    internal void CmdSetFirstTurn(bool value) {
        SetSyncedFirstTurn(value);
    }

    #endregion Commands

    #region Getters

    public string GetClientName() {
        return syncedPlayerName;
    }

    public BackColor GetBackColor() {
        return syncedBackColor;
    }

    public bool IsFirst() {
        return syncedFirstTurn;
    }

    public string GetChatName() {
        return GetClientName();
    }

    public Color GetChatColor(float alpha = 1) {
        return new Color(Color.gray.r, Color.gray.g, Color.gray.b, alpha);
    }

    #endregion Getters

    #region Server Event Overrides

    /// <summary>
    /// This is invoked for NetworkBehaviour objects when they become active on the server.
    /// <para>This could be triggered by NetworkServer.Listen() for objects in the scene, or by NetworkServer.Spawn() for objects that are dynamically created.</para>
    /// <para>This will be called for objects on a "host" as well as for object on a dedicated server.</para>
    /// </summary>
    [Server]
    public override void OnStartServer() {
        // auth data received, store full authData on server
        authData = (DSAuthData)connectionToClient.authenticationData;

        // send warnings for build/revision mismatch
        Version clientVersion = new(authData.clientVersion);
        string msg = null;
        if (!clientVersion.Build.Equals(DSTweakData.VERSION.Build))
            msg = CreateVersionMismatchWarning("Build", authData.clientName, clientVersion.Build, DSTweakData.VERSION.Build);
        else if (!clientVersion.Revision.Equals(DSTweakData.VERSION.Revision))
            msg = CreateVersionMismatchWarning("Revision", authData.clientName, clientVersion.Revision, DSTweakData.VERSION.Revision);
        if (msg != null) {
            DSChatManager.singleton.RpcReceiveChatMessage(msg, null);
            DSChatManager.singleton.RpcHeadsup(msg);
        }

        // set clientName as playerName and sync to all clients
        SetSyncedPlayerName(authData.clientName);

        // set client preferences
        SetSyncedBackColor(authData.prefBackColor);
        SetSyncedFirstTurn(authData.prefFirst);
    }

    #endregion Server Event Overrides

    #region Client Hook Overrides

    [Client]
    public override void ReadyStateChanged(bool _, bool newReadyState) {
        string name = GetClientName();
        if (string.IsNullOrEmpty(name)) return;
        if (LobbyUI.Instance == null) return;

        LobbyUI.Instance.OnRoomClientReadyStateChanged(name, newReadyState);
    }

    #endregion Client Hook Overrides

    #region Client Hooks

    [Client]
    public override void OnClientTypeChanged() {
        if (LobbyUI.Instance == null || GetClientName() == null) return;

        LobbyUI.Instance.OnRoomClientTypeChanged(GetClientName(), GetClientType());
    }

    [Client]
    private void SyncedBackColorChanged(BackColor _, BackColor newState) {
        if (LobbyUI.Instance == null || GetClientName() == null) return;

        LobbyUI.Instance.OnRoomClientColorChanged(GetClientName(), newState);
    }

    [Client]
    private void SyncedFirstTurnChanged(bool _, bool newState) {
        if (LobbyUI.Instance == null || GetClientName() == null) return;

        LobbyUI.Instance.OnRoomClientFirstChanged(GetClientName(), newState);
    }

    #endregion Client Hooks

    #region Helpers

    [Server]
    private string CreateVersionMismatchWarning(string versionType, string clientName, int clientVersion, int serverVersion) {
        return $"WARNING: {versionType} number mismatch: {clientName} (Client: {clientVersion}, Server: {serverVersion})";
    }

    [TargetRpc]
    internal void RpcSetReady() {
        CmdChangeReadyState(true);
    }

    [TargetRpc]
    internal void RpcSetUnready() {
        CmdChangeReadyState(false);
    }

    #endregion Helpers
}