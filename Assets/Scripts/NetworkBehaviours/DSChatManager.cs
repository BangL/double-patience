﻿using Assets.Scripts.Core;
using Mirror;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(NetworkIdentity))]
public class DSChatManager : NetworkBehaviour {
    internal static DSChatManager singleton;

    private void Awake() {
        if (singleton != null)
            Destroy(singleton); // destroy old instance
        singleton = this;
    }

    public override void OnStartClient() {
        ChatUI.singleton.gameObject.SetActive(true);
    }

    /// <summary>
    /// Broadcasts chat message to all clients
    /// </summary>
    /// <param name="message"></param>
    /// <param name="senderId"></param>
    [Command(requiresAuthority = false)]
    internal void CmdBroadcastChatMessage(string message, NetworkConnectionToClient sender = null) {
        if (!string.IsNullOrWhiteSpace(message))
            RpcReceiveChatMessage(message.Trim(), sender.identity.netId);
    }

    /// <summary>
    /// CmdSend Receiver
    /// </summary>
    /// <param name="message"></param>
    /// <param name="senderId"></param>
    [ClientRpc]
    internal void RpcReceiveChatMessage(string message, uint? senderId) {
        if (ChatUI.singleton == null) {
            Debug.LogWarning("Chat message received while no ChatUI is spawned.");
            return;
        }
        ChatUI.singleton.ReceiveChatMessage(message, senderId);
    }

    [Command(requiresAuthority = false)]
    internal void CmdTypingChanged(bool typing, NetworkConnectionToClient sender = null) {
        RpcTypingChanged(typing, sender.identity.netId);
    }

    [ClientRpc]
    internal void RpcTypingChanged(bool typing, uint senderId) {
        if (ChatUI.singleton == null) {
            Debug.LogWarning("Typing status received while no ChatUI is spawned.");
            return;
        }
        ChatUI.singleton.ReceiveTypingChanged(typing, senderId);
    }

    [ClientRpc]
    internal void RpcHeadsup(string v) {
        if (PopupManager.Instance != null) // FIXME
        {
            PopupManager.Instance.AddPopup(v);
        }
    }
}