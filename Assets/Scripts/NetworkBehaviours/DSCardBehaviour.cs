using Assets.Scripts;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	Documentation: https://mirror-networking.gitbook.io/docs/guides/networkbehaviour
	API Reference: https://mirror-networking.com/docs/api/Mirror.NetworkBehaviour.html
*/

// NOTE: Do not put objects in DontDestroyOnLoad (DDOL) in Awake.  You can do that in Start instead.

[AddComponentMenu("Double-Solitaire/DS Card")]
[RequireComponent(typeof(Transform))]
[RequireComponent(typeof(SpriteRenderer))]
public class DSCardBehaviour : NetworkBehaviour {
    // ===== PRIVATE

    /// <summary>
    /// current pos (equals value as if animation already done, is set before)
    /// </summary>
    private Vector3 currentPos;

    /// <summary>
    /// current zoom (equals value as if animation already done, is set before)
    /// </summary>
    private bool currentZoom;

    /// <summary>
    /// current shown/sprite (equals value as if animation already done, is set before)
    /// </summary>
    private bool currentShown;

    /// <summary>
    /// used for netsync
    /// </summary>
    [SyncVar] private uint syncedLastStack;

    /// <summary>
    /// used for netsync
    /// </summary>
    [SyncVar] private uint syncedSlipStack;

    /// <summary>
    /// used for netsync
    /// </summary>
    [SyncVar] private uint syncedCurrentStack;

    // ===== INTERNAL

    [SyncVar(hook = nameof(OnNameChanged))] internal new string name; // FIXME: proxy this

    /// <summary>
    /// used by flip animation, will be set automatically
    /// </summary>
    [SyncVar] private BackColor syncedBackColor;

    /// <summary>
    /// used by flip animation, will be set automatically
    /// </summary>
    [SyncVar] private string syncedFrontSprite;

    // ===== PUBLIC

    [Header("Card data")]

    /// <summary>
    ///
    /// </summary>
    [SyncVar] private Suit syncedSuit;

    /// <summary>
    ///
    /// </summary>
    [SyncVar] private Value syncedValue;

    /// <summary>
    /// initial value, only used to fix netsync, NOT current value! to get current value, use currentShown instead!
    /// </summary>
    [SyncVar] private bool syncedShown;

    #region Start & Stop Callbacks

    public override void OnStartServer() {
        name = base.name;
    }

    public override void OnStartClient() {
        currentShown = syncedShown || currentShown; // this strange hack fixes initial network sync issues
        GetComponent<SpriteRenderer>().sprite = DSResourceManager.singleton.GetResource<Sprite>(currentShown ? syncedFrontSprite : syncedBackColor.ToString());
    }

    #endregion Start & Stop Callbacks

    /// <summary>
    /// Internal function for name-syncing
    /// </summary>
    /// <param name="_"></param>
    /// <param name="newValue"></param>
    [Client]
    private void OnNameChanged(string _, string newValue) {
        base.name = newValue;
    }

    /// <summary>
    /// sets sorting order. [not refreshing it though. refresh is handled by dscardstack.refresh()]
    /// </summary>
    /// <param name="i"></param>
    [Client]
    internal void SetSortingOrder(int i) {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sortingOrder = i;
        transform.position = new Vector3(transform.position.x, transform.position.y, i * -.01f);
    }

    #region Shown (Flip)

    /// <summary>
    /// holds currently current flip animation (if)
    /// </summary>
    private Coroutine flipCoroutine = null;

    /// <summary>
    /// triggers flip animation, if needed
    /// </summary>
    /// <param name="shown"></param>
    [Client]
    internal void SetShown(bool shown) {
        if (currentShown != shown) {
            currentShown = shown;
            //Debug.Log($"Flip card: {name} now: {shown}");

            // switch layer
            GetComponent<SpriteRenderer>().sortingLayerName = DSTweakData.LAYER_HAND;

            // flip animation
            if (flipCoroutine != null) StopCoroutine(flipCoroutine);
            flipCoroutine = StartCoroutine(FlipAnim(shown));
        }
    }

    /// <summary>
    /// flip animation
    /// </summary>
    /// <param name="shown"></param>
    /// <returns></returns>
    [Client]
    private IEnumerator FlipAnim(bool shown) {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        float t = 0f;
        while (t < 1f) {
            t += Time.deltaTime / (DSTweakData.ANIMATION_SPEED);
            if (t > 1f) t = 1f;
            Sprite currentSprite = DSResourceManager.singleton.GetResource<Sprite>(shown ? syncedBackColor.ToString() : syncedFrontSprite);
            Sprite targetSprite = DSResourceManager.singleton.GetResource<Sprite>(shown ? syncedFrontSprite : syncedBackColor.ToString());
            if (t < .5f && spriteRenderer.sprite != currentSprite)
                spriteRenderer.sprite = currentSprite;
            else if (t >= .5f && spriteRenderer.sprite != targetSprite)
                spriteRenderer.sprite = targetSprite;
            transform.rotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(0, 180, 0), t <= .5f ? t : 1f - t);
            yield return null;
        }
        transform.rotation = Quaternion.identity;

        spriteRenderer.sortingLayerName = DSTweakData.LAYER_CARDS;

        flipCoroutine = null;
    }

    #endregion Shown (Flip)

    #region Position (Move)

    /// <summary>
    /// holds current move animation
    /// </summary>
    private Coroutine moveCoroutine = null;

    /// <summary>
    /// triggers move animation
    /// </summary>
    /// <param name="position"></param>
    [Client]
    internal void SetTablePosition(Vector3 position, int z) {
        if (currentPos != position) {
            currentPos = position;

            // switch layer
            GetComponent<SpriteRenderer>().sortingLayerName = DSTweakData.LAYER_HAND;

            if (moveCoroutine != null) StopCoroutine(moveCoroutine);
            moveCoroutine = StartCoroutine(MoveAnim(position, z));
        }
    }

    [Client]
    internal void SetHandPosition(Vector3 position) {
        if (currentPos != position) {
            currentPos = position;

            // switch layer
            GetComponent<SpriteRenderer>().sortingLayerName = DSTweakData.LAYER_HAND;

            if (moveCoroutine != null) StopCoroutine(moveCoroutine);
            transform.position = new Vector3(position.x, position.y);
        }
    }

    /// <summary>
    /// move animation
    /// </summary>
    /// <param name="targetPos"></param>
    /// <returns></returns>
    [Client]
    private IEnumerator MoveAnim(Vector3 targetPos, int z) {
        float t = 0f;
        Vector3 startPos = transform.position;
        while (t < 1) {
            t += Time.deltaTime / DSTweakData.ANIMATION_SPEED;
            if (t > 1) t = 1;
            transform.position = Vector3.Lerp(startPos, targetPos, t);
            SetSortingOrder(z);
            yield return null;
        }
        transform.position = targetPos;

        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        // reset layer
        spriteRenderer.sortingLayerName = DSTweakData.LAYER_CARDS;

        // update sorting order
        SetSortingOrder(z);

        // done
        moveCoroutine = null;
    }

    #endregion Position (Move)

    #region Focus (Zoom)

    /// <summary>
    /// holds current zoom animation
    /// </summary>
    private Coroutine zoomCoroutine = null;

    /// <summary>
    /// triggers zoom animation
    /// </summary>
    /// <param name="playerObj"></param>
    /// <param name="value"></param>
    [Client]
    internal void Focus(bool value/*, int z*/) {
        if (currentZoom != value) {
            currentZoom = value;

            // switch layer
            GetComponent<SpriteRenderer>().sortingLayerName = DSTweakData.LAYER_HAND;

            // zoom
            if (zoomCoroutine != null) StopCoroutine(zoomCoroutine);
            zoomCoroutine = StartCoroutine(ZoomAnim(Vector3.one * (value ? DSTweakData.ZOOM_FOCUSED : DSTweakData.ZOOM_UNFOCUSED), value ? DSTweakData.HAND_ALPHA : 1f/* , z */));
        }
    }

    /// <summary>
    /// zoom animation
    /// </summary>
    /// <param name="targetZoom"></param>
    /// <returns></returns>
    [Client]
    private IEnumerator ZoomAnim(Vector3 targetZoom, float targetAlpha/* , int z */) {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        Vector3 startZoom = transform.localScale;
        float startAlpha = spriteRenderer.color.a;

        float transparency = 1f - targetAlpha;
        targetAlpha = 1f - (transparency /* * (z + 1) */);
        if (targetAlpha < 0) targetAlpha = 0;

        float t = 0f;
        while (t < 1) {
            t += Time.deltaTime / DSTweakData.ANIMATION_SPEED;
            if (t > 1) t = 1;
            transform.localScale = Vector3.Lerp(startZoom, targetZoom, t);
            spriteRenderer.color = new(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, Mathf.Lerp(startAlpha, targetAlpha, t));
            yield return null;
        }
        transform.localScale = targetZoom;
        spriteRenderer.color = new(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, targetAlpha);

        GetComponent<SpriteRenderer>().sortingLayerName = DSTweakData.LAYER_CARDS;

        zoomCoroutine = null;
    }

    /// <summary>
    /// set current stack
    /// </summary>
    /// <param name="stack"></param>
    [Server]
    internal void SetCurrentStack(DSCardStackBehaviour stack) {
        syncedCurrentStack = stack != null ? stack.netId : 0;
    }

    /// <summary>
    /// returns current stack
    /// </summary>
    /// <returns></returns>
    internal DSCardStackBehaviour GetCurrentStack() {
        return DSNetworkRoomManager.GetNetworkComponent<DSCardStackBehaviour>(syncedCurrentStack);
    }

    /// <summary>
    /// set previous stack
    /// </summary>
    /// <param name="stack"></param>
    [Server]
    internal void SetLastStack(DSCardStackBehaviour stack) {
        syncedLastStack = stack != null ? stack.netId : 0;
    }

    /// <summary>
    /// returns previous stack
    /// </summary>
    /// <returns></returns>
    internal DSCardStackBehaviour GetLastStack() {
        return DSNetworkRoomManager.GetNetworkComponent<DSCardStackBehaviour>(syncedLastStack);
    }

    /// <summary>
    /// set slip stack
    /// </summary>
    /// <param name="stack"></param>
    [Server]
    internal void SetSlipStack(DSCardStackBehaviour stack) {
        syncedSlipStack = stack != null ? stack.netId : 0;
    }

    /// <summary>
    /// returns slip stack
    /// </summary>
    /// <returns></returns>
    internal DSCardStackBehaviour GetSlipStack() {
        return DSNetworkRoomManager.GetNetworkComponent<DSCardStackBehaviour>(syncedSlipStack);
    }

    #endregion Focus (Zoom)

    [Server]
    internal bool CanServeMiddle() {
        if (syncedValue.Equals(Value.Ace)) return true; // aces always can serve middle
        List<DSCardStackBehaviour> aceStacks = DSGameManager.singleton.GetStacksWhere(StackType.Middle);
        foreach (DSCardStackBehaviour aceStack in aceStacks) {
            if (aceStack != null) {
                DSCardBehaviour topCard = aceStack.GetTopCard();
                if ((topCard != null) && topCard.syncedSuit.Equals(syncedSuit) && topCard.syncedValue.Equals(syncedValue - 1)) {
                    return true;
                }
            }
        }
        return false;
    }

    internal Value GetValue() {
        return syncedValue;
    }

    internal Suit GetSuit() {
        return syncedSuit;
    }

    internal BackColor GetBackColor() {
        return syncedBackColor;
    }

    internal void SetSyncedSuit(Suit suit) {
        syncedSuit = suit;
    }

    internal void SetSyncedValue(Value value) {
        syncedValue = value;
    }

    internal void SetSyncedFrontSprite(string v) {
        syncedFrontSprite = v;
    }

    internal void SetSyncedBackColor(BackColor v) {
        syncedBackColor = v;
    }

    internal void SetSyncedShown(bool shown) {
        syncedShown = shown;
    }
}