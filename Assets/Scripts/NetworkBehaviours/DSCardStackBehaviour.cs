﻿using Assets.Scripts;
using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
	Documentation: https://mirror-networking.gitbook.io/docs/guides/networkbehaviour
	API Reference: https://mirror-networking.com/docs/api/Mirror.NetworkBehaviour.html
*/

// NOTE: Do not put objects in DontDestroyOnLoad (DDOL) in Awake.  You can do that in Start instead.

[AddComponentMenu("Double-Solitaire/DS Card Stack")]
public class DSCardStackBehaviour : NetworkBehaviour {
    private readonly SyncList<uint> cards = new();

    private readonly Dictionary<Color, Color> currentHighlights = new();

    private readonly Dictionary<Color, Color> currentBottomHighlights = new();

    #region SyncVars

    [SyncVar] private StackType syncedStackType;

    internal void SetSyncedStackType(StackType stackType) {
        syncedStackType = stackType;
    }

    internal StackType GetStackType() {
        return syncedStackType;
    }

    [SyncVar] private Vector2 syncedRenderOffset;

    internal void SetSyncedRenderOffset(Vector2 renderOffset) {
        syncedRenderOffset = renderOffset;
    }

    [SyncVar] private bool syncedIsBottomPlayable = false;

    internal void SetSyncedIsBottomPlayable(bool isBottomPlayable) {
        syncedIsBottomPlayable = isBottomPlayable;
    }

    [SyncVar(hook = nameof(SyncedNameChanged))]
    private string syncedName;

    [Client]
    private void SyncedNameChanged(string _1, string _2) {
        name = syncedName;
    }

    [SyncVar(hook = nameof(SyncedFakeModeChanged))]
    private bool syncedFakeMode = false;

    [Client]
    private void SyncedFakeModeChanged(bool _1, bool _2) {
        Refresh();
    }

    #endregion SyncVars

    #region Start & Stop Callbacks

    private void Start() {
        SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        SpriteRenderer topRenderer = spriteRenderers[0];
        SpriteRenderer bottomRenderer = spriteRenderers[1];
        topRenderer.enabled = false;
        bottomRenderer.enabled = false;
    }

    public override void OnStartServer() {
        syncedName = name;
    }

    #endregion Start & Stop Callbacks

    #region Server

    /// <summary>
    /// Removes a specific card from the stack
    /// </summary>
    /// <param name="card"></param>
    [Server]
    internal void RemoveCard(DSCardBehaviour card, bool sendUpdate) {
        cards.Remove(card.netId);
        if (sendUpdate) RpcCardsUpdated();
    }

    /// <summary>
    /// Takes one specific card to the top, removing it from it the old stack.
    /// </summary>
    /// <param name="card"></param>
    [Server]
    internal void TakeCard(DSCardBehaviour card, bool sendUpdate, bool slip = false) {
        DSCardStackBehaviour currentStack = card.GetCurrentStack();

        // remove card from hands
        DSIngameClient player1 = DSGameManager.GetPlayer1();
        if (player1 != null && player1.HasHand()) {
            player1.ClearHand(true);
        }
        DSIngameClient player2 = DSGameManager.GetPlayer2();
        if (player2 != null && player2.HasHand()) {
            player2.ClearHand(true);
        }

        if (slip) cards.Insert(0, card.netId); else cards.Add(card.netId); // take card slip/move
        card.SetCurrentStack(this); // set self as current stack

        // remove card from previous stack (if set)
        if (currentStack != null) {
            currentStack.RemoveCard(card, false);

            // special case: slip from player stack
            if (slip && (currentStack.syncedStackType.HasFlag(StackType.POwned))) {
                card.SetLastStack(DSGameManager.singleton.GetFreeSlot());
                card.SetSlipStack(currentStack);
            } else if (slip) { // slip, but not from player stack
                card.SetLastStack(currentStack);
                card.SetSlipStack(null);
            } else { // normal move / slip from game stacks
                card.SetLastStack(currentStack);
            }
        }

        if (sendUpdate) {
            RpcCardsUpdated();
            if (currentStack != null)
                currentStack.RpcCardsUpdated();
        }
    }

    /// <summary>
    /// Takes all cards, starting at card's index in it, removing it from the old stack.
    /// </summary>
    /// <param name="card"></param>
    [Server]
    internal void TakeStack(DSCardBehaviour card, bool slip = false, bool invert = false) {
        DSCardStackBehaviour srcStack = card.GetCurrentStack();
        int index = srcStack.IndexOf((slip && !invert) ? srcStack.GetBottomCard() : card);
        int count = srcStack.cards.Count - index; // store in var, as it will change each iteration
        if (slip && invert) {
            for (int i = index; i >= 0; i--) {
                TakeCard(srcStack.GetCardAt(i), false, true);
            }
        } else {
            for (int i = 0; i < count; i++) {
                if (slip) {
                    TakeCard(srcStack.GetTopCard(), false, true); // takecard removes, so take same index each time. (ignore i)
                } else {
                    TakeCard(srcStack.GetCardAt(index), false); // takecard removes, so take same index each time. (ignore i)
                }
            }
        }
        RpcCardsUpdated();
        srcStack.RpcCardsUpdated();
    }

    /// <summary>
    /// Flips the full stack of another stack into self, removing it from the old stack.
    /// </summary>
    /// <param name="card"></param>
    [Server]
    private void FlipFullStack(DSCardBehaviour card) {
        DSCardStackBehaviour srcStack = card.GetCurrentStack();
        int count = srcStack.cards.Count; // store in var, as it will change each iteration
        for (int i = 0; i < count; i++) {
            TakeCard(srcStack.GetTopCard(), false); // take card removes, so take top card each time. (ignore i)
        }
        RpcCardsUpdated();
        srcStack.RpcCardsUpdated();
    }

    /// <summary>
    /// Adds cards into the stack at given order, removing from old stacks.
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    [Server]
    internal void TakeCards(IList<GameObject> cards) {
        foreach (GameObject card in cards) {
            TakeCard(card.GetComponent<DSCardBehaviour>(), false);
        }
        RpcCardsUpdated();
    }

    /// <summary>
    /// Clear card list
    /// </summary>
    [Server]
    internal void ClearCards() {
        cards.Clear();
        RpcCardsUpdated();
    }

    /// <summary>
    /// Checks and triggers the win condition.
    /// </summary>
    [Server]
    private void CheckEndGame() {
        // win condition
        for (int p = 0; p <= 1; p++) {
            if (DSGameManager.singleton.GetStacksWhere(p == 0 ? StackType.P1Owned : StackType.P2Owned).TrueForAll(x => x.IsEmpty())) {
                Debug.Log("Win game");
                DSIngameClient player = p == 0 ? DSGameManager.GetPlayer1() : DSGameManager.GetPlayer2();
                DSGameManager.singleton.StageWin(player);
            }
        }
    }

    /// <summary>
    /// Send player highlighting
    /// </summary>
    /// <param name="isBottom"></param>
    /// <param name="color"></param>
    [Command(requiresAuthority = false)]
    internal void CmdHighlight(bool isBottom, Color color) {
        RpcHighlight(isBottom, color);
    }

    /// <summary>
    /// Delete player highlighting
    /// </summary>
    /// <param name="isBottom"></param>
    /// <param name="color"></param>
    [Command(requiresAuthority = false)]
    internal void CmdUnhighlight(bool isBottom, Color color) {
        RpcUnhighlight(isBottom, color);
    }

    /// <summary>
    /// Player request to pickup card
    /// </summary>
    /// <param name="playerObj"></param>
    /// <param name="isBottom"></param>
    [Command(requiresAuthority = false)]
    internal void CmdPickupCard(GameObject playerObj, bool isBottom) {
        DSIngameClient player = playerObj.GetComponent<DSIngameClient>();
        if (!ValidatePickup(playerObj, isBottom, false)) {
            DSGameManager.singleton.RpcPlaySound(GameSound.Error);
        } else {
            DSGameManager.singleton.RpcPlaySound(GameSound.Pickup);
            DSCardBehaviour card = isBottom ? GetBottomCard() : GetTopCard();
            if (card) {
                // pickup hand
                PerformMove(MoveType.Pickup, player, (!syncedStackType.HasFlag(StackType.PlayerMain)) && /*DSGameManager.singleton.lastMoveServedMiddle && (DSGameManager.singleton.lastMove != null) && */ card.CanServeMiddle(), card);
            } else if (syncedStackType.HasFlag(StackType.PlayerMain) && IsPlayerStackOf(player) && (!DSGameManager.singleton.GetStacksWhere(StackType.PlayerDrop | (player.Equals(DSGameManager.GetPlayer1()) ? StackType.P1Owned : StackType.P2Owned), false).First().IsEmpty())) {
                // redraw empty main stack
                PerformMove(MoveType.Redraw, player, false);
            }
        }
    }

    /// <summary>
    /// Perform game move routing function
    /// </summary>
    /// <param name="move"></param>
    /// <param name="player"></param>
    /// <param name="servedMiddle"></param>
    /// <param name="card"></param>
    /// <param name="invert"></param>
    [Server]
    internal void PerformMove(MoveType move, DSIngameClient player, bool servedMiddle, DSCardBehaviour card = null, bool invert = false) {
        Move result = new() {
            type = move,
            card = card,
            //srcStack = card != null ? card.currentStack.GetComponent<DSCardStackBehaviour>() : null, // set before actually changing anything
            //targetStack = this,
            player = player,
        };
        bool isMove = true;
        bool mute = false;
        bool count = false;
        switch (move) {
            case MoveType.Pickup:
                PerformPickup(player, card, invert);
                isMove = card.GetCurrentStack().syncedStackType.HasFlag(StackType.PlayerMain);
                mute = true;
                break;

            case MoveType.Return:
                PerformReturn(player, invert);
                isMove = false;
                break;

            case MoveType.MoveCard:
                PerformMoveCard(card, false, invert);
                isMove = !servedMiddle;
                count = true;
                break;

            case MoveType.Slip:
                PerformMoveCard(card, true, invert);
                count = true;
                break;

            case MoveType.MoveStack:
                PerformMoveStack(card);
                count = true;
                break;

            case MoveType.SlipStack:
                result.card = card.GetCurrentStack().GetTopCard();
                PerformSlipStack(card, invert);
                count = true;
                break;

            case MoveType.Redraw:
                PerformRedraw(player, invert);
                result.card = GetTopCard();
                break;

            default:
                throw new NotImplementedException($"unimplemented move type: {move}");
        }
        if (!mute) DSGameManager.singleton.RpcPlaySound(GameSound.Move);
        if (DSGameManager.IsCurrentPlayer(player) && !invert) {
            //DSGameManager.singleton.lastMoveServedMiddleBefore = DSGameManager.singleton.lastMoveServedMiddle;
            DSGameManager.singleton.lastMoveServedMiddle = servedMiddle;
            if (isMove) {
                DSGameManager.singleton.lastMovedCard = result.card.gameObject;
                DSGameManager.singleton.lastMove = result;
            } else if (servedMiddle) {
                DSGameManager.singleton.lastMovedCard = null;
                DSGameManager.singleton.lastMove = null;
            }
            if (count) DSGameManager.singleton.BumpMoves();
        }

        DSGameManager.singleton.CheckCanEndTurn(player);

        // check endgame
        CheckEndGame();
    }

    /// <summary>
    /// Game move routing function
    /// </summary>
    /// <param name="move"></param>
    /// <param name="player"></param>
    /// <param name="card"></param>
    [Server]
    internal void RevertMove(MoveType move, DSIngameClient player, DSCardBehaviour card) {
        PerformMove(move, player, false, card, true);
    }

    /// <summary>
    /// Internal Action route, Use PerformMove and RevertMove instead
    /// </summary>
    /// <param name="player"></param>
    /// <param name="card"></param>
    /// <param name="invert"></param>
    [Server]
    private void PerformPickup(DSIngameClient player, DSCardBehaviour card, bool invert) {
        if (!invert) {
            player.handTaken = Time.time; // set first
            player.SetHand(card);
        } else {
            player.ClearHand(true);
        }
    }

    /// <summary>
    /// Internal Action route, Use PerformMove and RevertMove instead
    /// </summary>
    /// <param name="player"></param>
    /// <param name="invert"></param>
    [Server]
    private void PerformRedraw(DSIngameClient player, bool invert) {
        DSCardStackBehaviour dropStack = DSGameManager.singleton.GetDropStackOf(player);
        player.ClearHand();
        if (!invert) {
            FlipFullStack(dropStack.GetBottomCard());
            PerformPickup(player, GetTopCard(), false);
        } else {
            PerformPickup(player, GetTopCard(), true);
            dropStack.FlipFullStack(GetBottomCard());
        }
    }

    [Server]
    private void PerformSlipStack(DSCardBehaviour card, bool invert) {
        TakeStack(card, true, invert);
    }

    /// <summary>
    /// Internal Action route, Use PerformMove and RevertMove instead
    /// </summary>
    /// <param name="player"></param>
    /// <param name="invert"></param>
    [Server]
    private void PerformReturn(DSIngameClient player, bool invert) {
        if (!invert) {
            player.ClearHand();
        } else {
            PerformPickup(player, GetTopCard(), false);
        }
    }

    /// <summary>
    /// Internal Action route, Use PerformMove and RevertMove instead
    /// </summary>
    /// <param name="player"></param>
    /// <param name="invert"></param>
    [Server]
    private void PerformMoveCard(DSCardBehaviour card, bool slip, bool invert) {
        if (!invert) {
            // move && redo
            DSCardStackBehaviour currentStack = card.GetCurrentStack();

            // special case: dropping ace to middle...
            DSCardStackBehaviour route = null;
            if (syncedStackType.HasFlag(StackType.Middle) && card.GetValue() == Value.Ace) {
                List<DSCardStackBehaviour> p1Stacks = DSGameManager.singleton.GetStacksWhere(StackType.Middle | StackType.P1Related, true);
                List<DSCardStackBehaviour> p2Stacks = DSGameManager.singleton.GetStacksWhere(StackType.Middle | StackType.P2Related, true);

                bool isP1card = card.GetBackColor().Equals(DSGameManager.GetPlayer1().GetBackColor());
                if (!isP1card) {
                    p1Stacks.Reverse();
                    p2Stacks.Reverse();
                }
                List<DSCardStackBehaviour> myStacks = isP1card ? p1Stacks : p2Stacks;
                List<DSCardStackBehaviour> neighbourStacks = isP1card ? p2Stacks : p1Stacks;

                // force matching ace row and return
                if (!route) route = FindAceStack(myStacks, neighbourStacks, (myStack, neighbourStack)
                    => myStack.GetBottomCard() == null && neighbourStack.GetBottomCard() != null && card.GetSuit() == neighbourStack.GetBottomCard().GetSuit());

                // .. or take suggested row, if free
                if (!route) route = FindAceStack(myStacks, neighbourStacks, (myStack, neighbourStack)
                    => (myStack == this || neighbourStack == this) && myStack.GetBottomCard() == null && neighbourStack.GetBottomCard() == null);

                // .. or find empty row and return
                if (!route) route = FindAceStack(myStacks, neighbourStacks, (myStack, neighbourStack)
                    => myStack.GetBottomCard() == null && neighbourStack.GetBottomCard() == null);
            }

            if (route)
                route.TakeCard(card, true);
            else
                TakeCard(card, true, slip);

            if (currentStack != null && currentStack.syncedStackType.HasFlag(StackType.PlayerMain)) {
                currentStack.syncedFakeMode = false;
            }
        } else {
            // revert on knock
            if (syncedStackType.HasFlag(StackType.PlayerMain)) {
                syncedFakeMode = true;
            }

            TakeCard(card, true);
        }
    }

    /// <summary>
    /// Internal Action route, Use PerformMove and RevertMove instead
    /// </summary>
    /// <param name="player"></param>
    /// <param name="invert"></param>
    [Server]
    private DSCardStackBehaviour FindAceStack(List<DSCardStackBehaviour> myStacks, List<DSCardStackBehaviour> neighbourStacks, Func<DSCardStackBehaviour, DSCardStackBehaviour, bool> condition) {
        foreach (DSCardStackBehaviour myStack in myStacks) {
            int stackIndex = myStacks.IndexOf(myStack);
            if (condition(myStack, neighbourStacks[stackIndex])) {
                return myStack; // route
            }
        }
        return null; // don't route
    }

    /// <summary>
    /// Internal Action route, Use PerformMove and RevertMove instead
    /// </summary>
    /// <param name="player"></param>
    /// <param name="invert"></param>
    [Server]
    private void PerformMoveStack(DSCardBehaviour card) {
        TakeStack(card);
    }

    /// <summary>
    /// Player request to Drop hand
    /// </summary>
    /// <param name="playerObj"></param>
    [Command(requiresAuthority = false)]
    internal void CmdDropCard(GameObject playerObj, bool isBottom) {
        // validate
        ValidateDropResult result = ValidateDrop(playerObj, isBottom, false);

        // skipped (no sound)
        if (result == ValidateDropResult.Null) return;
        // cancelled (error sound)
        if (result == ValidateDropResult.Cancel) {
            DSGameManager.singleton.RpcPlaySound(GameSound.Error);
            return;
        }

        DSIngameClient player = playerObj.GetComponent<DSIngameClient>();
        DSCardBehaviour hand = player.GetMainHand();
        DSCardStackBehaviour srcStack = hand.GetCurrentStack();

        // return/drop
        if (result.HasFlag(ValidateDropResult.Return)) {
            // is return
            PerformMove(MoveType.Return, player, DSGameManager.singleton.lastMoveServedMiddle/*Before*/, hand); // reset lastMoveServedMiddle
        } else {
            // is move
            if (result.HasFlag(ValidateDropResult.Drop)) {
                // is card move
                PerformMove(MoveType.MoveCard, player, result.HasFlag(ValidateDropResult.ServeMiddle), hand);
            } else if (result.HasFlag(ValidateDropResult.DropStack)) {
                // is stack move
                PerformMove(MoveType.MoveStack, player, false, srcStack.GetBottomCard());
            } else if (result.HasFlag(ValidateDropResult.Slip)) {
                PerformMove(MoveType.Slip, player, false, hand);
            } else if (result.HasFlag(ValidateDropResult.SlipStack)) {
                PerformMove(MoveType.SlipStack, player, false, srcStack.GetBottomCard());
            }
        }

        if (result.HasFlag(ValidateDropResult.EndTurn)) {
            // end turn
            DSGameManager.singleton.EndTurn();
        }
    }

    /// <summary>
    /// Player knock prove
    /// </summary>
    /// <param name="playerObj"></param>
    [Command(requiresAuthority = false)]
    internal void CmdKnockConfirm(GameObject playerObj) {
        ValidateDropResult result = ValidateDrop(playerObj, false, false);
        DSIngameClient opponent = DSGameManager.GetOpponentPlayer();
        if (result.HasFlag(ValidateDropResult.ServeMiddle)) {
            opponent.ClearHand(true);

            if (DSGameManager.singleton.lastMove.HasValue) {
                Move move = DSGameManager.singleton.lastMove.Value;

                if (move.type == MoveType.Slip && move.card.GetSlipStack() != null) {
                    move.card.GetSlipStack().TakeCard(move.card, true);
                }

                // reset fakeMode
                move.card.GetCurrentStack().syncedFakeMode = false;
            }

            // end turn
            DSGameManager.singleton.EndTurn();
        } else if (result.HasFlag(ValidateDropResult.Return)) {
            opponent.ClearHand(true);
        }
    }

    #endregion Server

    #region Client

    /// <summary>
    /// Draws current highlights
    /// </summary>
    /// <param name="isBottomCollider"></param>
    [Client]
    private void RefreshHighlight(bool isBottomCollider) {
        Dictionary<Color, Color> highlights = isBottomCollider ? currentBottomHighlights : currentHighlights;
        SpriteRenderer spriteRenderer = GetComponentsInChildren<SpriteRenderer>()[isBottomCollider ? 1 : 0];
        Color playerCopy = DSGameManager.GetLocalPlayer().GetChatColor();
        spriteRenderer.enabled = highlights.Count > 0;
        foreach (KeyValuePair<Color, Color> hightlight in highlights) {
            spriteRenderer.color = hightlight.Value;
            if (hightlight.Key == playerCopy) {
                break; // prio own player color
            }
        }
    }

    /// <summary>
    /// receives player highlight
    /// </summary>
    /// <param name="isBottomCollider"></param>
    /// <param name="color"></param>
    [ClientRpc]
    private void RpcHighlight(bool isBottomCollider, Color color) {
        var highlights = isBottomCollider ? currentBottomHighlights : currentHighlights;
        Color copy = new(color.r, color.g, color.b, 1f);
        if (!highlights.Keys.Contains(copy)) {
            highlights.Add(copy, color); // add
        } else {
            highlights[copy] = color; // overwrite
        }
        RefreshHighlight(isBottomCollider);
    }

    /// <summary>
    /// receives player highlight deletions
    /// </summary>
    /// <param name="isBottomCollider"></param>
    /// <param name="color"></param>
    [ClientRpc]
    internal void RpcUnhighlight(bool isBottomCollider, Color color) {
        var highlights = isBottomCollider ? currentBottomHighlights : currentHighlights;
        Color copy = new(color.r, color.g, color.b, 1f);
        if (highlights.Keys.Contains(copy)) {
            highlights.Remove(copy);
            RefreshHighlight(isBottomCollider);
        }
    }

    /// <summary>
    /// Card list updated by server
    /// </summary>
    [ClientRpc]
    internal void RpcCardsUpdated() {
        Refresh();
    }

    /// <summary>
    /// Refreshes visuals of the cards in the stack (move/flip)
    /// </summary>
    [Client]
    internal void Refresh() {
        if (DSGameManager.singleton == null) return; // skip if not fully loaded in yet

        int i = 0;
        int top = cards.Count - 1;
        Vector3 lastPos = transform.position;
        foreach (uint netId in cards) {
            DSCardBehaviour card = DSNetworkRoomManager.GetNetworkComponent<DSCardBehaviour>(netId);
            if (card == null) continue; // skip if not fully loaded in yet
            if (card.TryGetComponent<DSCardBehaviour>(out var cardBehaviour) && !DSGameManager.singleton.IsHand(cardBehaviour)) {
                cardBehaviour.SetShown((!syncedStackType.HasFlag(StackType.PlayerMain)) || (syncedFakeMode && (cardBehaviour.gameObject == DSGameManager.singleton.lastMovedCard)));
                lastPos = transform.position + new Vector3(i * syncedRenderOffset.x, i * (syncedRenderOffset.y * (DSGameManager.GetPlayer2().isLocalPlayer ? -1 : 1)), i * -.01f);
                cardBehaviour.SetTablePosition(lastPos, i);
            }
            i++;
        }

        SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();

        SpriteRenderer topRenderer = spriteRenderers[0];
        Transform topTransform = topRenderer.GetComponent<Transform>();

        // move top collider
        //topRenderer.enabled = false;
        topRenderer.sortingOrder = top;
        topTransform.position = new Vector3(lastPos.x, lastPos.y, lastPos.z - .02f);

        SpriteRenderer bottomRenderer = spriteRenderers[1];
        Transform bottomTransform = bottomRenderer.GetComponent<Transform>();
        BoxCollider2D bottomCollider = bottomRenderer.GetComponent<BoxCollider2D>();

        // enable/disable bottom collider
        //bottomRenderer.enabled = false;
        bottomTransform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - .01f);
        bottomCollider.enabled = syncedIsBottomPlayable;

        //if (DSSettings.singleton.DEBUG) Debug.Log($"Refreshed {i} / {top + 1} cards in stack {name}.");
    }

    #endregion Client

    #region General Helpers

    internal bool ValidatePickup(GameObject playerObj, bool isBottom, bool silent) {
        DSIngameClient player = playerObj.GetComponent<DSIngameClient>();

        // cannot act if not players turn
        if (!player.CanInteract()) {
            if (!silent) Debug.Log("cannot act if not my turn or knocking");
            return false;
        }

        // cannot grab from empty stack
        if (IsEmpty() && (!syncedStackType.HasFlag(StackType.PlayerMain))) {
            if (!silent) Debug.Log("cannot take from empty stack (only if main, for redraw)");
            return false;
        }

        // cannot grab from middle or other player
        if ((!IsPlayerStackOf(DSGameManager.GetCurrentPlayer())) && (!syncedStackType.HasFlag(StackType.Game))) {
            if (!silent) Debug.Log("cannot pickup from center or foreign player stacks");
            return false;
        }

        // cannot grab from unturned main stack when confirming knock
        if (DSGameManager.singleton.knockMode && syncedStackType.HasFlag(StackType.PlayerMain) && !syncedFakeMode) {
            if (!silent) Debug.Log("cannot prove middle-serve from foreign unturned stack when last played card was not taken from there");
            return false;
        }

        // cannot grab stacks in knock mode
        if (DSGameManager.singleton.knockMode && isBottom) {
            if (!silent) Debug.Log("cannot prove middle-serve by whole stack");
            return false;
        }

        return true;
    }

    /// <summary>
    /// validates if a player can drop its current hand on self
    /// </summary>
    /// <param name="playerObj"></param>
    /// <returns></returns>
    internal ValidateDropResult ValidateDrop(GameObject playerObj, bool isBottomCollider, bool silent) {
        DSIngameClient player = playerObj.GetComponent<DSIngameClient>();

        // cannot act if not my turn/knockmode
        if (!player.CanInteract()) {
            if (!silent) Debug.Log("cannot act if not my turn/knockmode");
            return ValidateDropResult.Cancel;
        }
        // cannot act if hand empty
        if (!player.HasHand()) {
            if (!silent) Debug.Log("cannot act if hand empty");
            return ValidateDropResult.Cancel;
        }

        DSIngameClient currentPlayer = DSGameManager.GetCurrentPlayer();
        DSCardBehaviour bottomCard = GetBottomCard();
        DSCardBehaviour topCard = GetTopCard();
        DSCardBehaviour card = player.GetMainHand();
        DSCardStackBehaviour srcStack = card.GetCurrentStack();
        DSCardBehaviour srcBottomCard = srcStack.GetBottomCard();
        DSCardBehaviour srcTopCard = srcStack.GetTopCard();
        bool isSrcBottom = card == srcBottomCard;
        bool isSrcTop = card == srcTopCard;
        bool isOwn = IsPlayerStackOf(currentPlayer);
        bool isSrcOwn = srcStack.IsPlayerStackOf(currentPlayer);
        bool isFirstTurn = DSGameManager.singleton.turnCount == 1;
        bool isFromMain = srcStack.syncedStackType.HasFlag(StackType.PlayerMain);
        bool isMain = syncedStackType.HasFlag(StackType.PlayerMain);
        bool isReturn = srcStack == this && (!isFromMain || syncedFakeMode);
        bool isKnock = DSGameManager.singleton.knockMode;
        bool isMiddle = syncedStackType.HasFlag(StackType.Middle);

        // cancel early return
        if (Time.time - player.handTaken < DSTweakData.ANIMATION_SPEED) {
            //if (!silent) Debug.Log("cancelled early return");
            return ValidateDropResult.Null;
        }

        // cannot drop/return to main stack
        if (isMain && !syncedFakeMode) {
            if (!silent) Debug.Log("cannot drop/return to main stack");
            return ValidateDropResult.Cancel;
        }

        // return card
        if (isReturn) {
            if (!silent) Debug.Log("return card");
            return ValidateDropResult.Return;
        }

        // lock to middle slots while confirming knock
        if (isKnock && !isMiddle) {
            if (!silent) Debug.Log("cannot drop to anything but middle stacks when confirming knock");
            return ValidateDropResult.Cancel;
        }

        // cant drop to own 13
        if (isOwn && syncedStackType.HasFlag(StackType.Player13)) {
            if (!silent) Debug.Log("cant drop to own 13");
            return ValidateDropResult.Cancel;
        }

        // end turn condition
        // drop from own main stack, to own drop stack ends turn
        if (isFromMain && isOwn && syncedStackType.HasFlag(StackType.PlayerDrop)) {
            if (!silent) Debug.Log("drop from own main stack, to own drop stack ends turn");
            return ValidateDropResult.Drop | ValidateDropResult.EndTurn;
        }

        switch (syncedStackType & (StackType.PlayerDrop | StackType.Player13 | StackType.Middle | StackType.Game)) {
            case StackType.PlayerDrop:
            case StackType.Player13:
                // cant drop stacks on player stacks
                if (isSrcBottom && !isSrcTop) {
                    if (!silent) Debug.Log("cant drop stacks on player stacks");
                    return ValidateDropResult.Cancel;
                }
                // cant drop on empty player stacks
                if (topCard == null) {
                    if (!silent) Debug.Log("cant drop on empty player stacks");
                    return ValidateDropResult.Cancel;
                }
                // cant drop if suit does not match
                if (topCard != null && topCard.GetSuit() != card.GetSuit()) {
                    if (!silent) Debug.Log("cant drop if suit does not match");
                    return ValidateDropResult.Cancel;
                }
                // cant drop if value diff not exactly +/-1
                if (topCard != null && Mathf.Abs((int)topCard.GetValue() - (int)card.GetValue()) != 1) {
                    if (!silent) Debug.Log("cant drop if value diff not exactly +/-1");
                    return ValidateDropResult.Cancel;
                }

                if (!silent) Debug.Log("drop on player stack");
                return ValidateDropResult.Drop;

            case StackType.Middle:
                // cant drop stacks on middle stacks
                if (isSrcBottom && !isSrcTop) {
                    if (!silent) Debug.Log("cant drop stacks on middle stacks");
                    return ValidateDropResult.Cancel;
                }
                // cant drop if top card suit does not match
                if (topCard != null && topCard.GetSuit() != card.GetSuit()) {
                    if (!silent) Debug.Log("cant drop if top card suit does not match");
                    return ValidateDropResult.Cancel;
                }
                // cant drop if top card diff not exactly +1
                if (topCard != null && card.GetValue() - topCard.GetValue() != 1) {
                    if (!silent) Debug.Log("cant drop if top card diff not exactly +1");
                    return ValidateDropResult.Cancel;
                }
                // cant drop if first card value not 1
                if (topCard == null && (int)card.GetValue() != 1) {
                    if (!silent) Debug.Log("cant drop if first card value not 1");
                    return ValidateDropResult.Cancel;
                }

                if (!silent) Debug.Log("serve middle");
                return ValidateDropResult.Drop | ValidateDropResult.ServeMiddle;

            case StackType.Game:

                // cant play opponent game stacks on first turn
                if (isFirstTurn && GetStackType().HasFlag(StackType.P2Related)) {
                    if (!silent) Debug.Log("cant play opponent game stacks on first turn");
                    return ValidateDropResult.Cancel;
                }

                bool isSrcStack = (isSrcBottom && !isSrcTop);

                DSCardBehaviour targetCard = (isBottomCollider || (cards.Count == 1)) ? bottomCard : topCard;
                DSCardBehaviour srcBaseCard = isSrcBottom ? srcBottomCard : srcTopCard;

                bool isBottom = targetCard == bottomCard;

                bool isTop = targetCard == topCard;

                bool isSlip = targetCard && ((srcTopCard.GetValue() - targetCard.GetValue()) == +1) && isBottom;
                bool isDrop = !targetCard || (((srcBaseCard.GetValue() - targetCard.GetValue()) == -1) && isTop);

                DSCardBehaviour srcCard = (isSrcStack && isSlip) ? srcTopCard : card;

                // cant drop if top card diff not exactly -1
                if (!isSlip && !isDrop) {
                    if (!silent) Debug.Log("cant drop/slip if diff not exactly +/-1");
                    return ValidateDropResult.Cancel;
                }

                // cant from player stack if no free slot
                if (isSlip && isSrcOwn && DSGameManager.singleton.GetFreeSlot() == null) {
                    if (!silent) Debug.Log("cant slip under from player stacks, if no free slot");
                    return ValidateDropResult.Cancel;
                }

                // cant drop on same color
                if (targetCard != null && ((targetCard.GetSuit() == Suit.Heart || targetCard.GetSuit() == Suit.Diamond) ?
                    (srcCard.GetSuit() == Suit.Heart || srcCard.GetSuit() == Suit.Diamond) :
                    (srcCard.GetSuit() == Suit.Spade || srcCard.GetSuit() == Suit.Club))) {
                    if (!silent) Debug.Log("cant drop on same color");
                    return ValidateDropResult.Cancel;
                }

                // only entire stacks can be moved from game stack to game stack
                if (srcStack.syncedStackType.HasFlag(StackType.Game) && !isSrcBottom) {
                    if (!silent) Debug.Log("only entire stacks can be moved from game stack to game stack");
                    return ValidateDropResult.Cancel;
                }

                if (isSrcStack) {
                    // move stack
                    if (isSlip) {
                        if (!silent) Debug.Log("slip stack");
                        return ValidateDropResult.SlipStack;
                    } else {
                        if (!silent) Debug.Log("drop stack");
                        return ValidateDropResult.DropStack;
                    }
                } else {
                    // move card
                    if (isSlip) {
                        if (!silent) Debug.Log("slip");
                        return ValidateDropResult.Slip;
                    } else {
                        if (!silent) Debug.Log("drop");
                        return ValidateDropResult.Drop;
                    }
                }
            default:
                throw new NotImplementedException($"unimplemented stack type: {syncedStackType}");
        }
    }

    internal bool IsPlayerStackOf(DSIngameClient player) {
        bool isP1 = DSGameManager.GetPlayer1().Equals(player) && GetStackType().HasFlag(StackType.P1Owned);
        bool isP2 = DSGameManager.GetPlayer2().Equals(player) && GetStackType().HasFlag(StackType.P2Owned);
        return isP1 || isP2;
    }

    internal void Shuffle() {
        cards.Shuffle();
        RpcCardsUpdated();
    }

    internal bool IsEmpty() {
        return cards.Count.Equals(0);
    }

    /// <summary>
    /// Get card at index
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    internal DSCardBehaviour GetCardAt(int index) {
        return DSNetworkRoomManager.GetNetworkComponent<DSCardBehaviour>(cards.ElementAt(index));
    }

    /// <summary>
    /// Returns the count of current cards in the stack
    /// </summary>
    /// <returns></returns>
    internal int GetCardCount() {
        return cards.Count;
    }

    /// <summary>
    /// returns the current top card of the stack
    /// </summary>
    /// <returns></returns>
    internal DSCardBehaviour GetTopCard() {
        if (cards.Count > 0) {
            return DSNetworkRoomManager.GetNetworkComponent<DSCardBehaviour>(cards[^1]);
        } else {
            return null;
        }
    }

    /// <summary>
    /// returns the current bottom card of the stack (effectively index 0)
    /// </summary>
    /// <returns></returns>
    internal DSCardBehaviour GetBottomCard() {
        if (cards.Count > 0) {
            return DSNetworkRoomManager.GetNetworkComponent<DSCardBehaviour>(cards[0]);
        } else {
            return null;
        }
    }

    /// <summary>
    /// returns the index of any card, IF its in the stack. otherwise -1!
    /// </summary>
    /// <param name="card"></param>
    /// <returns></returns>
    internal int IndexOf(DSCardBehaviour card) {
        return cards.IndexOf(card.netId);
    }

    #endregion General Helpers
}