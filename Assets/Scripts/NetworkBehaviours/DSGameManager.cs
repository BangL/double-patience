using Assets.Scripts;
using Assets.Scripts.Core;
using Assets.Scripts.UI.Networked;
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

[AddComponentMenu("Double-Solitaire/DS Game Manager")]
[DisallowMultipleComponent]
[RequireComponent(typeof(DSResourceManager))]
public class DSGameManager : NetworkBehaviour {
    // ===== fields

    // ===== internal

    internal static DSGameManager singleton;

    private WaitForSeconds pause;

    private readonly List<GameObject> cardsPlayer1 = new(); // server only
    private readonly List<GameObject> cardsPlayer2 = new(); // server only

    internal Move? lastMove = null; // server only

    // ===== synced

    [SyncVar] internal GameObject lastMovedCard;
    [SyncVar] internal bool gameReady;
    private readonly SyncDictionary<string, uint> stacks = new();
    [SyncVar(hook = nameof(OnCurrentPlayerChanged))] private GameObject currentPlayer; // FIXME shouldnt be GameObject, as SyncVar
    [SyncVar] private GameObject player1; // FIXME shouldnt be GameObject, as SyncVar
    [SyncVar] private GameObject player2; // FIXME shouldnt be GameObject, as SyncVar
    [SyncVar(hook = nameof(OnTurnCountChanged))] internal int turnCount = 0;
    [SyncVar(hook = nameof(OnKnockModeChanged))] internal bool knockMode;
    [SyncVar(hook = nameof(OnLastMoveServedMiddleChanged))] internal bool lastMoveServedMiddle;
    //internal bool lastMoveServedMiddleBefore;

    // ===== public

    [Header("Prefabs")]
    public GameObject cardStackPrefab;

    public GameObject cardPrefab;

    // ===== fields end

    private void Awake() {
        singleton = this;
    }

    private void Start() {
        pause = new(DSTweakData.ANIMATION_SPEED);
    }

    #region Client

    public override void OnStartClient() {
        LoadResources();
        if (!isServer) {
            IngameUI.singleton.RefreshKnockState();
        }
    }

    internal void LoadResources() {
        // back sprites
        foreach (BackColor backColor in Enum.GetValues(typeof(BackColor))) {
            string backName = backColor.ToString();
            DSResourceManager.singleton.PrecacheResource<Sprite>("CardBacks/BackColor_" + backName, backName);
        }

        // front sprites
        foreach (Suit suit in Enum.GetValues(typeof(Suit))) {
            foreach (Value value in Enum.GetValues(typeof(Value))) {
                string cardName = DSResourceManager.GetCardSpriteAlias(suit, value);
                DSResourceManager.singleton.PrecacheResource<Sprite>("CardFronts/" + cardName, cardName);
            }
        }
    }

    private void OnTurnCountChanged(int _1, int _2) {
        if (IngameUI.singleton != null) IngameUI.singleton.RefreshKnockState();
    }

    private void OnKnockModeChanged(bool _, bool value) {
        if (value) {
            DSIngameClient knockingPlayer = GetOpponentPlayer();

            // show enemy knocked on all client but knocker
            if (!knockingPlayer.isLocalPlayer)
                PopupManager.Instance.AddPopup($"{knockingPlayer.GetChatName()} knocks...");

            // show cancel button on knocking client
            else
                IngameUI.singleton.cancelKnockButton.gameObject.SetActive(true);
        } else
            IngameUI.singleton.cancelKnockButton.gameObject.SetActive(false);
    }

    [Client]
    private void OnLastMoveServedMiddleChanged(bool _1, bool _2) {
        if (IngameUI.singleton == null) return;
        IngameUI.singleton.RefreshKnockState();
    }

    #endregion Client

    #region Server

    private readonly List<DSIngameClient> players = new();
    private readonly SyncList<uint> spectators = new();

    internal void QueueAddPlayer(DSIngameClient player) {
        StartCoroutine(WaitAddPlayer(player));
    }

    private IEnumerator WaitAddPlayer(DSIngameClient player) {
        while (player.netIdentity == null || player.netIdentity.netId == 0)
            yield return null;

        if (player.GetClientType().Equals(BetterNetworkClientType.Player)) {
            if (players.Count < 2) {
                if (player.IsFirst() ? GetPlayer1() == null : GetPlayer2() != null)
                    SetPlayer1(player);
                else
                    SetPlayer2(player);
                players.Add(player);
            }
        } else {
            spectators.Add(player.netId);
            if (fullyLoaded) {
                player.OnPlayerFullyJoined(false);
                OnServerPlayerJoined(player, true);
            }
        }
    }

    [Server]
    internal void StartWaitingForPlayers() {
        StartCoroutine(WaitForPlayers());
    }

    [SyncVar] private bool fullyLoaded = false;

    private IEnumerator WaitForPlayers() {
        Debug.Log("Waiting for the 2 players to join..");
        while (players.Count < 2) {
            yield return null;
        }
        Debug.Log("Game starting...");

        fullyLoaded = true;
        foreach (DSIngameClient player in players) {
            player.OnPlayerFullyJoined(player == GetPlayer2());
            OnServerPlayerJoined(player, false);
        }
        foreach (DSIngameClient player in spectators.Select(x => DSNetworkRoomManager.GetNetworkComponent<DSIngameClient>(x))) {
            player.OnPlayerFullyJoined(false);
            OnServerPlayerJoined(player, false);
        }

        // Start game
        StartOrRestart();

        // wait for leavers
        StartCoroutine(WaitForPlayerLeave());
    }

    /// <summary>
    /// The scene to use for the room. This is similar to the offlineScene of the NetworkManager.
    /// </summary>
    [Scene]
    public string RoomScene;

    private IEnumerator WaitForPlayerLeave() {
        while (players.Count >= 2) {
            yield return null;
        }

        EndGame();
    }

    [Server]
    public void StartOrRestart() {
        if (!isServer) return;

        if (stacks.Count == 0) {
            stacks.AddRange(SpawnCardStacks());
            cardsPlayer1.AddRange(SpawnClassicDeck(GetPlayer1().GetBackColor()));
            cardsPlayer2.AddRange(SpawnClassicDeck(GetPlayer2().GetBackColor()));
        }

        StartCoroutine(StartGame(.5f));
    }

    private IEnumerator StartGame(float delay = 1f) {
        yield return new WaitForSeconds(delay);

        ResetGame();

        RpcGameStart();

        // add p1 deck to player stack
        var p1main = GetStacksWhere(StackType.P1Owned | StackType.PlayerMain).First();
        p1main.TakeCards(cardsPlayer1);
        RpcPlaySound(GameSound.Pickup);
        yield return pause;

        p1main.Shuffle();
        yield return pause;

        // add p2 deck to player stack
        var p2main = GetStacksWhere(StackType.P2Owned | StackType.PlayerMain).First();
        p2main.TakeCards(cardsPlayer2);
        RpcPlaySound(GameSound.Pickup);
        yield return pause;

        p2main.Shuffle();
        yield return pause;

        // move 13 player1 cards
        var p1_13 = GetStacksWhere(StackType.P1Owned | StackType.Player13).First();
        for (int i = 0; i < 13; i++) {
            p1_13.TakeCard(p1main.GetTopCard(), true);
        }
        RpcPlaySound(GameSound.Pickup);
        yield return pause;

        // move 13 player2 cards
        var p2_13 = GetStacksWhere(StackType.P2Owned | StackType.Player13).First();
        for (int i = 0; i < 13; i++) {
            p2_13.TakeCard(p2main.GetTopCard(), true);
        }
        RpcPlaySound(GameSound.Pickup);
        yield return pause;

        // lay out 4 red cards to talon
        foreach (var p1g in GetStacksWhere(StackType.Game | StackType.P1Related)) {
            p1g.TakeCard(p1main.GetTopCard(), true);
            RpcPlaySound(GameSound.Move);
            yield return pause;
        }

        gameReady = true;
        NextTurn(GetPlayer2(), GetPlayer1());
    }

    [ClientRpc]
    private void RpcGameStart() {
        IngameUI.singleton.OnGameStart();
    }

    [Client]
    private void OnCurrentPlayerChanged(GameObject _1, GameObject _2) {
        if (IngameUI.singleton) IngameUI.singleton.RefreshKnockState();
    }

    [Server]
    internal void EndTurn() {
        DSIngameClient nextPlayer = GetOpponentPlayer();
        DSIngameClient currentPlayer = GetCurrentPlayer();
        currentPlayer.canEndTurn = false;

        //currentPlayer.ClearHand(true);
        knockMode = false;
        lastMoveServedMiddle = true;
        lastMovedCard = null;
        lastMove = null;

        if (turnCount == 1) {
            // lay out 4 p2 cards to talon after first turn
            gameReady = false;
            StartCoroutine(AfterFirstTurn(currentPlayer, nextPlayer));
        } else {
            NextTurn(currentPlayer, nextPlayer);
        }
    }

    private IEnumerator AfterFirstTurn(DSIngameClient currentPlayer, DSIngameClient nextPlayer) {
        var p2main = GetStacksWhere(StackType.PlayerMain | StackType.P2Owned).First();

        // lay out 4 red cards to talon
        foreach (var p2g in GetStacksWhere(StackType.Game | StackType.P2Related)) {
            p2g.TakeCard(p2main.GetTopCard(), true);
            RpcPlaySound(GameSound.Move);
            yield return pause;
        }

        gameReady = true;
        NextTurn(currentPlayer, nextPlayer);
    }

    [Server]
    private void NextTurn(DSIngameClient currentPlayer, DSIngameClient nextPlayer) {
        RemoveHighlights(currentPlayer);
        this.currentPlayer = nextPlayer.gameObject;

        turnCount++; // count turn count up by one

        //DSChatManager.singleton.RpcReceiveChatMessage($"{ChatUI.ColorizeText($"{nextPlayer.GetClientName()}'s", nextPlayer.GetChatColor())} turn.", null);
        IngameUI.singleton.RpcNextTurn(nextPlayer.netId);

        CheckCanEndTurn(currentPlayer);
        CheckCanEndTurn(nextPlayer);
    }

    [Command(requiresAuthority = false)]
    internal void CmdEndTurn(NetworkConnectionToClient conn = null) {
        if (conn.identity.TryGetComponent(out DSIngameClient player) && player.canEndTurn)
            EndTurn();
    }

    [Server]
    internal void ResetGame() {
        gameReady = false;
        currentPlayer = null;
        lastMoveServedMiddle = true;
        lastMovedCard = null;
        lastMove = null;
        turnCount = 0;
        moves = 0;
    }

    private Coroutine waitForEndGameCoroutine;

    [Server]
    internal void StageWin(DSIngameClient winner) {
        IngameUI.singleton.winner = winner ? winner.netId : null;
        if (waitForEndGameCoroutine != null) StopCoroutine(waitForEndGameCoroutine);
        if (winner) {
            DSChatManager.singleton.RpcHeadsup($"{ChatUI.ColorizeText(winner.GetChatName(), winner.GetChatColor())} wins.");
            waitForEndGameCoroutine = StartCoroutine(WaitForEndGame());
        }
    }

    [Server]
    internal IEnumerator WaitForEndGame() {
        float secondsLeft = 5f;
        while (secondsLeft >= 1f) {
            string msg = $"Game ends in {secondsLeft}...";
            DSChatManager.singleton.RpcReceiveChatMessage(msg, null);
            DSChatManager.singleton.RpcHeadsup(msg);
            yield return new WaitForSeconds(1f);
            secondsLeft -= 1f;
        }
        if (IngameUI.singleton.winner != null)
            EndGame();
    }

    internal void EndGame() {
        StopAllCoroutines();
        // end host/scene
        DSNetworkRoomManager.singleton.ServerChangeScene(RoomScene);
    }

    [Server]
    internal void RemoveHighlights(DSIngameClient player) {
        stacks.Values.Select(x => DSNetworkRoomManager.GetNetworkComponent<DSCardStackBehaviour>(x)).ToList()
            .ForEach(x => {
                Color color = player.GetChatColor();
                x.RpcUnhighlight(false, color); // top collider
                x.RpcUnhighlight(true, color); // bottom collider
            });
    }

    [ClientRpc]
    internal void RpcPlaySound(GameSound sound) {
        DSAudioManager.PlaySound(sound);
    }

    private DSCardBehaviour lastHand;

    [SyncVar] private int moves;

    [Command(requiresAuthority = false)]
    internal void CmdKnock(uint playerId) {
        DSIngameClient player = DSNetworkRoomManager.GetNetworkComponent<DSIngameClient>(playerId);
        if (!knockMode // not already knocking
            && !lastMoveServedMiddle // middle not served
            && (player != null)
            && lastMove.HasValue // FIXME: checked server only
            && IsReady() // game ready
            && !IsCurrentPlayer(player) // cant knock self
            && player.GetClientType().Equals(BetterNetworkClientType.Player) // is player // TODO: spectatorknock!
            && (turnCount > 0) // game started
                               // TODO ValidateKnock
        ) {
            knockMode = true;
            Move move = lastMove.Value;

            StageWin(null); // unset winner during knock

            RpcPlaySound(GameSound.Knock);

            // keep & clean last hand
            DSIngameClient currentPlayer = GetCurrentPlayer();
            if (currentPlayer.HasHand())
                lastHand = currentPlayer.GetMainHand();
            else
                lastHand = null;
            currentPlayer.ClearHand(true);

            // revert last move
            DSCardStackBehaviour targetStack = (/*move.type == MoveType.Return || */move.type == MoveType.Redraw || move.type == MoveType.Pickup) ? move.card.GetCurrentStack() : move.card.GetLastStack();
            if (targetStack != null) targetStack.RevertMove(move.type, move.player, move.card);
        }
    }

    [Command(requiresAuthority = false)]
    internal void CmdCancelKnock(uint playerId) {
        DSIngameClient player = DSNetworkRoomManager.GetNetworkComponent<DSIngameClient>(playerId);
        if (knockMode && !IsCurrentPlayer(player)) { // TODO ValidateCancelKnock
            knockMode = false;
            if (lastMove.HasValue) {
                Move move = lastMove.Value;

                // redo reverted last move
                DSCardStackBehaviour targetStack = (/*move.type == MoveType.Return || */move.type == MoveType.Pickup) ? move.card.GetCurrentStack() : move.card.GetLastStack();
                if (targetStack != null) targetStack.PerformMove(move.type, move.player, false, move.card);

                // regive last hand
                if (lastHand != null) GetCurrentPlayer().SetHand(lastHand);
            }
            player.ClearHand(true);
            RemoveHighlights(player);
        }
    }

    #endregion Server

    #region Spawner Functions

    [Server]
    private Dictionary<string, uint> SpawnCardStacks() {
        if (!isServer) return null;

        Dictionary<string, uint> result = new();
        foreach (KeyValuePair<string, CardStackDefinition> stackDefinition in DSTweakData.STACKS) {
            GameObject newStack = Instantiate(cardStackPrefab);

            newStack.name = stackDefinition.Key;

            if (newStack.TryGetComponent<DSCardStackBehaviour>(out var behaviour)) {
                behaviour.SetSyncedStackType(stackDefinition.Value.type);
                behaviour.SetSyncedRenderOffset(stackDefinition.Value.renderOffset);
                behaviour.SetSyncedIsBottomPlayable(stackDefinition.Value.isBottomPlayable);
            }

            if (newStack.TryGetComponent<Transform>(out var stackTransform)) {
                stackTransform.localPosition = new Vector3(stackDefinition.Value.position.x, stackDefinition.Value.position.y, 0);
            }

            newStack.SetActive(true);

            NetworkServer.Spawn(newStack);

            result.Add(newStack.name, newStack.GetComponent<NetworkIdentity>().netId);
        }
        return result;
    }

    [Server]
    private List<GameObject> SpawnClassicDeck(BackColor backColor) {
        if (!isServer) return null;

        List<GameObject> result = new();
        for (int suit = 1; suit <= 4; suit++) { // 4 suits
            for (int value = 1; value <= 13; value++) { // 13 values
                result.Add(SpawnCard(backColor, (Suit)suit, (Value)value));
            }
        }
        return result;
    }

    [Server]
    private GameObject SpawnCard(BackColor backColor, Suit suit, Value value, bool shown = false, bool active = true) {
        if (!isServer) return null;

        string cardName = DSResourceManager.GetCardSpriteAlias(suit, value);

        GameObject newCard = Instantiate(cardPrefab);

        newCard.name = cardName;

        if (newCard.TryGetComponent<DSCardBehaviour>(out var card)) {
            card.SetSyncedSuit(suit);
            card.SetSyncedValue(value);
            card.SetSyncedFrontSprite(cardName);
            card.SetSyncedBackColor(backColor);
            card.SetSyncedShown(shown);
        }

        if (newCard.TryGetComponent<Transform>(out var cardTransform)) {
            cardTransform.localScale = new Vector3(1, 1, 1);
        }

        newCard.SetActive(active);

        NetworkServer.Spawn(newCard);
        return newCard;
    }

    internal static DSIngameClient GetCurrentPlayer() {
        return singleton.currentPlayer != null ? singleton.currentPlayer.GetComponent<DSIngameClient>() : null;
    }

    internal static DSIngameClient GetOpponentPlayer() {
        return singleton.player1 == singleton.currentPlayer ? GetPlayer2() : GetPlayer1();
    }

    internal static bool IsCurrentPlayer(DSIngameClient player = null) {
        if (player == null) player = GetLocalPlayer();
        return player != null && player == GetCurrentPlayer();
    }

    [Client]
    internal static DSIngameClient GetLocalPlayer() {
        return NetworkClient.localPlayer ? NetworkClient.localPlayer.gameObject.GetComponent<DSIngameClient>() : null;
    }

    internal static DSIngameClient GetPlayer1() {
        return singleton.player1 != null ? singleton.player1.GetComponent<DSIngameClient>() : null;
    }

    internal static DSIngameClient GetPlayer2() {
        return singleton.player2 != null ? singleton.player2.GetComponent<DSIngameClient>() : null;
    }

    internal static void SetPlayer1(DSIngameClient player) {
        player.SetSyncedFirstTurn(true);
        singleton.player1 = player.gameObject;
    }

    internal static void SetPlayer2(DSIngameClient player) {
        player.SetSyncedFirstTurn(false);
        singleton.player2 = player.gameObject;
    }

    internal DSCardStackBehaviour GetFreeSlot() {
        List<DSCardStackBehaviour> stacks = GetStacksWhere(turnCount > 1 ? StackType.Game : StackType.Game | StackType.P1Related);
        foreach (DSCardStackBehaviour stack in stacks) {
            if (stack.IsEmpty())
                return stack;
        }
        return null;
    }

    internal bool IsHand(DSCardBehaviour card) {
        return (player1 && GetPlayer1().IsHand(card)) || (player2 && GetPlayer2().IsHand(card));
    }

    internal void RemovePlayer(DSIngameClient player) {
        if (players.Contains(player)) players.Remove(player);
    }

    [Server]
    internal void OnServerPlayerJoined(DSIngameClient player, bool isLateJoin) {
        string msg = $"{player.GetChatName()} has {(isLateJoin ? "late-" : string.Empty)}joined.";
        DSChatManager.singleton.RpcReceiveChatMessage(msg, null);
        DSChatManager.singleton.RpcHeadsup(msg);

        if (isLateJoin)
            stacks.Values.Select(x => DSNetworkRoomManager.GetNetworkComponent<DSCardStackBehaviour>(x)).ToList()
                .ForEach(x => player.OnLateJoinStackRefresh(x));
    }

    internal bool IsReady() {
        return fullyLoaded && GetPlayer1() != null && GetPlayer2() != null;
    }

    internal List<DSCardStackBehaviour> GetStacksWhere(StackType flag, bool orderByName = false) {
        IEnumerable<DSCardStackBehaviour> result = stacks.Values
            .Select(x => DSNetworkRoomManager.GetNetworkComponent<DSCardStackBehaviour>(x))
            .Where(x => x.GetStackType().HasFlag(flag));

        if (orderByName)
            result.OrderBy(x => x.name);

        return result.ToList();
    }

    internal DSCardStackBehaviour GetDropStackOf(DSIngameClient player) {
        return GetStacksWhere(StackType.PlayerDrop | (GetPlayer1() == player ? StackType.P1Owned : StackType.P2Owned)).FirstOrDefault();
    }

    #endregion Spawner Functions

    internal void CheckCanEndTurn(DSIngameClient player) {
        // check  canEndTurn (critical 13 stack state)
        player.canEndTurn = GetCurrentPlayer().Equals(player) && GetStacksWhere(player.Equals(GetPlayer1()) ? StackType.P1Owned : StackType.P2Owned)
            .TrueForAll(x => x.GetStackType().HasFlag(StackType.Player13) != x.IsEmpty());
    }

    internal void BumpMoves() {
        moves++;
    }

    internal int GetMoves() {
        return moves;
    }
}