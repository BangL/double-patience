using Assets.Scripts;
using Assets.Scripts.UI.Networked;
using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
	Documentation: https://mirror-networking.gitbook.io/docs/guides/networkbehaviour
	API Reference: https://mirror-networking.com/docs/api/Mirror.NetworkBehaviour.html
*/

// NOTE: Do not put objects in DontDestroyOnLoad (DDOL) in Awake.  You can do that in Start instead.

[AddComponentMenu("Double-Solitaire/DS Player")]
public class DSIngameClient : NetworkBehaviour, IDSClient {
    internal static readonly HashSet<string> playerNames = new();

    internal float handTaken;

    private Dictionary<DSCardBehaviour, bool> currentHand = new();

    private uint? lastMainHandId = null;

    [SyncVar] private BetterNetworkClientType syncedClientType = BetterNetworkClientType.Spectator;

    [SyncVar] private BackColor syncedBackColor = BackColor.Black;

    [SyncVar] private bool syncedFirstTurn = false;
    [SyncVar] private string syncedPlayerName;

    private float lastPosSent = 0;
    //private bool dragging = false;

    //private Vector3 dragOffset = new();

    private void Update() {
        //bool dragging = false;
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (isLocalPlayer && CanInteract() && HasHand()) {
            //dragging = true; // set each frame, while holding hand
            if (Time.time - lastPosSent >= .002f) { // just send every 2ms, not each frame
                lastPosSent = Time.time;
                CmdCursorPosition(mouseWorldPos);
            }
        }
        //if (dragging != this.dragging) { // only update on change
        //    this.dragging = dragging;
        //    if (dragging) { // only do once, at the beginning of a drag
        //        Vector3 handPos = currentHand.First(x => x.Value).Key.transform.position;
        //        dragOffset = new Vector3(
        //            mouseWorldPos.x - handPos.x,
        //            mouseWorldPos.y - handPos.y
        //        );
        //    }
        //}
    }

    [TargetRpc]
    internal void RpcPlaySound(GameSound sound) {
        if (!DSGameManager.singleton || !DSGameManager.singleton.IsReady()) return;

        DSAudioManager.PlaySound(sound);
    }

    [Command]
    private void CmdCursorPosition(Vector3 mousePosition) { //Position of player
        if (CanInteract() && HasHand())
            RpcMove(mousePosition);
    }

    [ClientRpc]
    private void RpcMove(Vector3 mousePosition) {
        if (CanInteract() && HasHand()) {
            int i = 0;
            float centerOffsetRatio = MathF.Abs(mousePosition.x) / GameObject.Find("Game2").transform.position.x;
            foreach (var x in currentHand.Keys) {
                x.SetHandPosition(new Vector3(
                    mousePosition.x + (i * centerOffsetRatio * (mousePosition.x < 0 ? -DSTweakData.X_OFFSET : DSTweakData.X_OFFSET))/* - dragOffset.x*/,
                    mousePosition.y/* - dragOffset.y*/ + (5 * (Equals(DSGameManager.GetPlayer2()) ? 1 : -1))
                ));
                i++;
            }
        }
    }

    #region Start & Stop Callbacks

    /// <summary>
    /// This is invoked for NetworkBehaviour objects when they become active on the server.
    /// <para>This could be triggered by NetworkServer.Listen() for objects in the scene, or by NetworkServer.Spawn() for objects that are dynamically created.</para>
    /// <para>This will be called for objects on a "host" as well as for object on a dedicated server.</para>
    /// </summary>
    public override void OnStartServer() {
        //name = base.name;
        syncedPlayerName = ((DSAuthData)connectionToClient.authenticationData).clientName;
    }

    /// <summary>
    /// Invoked on the server when the object is unspawned
    /// <para>Useful for saving object data in persistent storage</para>
    /// </summary>
    //public override void OnStopServer() { }

    /// <summary>
    /// Called on every NetworkBehaviour when it is activated on a client.
    /// <para>Objects on the host have this function called, as there is a local client on the host. The values of SyncVars on object are guaranteed to be initialized correctly with the latest state from the server when this function is called on the client.</para>
    /// </summary>
    //public override void OnStartClient() { }

    /// <summary>
    /// This is invoked on clients when the server has caused this object to be destroyed.
    /// <para>This can be used as a hook to invoke effects or do client specific cleanup.</para>
    /// </summary>
    //public override void OnStopClient() { }

    /// <summary>
    /// Called when the local player object has been set up.
    /// <para>This happens after OnStartClient(), as it is triggered by an ownership message from the server. This is an appropriate place to activate components or functionality that should only be active for the local player, such as cameras and input.</para>
    /// </summary>
    public override void OnStartLocalPlayer() {
        //ChatUI.localPlayerName = syncedPlayerName;
    }

    /// <summary>
    /// Called when the local player object is being stopped.
    /// <para>This happens before OnStopClient(), as it may be triggered by an ownership message from the server, or because the player object is being destroyed. This is an appropriate place to deactivate components or functionality that should only be active for the local player, such as cameras and input.</para>
    /// </summary>
    //public override void OnStopLocalPlayer() { }

    /// <summary>
    /// This is invoked on behaviours that have authority, based on context and <see cref="NetworkIdentity.hasAuthority">NetworkIdentity.hasAuthority</see>.
    /// <para>This is called after <see cref="OnStartServer">OnStartServer</see> and before <see cref="OnStartClient">OnStartClient.</see></para>
    /// <para>When <see cref="NetworkIdentity.AssignClientAuthority">AssignClientAuthority</see> is called on the server, this will be called on the client that owns the object. When an object is spawned with <see cref="NetworkServer.Spawn">NetworkServer.Spawn</see> with a NetworkConnectionToClient parameter included, this will be called on the client that owns the object.</para>
    /// </summary>
    //public override void OnStartAuthority() { }

    /// <summary>
    /// This is invoked on behaviours when authority is removed.
    /// <para>When NetworkIdentity.RemoveClientAuthority is called on the server, this will be called on the client that owns the object.</para>
    /// </summary>
    //public override void OnStopAuthority() { }

    #endregion Start & Stop Callbacks

    #region General Helper Functions

    public string GetClientName() {
        return syncedPlayerName;
    }

    public string GetChatName() {
        string affix = string.Empty;
        if (syncedClientType != BetterNetworkClientType.Player)
            affix = " (Spectator)";
        return syncedPlayerName + affix;
    }

    public BackColor GetBackColor() {
        return syncedBackColor;
    }

    public Color GetChatColor(float alpha = 1f) {
        return syncedBackColor switch {
            BackColor.Black => new Color(0, 0, 0, alpha),
            BackColor.Blue => new Color(0, 0, 1, alpha),
            BackColor.Red => new Color(1, 0, 0, alpha),
            _ => throw new NotImplementedException($"unimplemented backcolor type: {syncedBackColor}"),
        };
    }

    public BetterNetworkClientType GetClientType() {
        return syncedClientType;
    }

    public bool IsFirst() {
        return syncedFirstTurn;
    }

    internal bool CanInteract() {
        return (IngameUI.singleton != null) && DSGameManager.singleton.gameReady // game is ready
            && syncedClientType.Equals(BetterNetworkClientType.Player)
            && (DSGameManager.GetPlayer1() == this || DSGameManager.GetPlayer2() == this) // and is not spectator
            && (DSGameManager.IsCurrentPlayer(this) != DSGameManager.singleton.knockMode) // and is current player while not knocking OR inactive player while knocking
            && (!IngameUI.singleton.lockedControls)
        ;
    }

    #endregion General Helper Functions

    #region Hand

    [Command]
    internal void CmdClearHand() {
        // cannot act if not players turn
        if (!CanInteract()) {
            Debug.Log("cannot act if not my turn");
            return;
        }

        ClearHand();
    }

    [Server]
    internal void ClearHand(bool force = false) {
        bool canReturn = false;
        if (!force && HasHand()) {
            DSCardBehaviour hand = GetMainHand();
            DSCardStackBehaviour stack = hand.GetCurrentStack();
            canReturn = !stack.GetStackType().HasFlag(StackType.PlayerMain);
        }
        if (canReturn || force) {
            //Debug.Log((force ? "Force clear" : "Clear") + " hand");
            SetHand(null);
        }
    }

    [Server]
    internal void SetHand(DSCardBehaviour mainCard) {
        List<uint> cards = null;

        if (mainCard != null) {
            cards = new();
            // collect card netIds
            DSCardStackBehaviour stack = mainCard.GetCurrentStack();
            int startIndex = stack.IndexOf(mainCard);
            int length = stack.GetCardCount() - startIndex;
            for (int i = 0; i < length; i++) {
                DSCardBehaviour card = stack.GetCardAt(startIndex + i);
                cards.Add(card.netId);
            }
        }

        uint? newMainCardId = mainCard ? mainCard.netId : null;
        if (!lastMainHandId.Equals(newMainCardId)) {
            uint? lastId = null;
            if (lastMainHandId.HasValue) lastId = lastMainHandId.Value;
            lastMainHandId = newMainCardId;
            // send to clients
            if (isServerOnly) SetCurrentHand(cards, newMainCardId);
            RpcHandChanged(netId, cards, newMainCardId);
            if (lastId.HasValue) DSNetworkRoomManager.GetNetworkComponent<DSCardBehaviour>(lastId.Value).GetCurrentStack().RpcCardsUpdated();
        }
    }

    private void SetCurrentHand(List<uint> fullStack, uint? mainHandId) {
        currentHand.Clear(); // clear && update
        if (mainHandId.HasValue && (fullStack != null)) {
            currentHand = fullStack.ToDictionary(
                netId => DSNetworkRoomManager.GetNetworkComponent<DSCardBehaviour>(netId),
                netId => netId == mainHandId.Value
            );
        }
    }

    [ClientRpc]
    private void RpcHandChanged(uint playerId, List<uint> fullStack, uint? mainHandId) {
        if (netId != playerId) Debug.LogError("Fucked up hand comms! wrong player received wrong hand!");
        Dictionary<DSCardBehaviour, bool> oldHand = new(currentHand); // clone current hand
        SetCurrentHand(fullStack, mainHandId);
        HandleHandChanged(oldHand, false); // do unzoom with old clone
        HandleHandChanged(currentHand, true); // do zoom with current hand
    }

    [Client]
    private void HandleHandChanged(IDictionary<DSCardBehaviour, bool> cards, bool focus) {
        int count = 0;
        DSCardBehaviour mainCard = null;
        foreach (KeyValuePair<DSCardBehaviour, bool> card in cards) {
            if (card.Value)
                mainCard = card.Key;

            card.Key.Focus(focus/*, count*/);

            count++;
        }

        // flip locally, if from own main stack
        if (mainCard != null && focus) {
            DSCardStackBehaviour stack = mainCard.GetCurrentStack();
            if (stack.GetStackType().HasFlag(StackType.PlayerMain) && stack.IsPlayerStackOf(DSGameManager.GetLocalPlayer())) {
                mainCard.SetShown(true);
            }
        }

        Debug.Log($"hand changed. {(focus ? string.Empty : "un")}zoomed {count} cards.");
    }

    internal DSCardBehaviour GetMainHand() {
        return currentHand.FirstOrDefault((KeyValuePair<DSCardBehaviour, bool> x) => x.Value).Key;
    }

    internal bool HandIsStack() {
        return currentHand.Count > 1;
    }

    internal IDictionary<DSCardBehaviour, bool> GetHand() {
        return currentHand;
    }

    internal bool HasHand() {
        return currentHand.Count > 0;
    }

    internal bool IsHand(DSCardBehaviour cardBehaviour) {
        return currentHand.ContainsKey(cardBehaviour);
    }

    #endregion Hand

    // RuntimeInitializeOnLoadMethod -> fast playmode without domain reload
    [RuntimeInitializeOnLoadMethod]
    private static void ResetStatics() {
        playerNames.Clear();
    }

    [TargetRpc]
    internal void OnPlayerFullyJoined(bool isP2) {
        // p2 camera setup
        if (isP2) {
            // Client
            Camera.main.transform.Rotate(new Vector3(0, 0, 180));
        }

        IngameUI.singleton.RefreshKnockState();
    }

    [TargetRpc]
    internal void OnLateJoinStackRefresh(DSCardStackBehaviour stack) {
        stack.Refresh();
    }

    internal void SetSyncedFirstTurn(bool value) {
        syncedFirstTurn = value;
    }

    internal void SetSyncedBackColor(BackColor backColor) {
        syncedBackColor = backColor;
    }

    internal void SetSyncedClientType(BetterNetworkClientType playerType) {
        syncedClientType = playerType;
    }

    [SyncVar(hook = nameof(CanEndTurnChanged))] internal bool canEndTurn;

    internal void CanEndTurnChanged(bool _, bool canEndturn) {
        if (IngameUI.singleton != null)
            IngameUI.singleton.endTurnButton.gameObject.SetActive(canEndturn && DSGameManager.IsCurrentPlayer(this) && isLocalPlayer);
    }
}