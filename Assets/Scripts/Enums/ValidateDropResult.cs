﻿using System;

[Flags]
public enum ValidateDropResult {
    Null = 0,
    Cancel = 1,
    Drop = 2,
    DropStack = 4,
    EndTurn = 8,
    Return = 16,
    ServeMiddle = 32,
    Slip = 64,
    SlipStack = 128,
}