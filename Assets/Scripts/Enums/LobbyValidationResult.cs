﻿using System;

[Flags]
public enum LobbyValidationResult {
    Empty = 0,

    PlayerCountGood = 1,
    PlayerCountNotEnough = 2,
    PlayerCountTooMany = 4,

    FirstCountGood = 8,
    FirstCountNotEnough = 16,
    FirstCountTooMany = 32,

    SameColorCountGood = 64,
    SameColorCountTooMany = 128,
}