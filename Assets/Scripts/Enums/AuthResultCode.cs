﻿public enum AuthResultCode {
    Success = 100,
    RequestAlreadyPending = 200,
    MajorVersionMismatch = 201,
    MinorVersionMismatch = 202,
    ClientNameAlreadyInUse = 203,
    MissingParams = 204,
}