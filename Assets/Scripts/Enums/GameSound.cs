﻿public enum GameSound {
    Pickup,
    Move,
    Error,
    Knock,
    Winner,
    Loser,
    YourTurn,
    Chat,
    Shuffle,
}