﻿using System;

[Flags]
public enum StackType {
    PlayerMain = 1,
    PlayerDrop = 2,
    Player13 = 4,

    Middle = 8,

    Game = 16,

    P1Related = 32,
    P2Related = 64,

    P1Owned = 128,
    P2Owned = 256,

    POwned = 512,
}