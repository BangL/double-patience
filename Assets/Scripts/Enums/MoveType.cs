﻿public enum MoveType {
    Pickup = 1,
    Return = 2,
    MoveCard = 3,
    MoveStack = 4,
    Redraw = 5,
    Slip = 6,
    SlipStack = 7,
}