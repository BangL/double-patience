﻿using UnityEngine;

internal interface IDSClient {

    public string GetClientName();

    public string GetChatName();

    public BetterNetworkClientType GetClientType();

    public BackColor GetBackColor();

    public Color GetChatColor(float alpha = 1f);

    public bool IsFirst();
}