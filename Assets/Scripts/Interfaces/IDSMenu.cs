﻿using System;
using System.Collections.Generic;

public struct MenuPage {
    public Enum page;
    public Func<bool> canShow;
}

public interface IMenu {

    public List<MenuPage> GetPages();

    public List<IMenuEntry> GetPage(Enum page);
}