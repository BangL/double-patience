using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(TMP_Dropdown))]
[DisallowMultipleComponent]
public class DropDownController : MonoBehaviour, IPointerClickHandler {

    [Tooltip("Indexes that should be ignored. 0 based.")]
    public List<int> disabledIndexes = new();

    private TMP_Dropdown _dropdown;

    private void Awake() {
        _dropdown = GetComponent<TMP_Dropdown>();
    }

    public void OnPointerClick(PointerEventData eventData) {
        var dropDownList = GetComponentInChildren<Canvas>();
        if (!dropDownList) return;
        var toggles = dropDownList.GetComponentsInChildren<Toggle>(true);
        for (var i = 1; i < toggles.Length; i++) {
            bool enable = !disabledIndexes.Contains(i - 1);
            toggles[i].interactable = enable;
            toggles[i].SetIsOnWithoutNotify(enable);
        }
    }

    public void EnableOption(int index, bool enable) {
        if (index < 0 || index >= _dropdown.options.Count) {
            Debug.LogWarning($"Index out of range -> ignored! {index}/1-{_dropdown.options.Count - 1}", this);
            return;
        }
        if (enable && disabledIndexes.Contains(index)) disabledIndexes.Remove(index);
        else if (!enable && !disabledIndexes.Contains(index)) disabledIndexes.Add(index);
        var dropDownList = GetComponentInChildren<Canvas>();
        if (!dropDownList) return;
        var toggles = dropDownList.GetComponentsInChildren<Toggle>(true);
        toggles[index + 1].interactable = enable;
    }

    public void EnableOption(string label, bool enable) {
        var index = _dropdown.options.FindIndex(o => string.Equals(o.text, label)) - 1;
        EnableOption(index, enable);
    }
}