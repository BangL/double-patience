﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[DisallowMultipleComponent]
[AddComponentMenu("Double-Solitaire/DS Chat UI")]
public class ChatUI : MonoBehaviour {
    internal static ChatUI singleton;

    private CanvasGroup scrollbarPanel;

    private bool pressingEscape = false;

    [Header("UI Elements")]
    [SerializeField] private Text chatHistory;

    [SerializeField] private Scrollbar scrollbar;

    [SerializeField] private InputField chatMessage;

    [SerializeField] private Button sendButton;

    [SerializeField] private Image chatBackground;

    [SerializeField] private CanvasGroup chatInput;

    [SerializeField] private Text typingText;

    [Header("Scenes")]
    [Scene]
    [SerializeField] private string offlineScene;

    [Scene]
    [SerializeField] private string lobbyScene;

    // ======

    #region Startup / Singleton / DDOL

    private void Awake() {
        if (singleton != null)
            Destroy(singleton); // destroy old instance
        singleton = this;
        gameObject.SetActive(false);
    }

    private void Start() {
        scrollbar.TryGetComponent(out scrollbarPanel);
        chatHistory.text = string.Empty;
        FocusChat(false);
    }

    #endregion Startup / Singleton / DDOL

    #region KeyHandler

    private void OnGUI() {
        Event currentEvent = Event.current;
        EventType eventType = currentEvent.GetTypeForControl(GetInstanceID());

        if (eventType == EventType.KeyDown || eventType == EventType.KeyUp) HandleKeyEvents(currentEvent);
    }

    private void HandleKeyEvents(Event currentEvent) {
        if (isFocused) {
            // chat open

            // escape chat
            if (Input.GetKeyDown(KeyCode.Escape)) {
                if (!pressingEscape) {
                    pressingEscape = true;

                    // make sure the buttons no controls are selected to prevent double trigger
                    EventSystem.current.SetSelectedGameObject(null, null);

                    FocusChat(false); // ingame only
                }
                currentEvent.Use();
            } else if (pressingEscape) {
                pressingEscape = false;
                currentEvent.Use();
            }
        }
    }

    #endregion KeyHandler

    #region Message Transport

    [Client]
    public void SendChatMessage() {
        if (!NetworkClient.active || NetworkClient.localPlayer == null) return;

        if (!string.IsNullOrWhiteSpace(chatMessage.text)) {
            DSChatManager.singleton.CmdBroadcastChatMessage(chatMessage.text.Trim());
            chatMessage.text = string.Empty;
            chatMessage.ActivateInputField();
        } else { // ingame only
            FocusChat(false); // leave when empty
        }
    }

    [Client]
    internal void ReceiveChatMessage(string message, uint? senderId) {
        if (
            senderId.HasValue
            && NetworkClient.spawned.TryGetValue(senderId.Value, out NetworkIdentity identity)
        ) {
            var player = identity.GetComponent<IDSClient>();
            AppendMessage(ColorizeText($"{ColorizeText($"{player.GetChatName()}:", player.GetChatColor())} {message}", Color.white));
            DSAudioManager.PlaySound(GameSound.Chat);
        } else {
            // system message
            AppendMessage(ColorizeText($"Server: {message}", Color.gray));
        }
    }

    #endregion Message Transport

    #region Is typing... Transport

    private readonly List<string> typingNames = new();

    private Coroutine typingCoroutine;

    [Client]
    private void SendTypingChanged(bool typing) {
        if (!NetworkClient.active || NetworkClient.localPlayer == null) return;

        if (typingCoroutine != null) StopCoroutine(typingCoroutine);
        if (typing) typingCoroutine = StartCoroutine(StartedTyping());
        DSChatManager.singleton.CmdTypingChanged(typing);
    }

    [Client]
    private IEnumerator StartedTyping() {
        yield return new WaitForSeconds(1);
        SendTypingChanged(false);
    }

    [Client]
    internal void ReceiveTypingChanged(bool typing, uint senderId) {
        if (NetworkClient.spawned.TryGetValue(senderId, out NetworkIdentity identity)) {
            string clientName = GetClientName(identity);
            if (!string.IsNullOrEmpty(clientName)) {
                if (typing && !typingNames.Contains(clientName)) typingNames.Add(clientName);
                if (!typing && typingNames.Contains(clientName)) typingNames.Remove(clientName);
                StartCoroutine(OnTypingChanged(typingNames));
            }
        }
    }

    [Client]
    private IEnumerator OnTypingChanged(List<string> typingNames) {
        typingText.text = $"{string.Join(", ", typingNames)} {(typingNames.Count == 1 ? "is" : "are")} typing...";
        typingText.gameObject.SetActive(typingNames.Count > 0);
        yield return null;
        yield return null;
        if (typingNames.Count > 0) scrollbar.value = 0;
    }

    #endregion Is typing... Transport

    #region UI Event Handlers

    [Client]
    public void OnMessageValueChanged(string input) {
        bool hasText = !string.IsNullOrWhiteSpace(input);
        sendButton.interactable = hasText;
        SendTypingChanged(hasText);
    }

    [Client]
    public void OnMessageSubmit(string _) {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Submit"))
            SendChatMessage();
    }

    [Client]
    public void OnMessageEndEdit(string _) {
        if (!Input.GetKeyDown(KeyCode.Return) && !Input.GetKeyDown(KeyCode.KeypadEnter) && !Input.GetButtonDown("Submit"))
            FocusChat(false); // hide on leave
    }

    [Client]
    public void OnSendButtonClick() {
        SendChatMessage();
    }

    #endregion UI Event Handlers

    #region Helper

    private string GetClientName(NetworkIdentity identity) {
        string clientName = null;
        if (identity.gameObject.TryGetComponent<IDSClient>(out var roomPlayer))
            clientName = roomPlayer.GetChatName();
        return clientName;
    }

    internal bool isFocused;

    [Client]
    public void FocusChat(bool show) {
        bool inLobby = SceneManager.GetActiveScene().path.Equals(lobbyScene);

        //if (inLobby) chatMessage.text = string.Empty; // clear on hide

        chatBackground.color = new Color(chatBackground.color.r, chatBackground.color.g, chatBackground.color.b, (inLobby) ? 1f : .4f);
        scrollbarPanel.gameObject.SetActive(show || inLobby);
        chatInput.gameObject.SetActive(show || inLobby);
        if (!EventSystem.current.alreadySelecting)
            EventSystem.current.SetSelectedGameObject(show ? chatMessage.gameObject : gameObject, null);

        isFocused = show;
    }

    [Client]
    private void AppendMessage(string message) {
        StartCoroutine(AppendAndScroll(message));
    }

    [Client]
    private IEnumerator AppendAndScroll(string message) {
        chatHistory.text += message + "\n";

        // it takes 2 frames for the UI to update ?!?!
        yield return null;
        yield return null;

        // slam the scrollbar down
        scrollbar.value = 0;
    }

    internal static string ColorizeText(string text, Color color) {
        return $"<color={color.ToHtml()}>{text}</color>";
    }

    #endregion Helper
}