using UnityEngine;

namespace Assets.Scripts.UI.Networked {

    public class LobbyUI : MonoBehaviour {

        private void OnGUI() {
            Event currentEvent = Event.current;
            EventType eventType = currentEvent.GetTypeForControl(GetInstanceID());

            if (eventType == EventType.KeyDown || eventType == EventType.KeyUp) HandleKeyEvents(currentEvent);
        }

        private bool pressingEnter;

        private void HandleKeyEvents(Event currentEvent) {
            if (!ChatUI.singleton.isFocused) {
                // return/enter > focus chat
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) {
                    if (!pressingEnter) {
                        pressingEnter = true;

                        ChatUI.singleton.FocusChat(true);
                    }
                    currentEvent.Use();
                } else if (pressingEnter) {
                    pressingEnter = false;
                    currentEvent.Use();
                }
            }
        }
    }
}