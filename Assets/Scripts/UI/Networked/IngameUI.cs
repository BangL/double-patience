using Assets.Scripts.Core;
using Mirror;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Networked
{

    [DisallowMultipleComponent]
    [AddComponentMenu("Double-Solitaire/DS Ingame UI")]
    public class IngameUI : NetworkBehaviour
    {

        [Header("UI Elements")]
        //[SerializeField] internal Text headsupText;

        [SerializeField] internal Button knockButton;

        [SerializeField] internal Button cancelKnockButton;

        [SerializeField] internal Button endTurnButton;

        [SyncVar(hook = nameof(OnWinnerChanged))]
        internal uint? winner;

        internal static IngameUI singleton;

        private bool pressingEnter = false;
        private bool pressingSpace = false;

        internal bool lockedControls;

        public Image backgroundImage;

        private void Awake()
        {
            singleton = this;
        }

        private void Start()
        {
            LoadResources();
            backgroundImage.sprite = DSResourceManager.singleton.GetResource<Sprite>(((TableTextures)PlayerPrefs.GetInt("TableTexture", (int)TableTextures.WoodTable)).ToString());
        }

        internal void LoadResources()
        {
            // table textures
            foreach (TableTextures tableTexture in Enum.GetValues(typeof(TableTextures)))
            {
                string textureName = tableTexture.ToString();
                DSResourceManager.singleton.PrecacheResource<Sprite>(textureName, textureName);
            }
        }

        public override void OnStartClient()
        {
            knockButton.gameObject.SetActive(false);
        }

        private void OnGUI()
        {
            Event currentEvent = Event.current;
            EventType eventType = currentEvent.GetTypeForControl(GetInstanceID());

            if (eventType == EventType.KeyDown || eventType == EventType.KeyUp) HandleKeyEvents(currentEvent);
        }

        private void HandleKeyEvents(Event currentEvent)
        {
            if (lockedControls) return;

            if (!ChatUI.singleton.isFocused)
            {
                // space > knock
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (!pressingSpace)
                    {
                        pressingSpace = true;

                        // make sure the buttons no controls are selected to prevent double trigger
                        EventSystem.current.SetSelectedGameObject(null, null);

                        if (!DSGameManager.singleton.knockMode)
                        {
                            OnKnockButtonClick(); // knock
                        } else
                        {
                            OnCancelKnockButtonClick(); // cancel knock
                        }
                    }
                    currentEvent.Use();
                } else if (pressingSpace)
                {
                    pressingSpace = false;
                    currentEvent.Use();
                }

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    // make sure the buttons no controls are selected to prevent double trigger
                    EventSystem.current.SetSelectedGameObject(null, null);

                    IngameMenu.singleton.gameObject.SetActive(true);
                    lockedControls = true;

                    currentEvent.Use();
                }

                if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
                {
                    if (!pressingEnter)
                    {
                        pressingEnter = true;

                        ChatUI.singleton.FocusChat(true);
                    }
                    currentEvent.Use();
                } else if (pressingEnter)
                {
                    pressingEnter = false;
                    currentEvent.Use();
                }
            }
        }

        private void OnWinnerChanged(uint? _, uint? winnerId)
        {
            if (
                winnerId.HasValue
                && NetworkClient.spawned.TryGetValue(winnerId.Value, out NetworkIdentity identity)
                && identity.TryGetComponent(out DSIngameClient winner)
            )
            {
                DSIngameClient player = DSGameManager.GetLocalPlayer();
                if (winner == player || player.GetClientType().Equals(BetterNetworkClientType.Spectator))
                {
                    DSAudioManager.PlaySound(GameSound.Winner);
                } else
                {
                    DSAudioManager.PlaySound(GameSound.Loser);
                }
            }
        }

        internal void RefreshKnockState()
        {
            DSGameManager game = DSGameManager.singleton;
            if (game == null/* || !game.IsReady()*/) return; // spectator crashfix

            DSIngameClient player = DSGameManager.GetLocalPlayer();
            knockButton.gameObject.SetActive(
                (player != null)
                && player.GetClientType().Equals(BetterNetworkClientType.Player)
                && (game.turnCount > 0) // game started
                && (!DSGameManager.IsCurrentPlayer(player)) // not my turn
                && (!game.knockMode) // not already knocking
                && game.IsReady() // checked above
            );
            knockButton.interactable = !game.lastMoveServedMiddle; // middle not served
            // TODO: ValidateKnock
        }

        public void OnKnockButtonClick()
        {
            Debug.Log("Knock button click");
            if (!DSGameManager.singleton.knockMode)
            {
                DSGameManager.singleton.CmdKnock(DSGameManager.GetLocalPlayer().netId);
            }
        }

        public void OnCancelKnockButtonClick()
        {
            Debug.Log("Cancel knock button click");
            if (DSGameManager.singleton.knockMode)
            {
                DSGameManager.singleton.CmdCancelKnock(DSGameManager.GetLocalPlayer().netId);
            }
        }

        public void OnEndTurnButtonClick()
        {

            DSGameManager.singleton.CmdEndTurn();
        }

        internal void OnGameStart()
        {
            //DSGameManager.PlaySound(GameSound.Shuffle);
        }

        [ClientRpc]
        internal void RpcNextTurn(uint netId)
        {
            DSIngameClient player = DSNetworkRoomManager.GetNetworkComponent<DSIngameClient>(netId);
            if (player)
            {
                if (player == DSGameManager.GetLocalPlayer()) DSAudioManager.PlaySound(GameSound.YourTurn);
                PopupManager.Instance.AddPopup(ChatUI.ColorizeText($"{player.GetClientName()}'s turn", player.GetChatColor()));
            }
        }
    }
}