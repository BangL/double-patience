using Assets.Scripts;
using Assets.Scripts.UI.Networked;
using Mirror;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class IngameMenu : MonoBehaviour {
    internal static IngameMenu singleton;

    public Button leaveGameButton;
    public Button returnToLobbyButton;
    public Button stopServerButton;
    public Button closeMenuButton;

    private void Awake() {
        if (singleton != null)
            Destroy(singleton.gameObject);
        singleton = this;
        gameObject.SetActive(false);
    }

    private void Start() {
        leaveGameButton.interactable = NetworkClient.isConnected && !NetworkServer.active;
        returnToLobbyButton.interactable = NetworkServer.active;
        stopServerButton.interactable = NetworkServer.active;
    }

    private void OnGUI() {
        Event currentEvent = Event.current;
        EventType eventType = currentEvent.GetTypeForControl(GetInstanceID());

        if (eventType == EventType.KeyDown || eventType == EventType.KeyUp) HandleKeyEvents(currentEvent);
    }

    private void HandleKeyEvents(Event currentEvent) {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            // make sure the buttons no controls are selected to prevent double trigger
            EventSystem.current.SetSelectedGameObject(null, null);
            OnCloseMenuButtonClick();
            currentEvent.Use();
        }
    }

    public void OnLeaveGameButtonClick() {
        if (NetworkClient.isConnected)
            DSNetworkRoomManager.singleton.StopClient();
    }

    public void OnReturnToLobbyButtonClick() {
        if (NetworkServer.active && (DSGameManager.singleton != null))
            DSGameManager.singleton.EndGame();
    }

    public void OnStopServerButtonClick() {
        if (NetworkServer.active) {
            if (NetworkClient.isConnected)
                DSNetworkRoomManager.singleton.StopHost(); // stop host if host mode
            else
                DSNetworkRoomManager.singleton.StopServer();  // stop server if server-only
        }
    }

    public void OnCloseMenuButtonClick() {
        IngameUI.singleton.lockedControls = false;
        EventSystem.current.SetSelectedGameObject(null, null);
        gameObject.SetActive(false);
    }
}