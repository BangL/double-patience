using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {
    internal static SettingsMenu singleton;

    public AudioMixer mixer;
    public Slider masterVolumeSlider;

    private void Awake() {
        if (singleton != null)
            Destroy(singleton.gameObject);
        singleton = this;
    }

    private bool isProg = false;

    private void OnEnable() {
        mixer.GetFloat("MasterVolume", out float masterVolume); // get current from mixer

        isProg = true;
        masterVolumeSlider.value = masterVolume; // set current to slider
        isProg = false;
    }

    public void OnMasterVolumeSliderChange(float volume) {
        if (isProg) return;

        mixer.SetFloat("MasterVolume", volume); // set new
        DSAudioManager.PlaySound(GameSound.Error);

        PlayerPrefs.SetFloat("MasterVolume", volume); // save new
        PlayerPrefs.Save();
    }
}