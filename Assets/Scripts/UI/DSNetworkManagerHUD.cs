using Assets.Scripts;
using Assets.Scripts.UI;
using Assets.Scripts.UI.Networked;
using Mirror;
using System.Linq;
using UnityEngine;

/// <summary>Shows NetworkManager controls in a GUI at runtime.</summary>
[DisallowMultipleComponent]
[AddComponentMenu("Double-Solitaire/DS Network Manager HUD")]
[RequireComponent(typeof(NetworkManager))]
[HelpURL("https://mirror-networking.gitbook.io/docs/components/network-manager-hud")]
public class DSNetworkManagerHUD : MonoBehaviour {
    public static DSNetworkManagerHUD singleton;

    private NetworkManager manager;

    private Rect statusRect;

    private void Awake() {
        if (!singleton)
            singleton = this;
    }

    private void Start() {
        manager = GetComponent<NetworkManager>();
        statusRect = new Rect(10, 10, DSTweakData.STATUS_WIDTH, DSTweakData.STATUS_HEIGHT * 2);
    }

    private void OnGUI() {
        GUILayout.BeginArea(statusRect);
        DrawStatus();
        GUILayout.EndArea();
    }

    private void DrawStatus() {
        // game title / version
        GUILayout.Label($"<b>Double-Solitaire</b> v{DSTweakData.VERSION}");

        // host/server/client mode
        if (NetworkServer.active && NetworkClient.active) {
            // host
            GUILayout.Label($"<b>Host</b> running via {Transport.active}");
        } else if (NetworkServer.active) {
            // server only
            GUILayout.Label($"<b>Server</b> running via {Transport.active}");
        } else if (NetworkClient.isConnected) {
            // client only, connected
            GUILayout.Label($"<b>Client</b> connected to {manager.networkAddress} via {Transport.active}");
        }

        DSGameManager game = DSGameManager.singleton;
        if (game != null) {
            string gameState = ChatUI.ColorizeText("Locked", Color.gray);
            if (game.IsReady()) {
                DSIngameClient currentPlayer = DSGameManager.GetCurrentPlayer();
                DSIngameClient knockPlayer = DSGameManager.GetOpponentPlayer();
                if (currentPlayer != null && knockPlayer != null) {
                    if (IngameUI.singleton.winner.HasValue) {
                        DSIngameClient winner = DSNetworkRoomManager.GetNetworkComponent<DSIngameClient>(IngameUI.singleton.winner.Value);
                        gameState = ChatUI.ColorizeText($"{winner.GetChatName()} won", winner.GetChatColor());
                    } else if (game.knockMode) {
                        gameState = ChatUI.ColorizeText($"{knockPlayer.GetChatName()} knocking", knockPlayer.GetChatColor());
                    } else {
                        gameState = ChatUI.ColorizeText($"{currentPlayer.GetChatName()}'s turn", currentPlayer.GetChatColor());
                    }
                    //} else {
                    //    gameState = ChatUI.ColorizeText("Waiting for players...", Color.gray);
                }
            }

            GUILayout.Space(20);
            GUILayout.Label($"<b>Status</b>: {gameState}");
            if (game.turnCount > 0) {
                GUILayout.Label($"<b>Moves</b>: {game.GetMoves()}");
                GUILayout.Label($"<b>Turns</b>: {game.turnCount - 1}");

                // players
                GUILayout.Space(20);
                GUILayout.Label($"<b>Players</b>:");
                DSIngameClient player1 = DSGameManager.GetPlayer1();
                DSIngameClient player2 = DSGameManager.GetPlayer2();
                if (player1 != null && player2 != null) {
                    GUILayout.Label(ChatUI.ColorizeText($"{player1.GetChatName()}", player1.GetChatColor()));
                    GUILayout.Label(ChatUI.ColorizeText($"{player2.GetChatName()}", player2.GetChatColor()));
                }

                // spectators
                GUILayout.Space(20);
                GUILayout.Label($"<b>Spectators</b>:");
                DSNetworkRoomManager.singleton.roomSlots.Select(x => x.GetComponent<IDSClient>()).Where(x => x.GetClientType().Equals(BetterNetworkClientType.Spectator))
                    .ToList().ForEach(x => {
                        GUILayout.Label(ChatUI.ColorizeText($"{x.GetChatName()}", Color.gray));
                    });
            }
        }
    }
}