﻿using UnityEngine;

internal struct CardStackDefinition {

    // position of the stack
    public Vector2 position;

    // stack type (for game rules)
    public StackType type;

    // render offset (for the stacked cards)
    public Vector2 renderOffset;

    // can move the entire stack? (for talon)
    public bool isBottomPlayable;
}