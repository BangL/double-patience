﻿using Mirror;

public struct DSAuthResponse : NetworkMessage {
    public AuthResultCode code;
    public string message;
}