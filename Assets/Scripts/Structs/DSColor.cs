﻿using UnityEngine;

public struct DSColor {

    public static Color32 GoldenSand {
        get => new(236, 204, 104, 255);
    }

    public static Color32 Orange {
        get => new(255, 165, 2, 255);
    }

    public static Color32 LimeSoap {
        get => new(123, 237, 159, 255);
    }

    public static Color32 UfoGreen {
        get => new(46, 213, 115, 255);
    }

    public static Color32 Coral {
        get => new(255, 127, 80, 255);
    }

    public static Color32 BruschettaTomato {
        get => new(255, 99, 72, 255);
    }

    public static Color32 FrenchSkyBlue {
        get => new(112, 161, 255, 255);
    }

    public static Color32 ClearChill {
        get => new(30, 144, 255, 255);
    }

    public static Color32 WildWatermelon {
        get => new(255, 107, 129, 255);
    }

    public static Color32 Watermelon {
        get => new(255, 71, 87, 255);
    }

    public static Color32 SaturatedSky {
        get => new(83, 82, 237, 255);
    }

    public static Color32 BrightGreek {
        get => new(55, 66, 250, 255);
    }

    public static Color32 Peace {
        get => new(164, 176, 190, 255);
    }

    public static Color32 BayWharf {
        get => new(116, 125, 140, 255);
    }

    public static Color32 White {
        get => new(255, 255, 255, 255);
    }

    public static Color32 AntiFlashWhite {
        get => new(241, 242, 246, 255);
    }

    public static Color32 Gris {
        get => new(87, 96, 111, 255);
    }

    public static Color32 Prestige {
        get => new(47, 53, 66, 255);
    }

    public static Color32 CityL {
        get => new(223, 228, 234, 255);
    }

    public static Color32 Twinkle {
        get => new(206, 214, 224, 255);
    }
}