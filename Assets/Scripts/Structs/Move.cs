﻿internal struct Move {
    public MoveType type;
    public DSCardBehaviour card;
    public DSIngameClient player;
}