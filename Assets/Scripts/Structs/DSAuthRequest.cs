﻿using Mirror;

public struct DSAuthRequest : NetworkMessage {
    public string clientDeviceID;
    public string clientVersion;
    public string clientName;
    public BackColor prefBackColor;
    public bool prefFirst;
}