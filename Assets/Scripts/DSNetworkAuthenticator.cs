using Mirror;
using Mirror.Authenticators;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    Documentation: https://mirror-networking.gitbook.io/docs/components/network-authenticators
    API Reference: https://mirror-networking.com/docs/api/Mirror.NetworkAuthenticator.html
*/

[DisallowMultipleComponent]
[AddComponentMenu("Double-Solitaire/DS Network Authenticator")]
public class DSNetworkAuthenticator : DeviceAuthenticator {

    [Header("Client Username")]
    public string clientName;

    [Header("Preferred Back Color")]
    public BackColor preferredBackColor;

    [Header("Prefers First")]
    public bool preferredFirst;

    private readonly HashSet<NetworkConnection> connectionsPendingDisconnect = new();

    #region Server

    /// <summary>
    /// Called on server from StartServer to initialize the Authenticator
    /// <para>Server message handlers should be registered in this method.</para>
    /// </summary>
    public override void OnStartServer() {
        // register a handler for the authentication request we expect from client
        NetworkServer.RegisterHandler<DSAuthRequest>(OnAuthRequestReceived, false);
    }

    /// <summary>
    /// Called on server from StopServer to reset the Authenticator
    /// <para>Server message handlers should be registered in this method.</para>
    /// </summary>
    public override void OnStopServer() {
        // unregister the handler for the authentication request
        NetworkServer.UnregisterHandler<DSAuthRequest>();
    }

    /// <summary>
    /// Called on server from OnServerConnectInternal when a client needs to authenticate
    /// </summary>
    /// <param name="conn">Connection to client.</param>
    public override void OnServerAuthenticate(NetworkConnectionToClient conn) {
        //do nothing...wait for DSAuthRequest from client
    }

    [Server]
    private AuthResultCode ValidateAuth(NetworkConnectionToClient conn, DSAuthRequest msg)
    {
        Version clientVersion = new(msg.clientVersion);

        // auth validation
        AuthResultCode result = AuthResultCode.Success; // success

        // pending request check
        if (connectionsPendingDisconnect.Contains(conn)) result = AuthResultCode.RequestAlreadyPending;
        // unique name check
        else if (DSIngameClient.playerNames.Contains(msg.clientName)) result = AuthResultCode.ClientNameAlreadyInUse;
        // major/minor version match check
        else if (!clientVersion.Major.Equals(DSTweakData.VERSION.Major)) result = AuthResultCode.MajorVersionMismatch;
        else if (!clientVersion.Minor.Equals(DSTweakData.VERSION.Minor)) result = AuthResultCode.MinorVersionMismatch;

        return result;
    }

    /// <summary>
    /// Called on server when the client's DSAuthRequest arrives
    /// </summary>
    /// <param name="conn">Connection to client.</param>
    /// <param name="msg">The message payload</param>
    public void OnAuthRequestReceived(NetworkConnectionToClient conn, DSAuthRequest msg) {
        Debug.Log($"connection {conn.connectionId} ({msg.clientName}) authenticated with id {msg.clientDeviceID}");

        AuthResultCode code = ValidateAuth(conn, msg);

        string message = "Success";
        switch (code) {
            case AuthResultCode.ClientNameAlreadyInUse:
                message = "Name already in use!";
                break;

            case AuthResultCode.MajorVersionMismatch:
                message = $"Major version mismatch! (Server: {DSTweakData.VERSION})";
                break;

            case AuthResultCode.MinorVersionMismatch:
                message = $"Minor version mismatch! (Server: {DSTweakData.VERSION})";
                break;

            case AuthResultCode.Success:
                break;

            default:
                message = $"Unkown Error code: " + code;
                break;
        }

        // create and send msg to client so it knows to disconnect
        DSAuthResponse authResponseMessage = new() {
            code = code,
            message = message
        };

        conn.Send(authResponseMessage); // it is important to send BEFORE ServerAccept() for late joins TODO: check for possible condition races in network traffic? didnt happen yet, try latency sim

        // check the credentials by calling your web server, database table, playfab api, or any method appropriate.
        if (code.Equals(AuthResultCode.Success)) {
            // Add the name to the HashSet
            DSIngameClient.playerNames.Add(msg.clientName);

            // Store DSAuthData in authenticationData
            // This will be read in Player.OnStartServer
            // to set the playerName SyncVar and check for version mismatch.
            conn.authenticationData = new DSAuthData() {
                clientName = msg.clientName,
                clientVersion = msg.clientVersion,
                //clientDeviceID = msg.clientDeviceID // TODO: need for reconnect?
                prefBackColor = msg.prefBackColor,
                prefFirst = msg.prefFirst,
            };

            // Accept the successful authentication
            ServerAccept(conn);
        } else {
            connectionsPendingDisconnect.Add(conn);

            // must set NetworkConnection isAuthenticated = false
            conn.isAuthenticated = false;

            // disconnect the client after 1 second so that response message gets delivered
            StartCoroutine(DelayedDisconnect(conn, 1f));
        }
    }

    private IEnumerator DelayedDisconnect(NetworkConnectionToClient conn, float waitTime) {
        yield return new WaitForSeconds(waitTime);

        // Reject the unsuccessful authentication
        ServerReject(conn);

        yield return null;

        // remove conn from pending connections
        connectionsPendingDisconnect.Remove(conn);
    }

    #endregion Server

    #region Client

    public void SetClientName(string clientName) {
        this.clientName = clientName;
    }

    public void SetPrefBackColor(BackColor preferredBackColor) {
        this.preferredBackColor = preferredBackColor;
    }

    public void SetPrefFirst(bool preferredFirst) {
        this.preferredFirst = preferredFirst;
    }

    /// <summary>
    /// Called on client from StartClient to initialize the Authenticator
    /// <para>Client message handlers should be registered in this method.</para>
    /// </summary>
    public override void OnStartClient() {
        // register a handler for the authentication response we expect from server
        NetworkClient.RegisterHandler<DSAuthResponse>(OnAuthResponseMessage, false);
    }

    /// <summary>
    /// Called on client from StopClient to reset the Authenticator
    /// <para>Client message handlers should be unregistered in this method.</para>
    /// </summary>
    public override void OnStopClient() {
        // unregister the handler for the authentication response
        NetworkClient.UnregisterHandler<DSAuthResponse>();
    }

    /// <summary>
    /// Called on client from OnClientConnectInternal when a client needs to authenticate
    /// </summary>
    public override void OnClientAuthenticate() {
        string deviceUniqueIdentifier = SystemInfo.deviceUniqueIdentifier;

        // Not all platforms support this, so we use a GUID instead
        if (deviceUniqueIdentifier.Equals(SystemInfo.unsupportedIdentifier)) {
            // Get the value from PlayerPrefs if it exists, new GUID if it doesn't
            deviceUniqueIdentifier = PlayerPrefs.GetString("deviceUniqueIdentifier", Guid.NewGuid().ToString());

            // Store the deviceUniqueIdentifier to PlayerPrefs (in case we just made a new GUID)
            PlayerPrefs.SetString("deviceUniqueIdentifier", deviceUniqueIdentifier);
            PlayerPrefs.Save();
        }

        NetworkClient.Send(new DSAuthRequest {
            clientName = clientName,
            clientVersion = DSTweakData.VERSION.ToString(),
            clientDeviceID = deviceUniqueIdentifier,
            prefBackColor = preferredBackColor,
            prefFirst = preferredFirst,
        });
    }

    /// <summary>
    /// Called on client when the server's AuthResponseMessage arrives
    /// </summary>
    /// <param name="msg">The message payload</param>
    public void OnAuthResponseMessage(DSAuthResponse msg) {
        if (msg.code.Equals(AuthResultCode.Success)) {
            // Authentication has been accepted
            OnAuthResponseMessage(new AuthResponseMessage());

            //MainMenu.singleton.SetError(null);
        } else {
            if (DSTweakData.DEBUG) Debug.Log($"Authentication Response: {msg.message}");

            // Authentication has been rejected
            // StopHost works for both host client and remote clients
            NetworkManager.singleton.StopHost();

            // Do this AFTER StopHost so it doesn't get cleared / hidden by OnClientDisconnect
            //MainMenu.singleton.SetError(msg.message);
        }
    }

    #endregion Client
}