using Assets.Scripts.Menus;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
	Documentation: https://mirror-networking.gitbook.io/docs/components/network-room-manager

	Documentation (base): https://mirror-networking.gitbook.io/docs/components/network-manager
	API Reference (base): https://mirror-networking.com/docs/api/Mirror.NetworkManager.html
*/

namespace Assets.Scripts
{

    [DisallowMultipleComponent]
    [RequireComponent(typeof(DSNetworkAuthenticator))]
    [AddComponentMenu("Double-Solitaire/DS Network Room Manager")]
    public class DSNetworkRoomManager : BetterNetworkRoomManager {
    #pragma warning disable IDE1006 // Benennungsstile

        // Overrides the base singleton so we don't
        // have to cast to this type everywhere.
        public new static DSNetworkRoomManager singleton { get; private set; }

    #pragma warning restore IDE1006 // Benennungsstile

        private Coroutine playersReadyCoroutine;

        /// <summary>
        /// Runs on both Server and Client
        /// Networking is NOT initialized when this fires
        /// </summary>
        public override void Awake() {
            base.Awake();
            singleton = this;
        }

        /// <summary>
        /// This is called on the server when a new client connects to the server.
        /// </summary>
        /// <param name="conn">The new connection.</param>
        public override void OnRoomServerConnect(NetworkConnectionToClient conn) {
            if (conn == null || conn.authenticationData == null) return;

            string clientName = ((DSAuthData)conn.authenticationData).clientName;

            string msg = $"{clientName} connected.";
            DSChatManager.singleton.RpcReceiveChatMessage(msg, null);
            DSChatManager.singleton.RpcHeadsup(msg);
        }

        /// <summary>
        /// Called on the server when a client disconnects.
        /// <para>This is called on the Server when a Client disconnects from the Server. Use an override to decide what should happen when a disconnection is detected.</para>
        /// </summary>
        /// <param name="conn">Connection from client.</param>
        public override void OnRoomServerDisconnect(NetworkConnectionToClient conn) {
            if (conn == null || conn.authenticationData == null) return;

            string clientName = ((DSAuthData)conn.authenticationData).clientName;

            // remove player name from the HashSet of taken names
            DSIngameClient.playerNames.Remove(clientName);

            // remove client from game manager (if ingame)
            DSGameManager gameManager = DSGameManager.singleton;
            DSIngameClient player = conn.identity ? conn.identity.GetComponent<DSIngameClient>() : null;
            if (gameManager != null && player != null)
                DSGameManager.singleton.RemovePlayer(player);

            string msg = $"{clientName} disconnected.";
            DSChatManager.singleton.RpcReceiveChatMessage(msg, null);
            DSChatManager.singleton.RpcHeadsup(msg);
        }

        public override void OnRoomClientEnter() {
            if (LobbyUI.Instance == null) return;
            LobbyUI.Instance.OnRoomClientListChanged(roomSlots.Cast<DSRoomClient>());
        }

        public override void OnRoomServerClientEnter() {
            if (LobbyUI.Instance == null) return;
            LobbyUI.Instance.OnRoomClientListChanged(roomSlots.Cast<DSRoomClient>());
        }

        public override void OnRoomClientExit() {
            if (LobbyUI.Instance == null) return;
            LobbyUI.Instance.OnRoomClientListChanged(roomSlots.Cast<DSRoomClient>());
        }

        public override void OnRoomServerClientExit() {
            if (LobbyUI.Instance == null) return;
            LobbyUI.Instance.OnRoomClientListChanged(roomSlots.Cast<DSRoomClient>());
        }

        public override void OnRoomClientSceneChanged() {
            if (LobbyUI.Instance == null) return;
            LobbyUI.Instance.OnRoomClientListChanged(roomSlots.Cast<DSRoomClient>());
        }

        /// <summary>
        /// This is called on the server when a networked scene finishes loading.
        /// </summary>
        /// <param name="sceneName">Name of the new scene.</param>
        public override void OnRoomServerSceneChanged(string sceneName) {
            if (sceneName.Equals(GameplayScene))
                DSGameManager.singleton.StartWaitingForPlayers();
        }

        /// <summary>
        /// Called just after GamePlayer object is instantiated and just before it replaces RoomPlayer object.
        /// This is the ideal point to pass any data like player name, credentials, tokens, colors, etc.
        /// into the GamePlayer object as it is about to enter the Online scene.
        /// </summary>
        /// <param name="roomPlayer"></param>
        /// <param name="gamePlayer"></param>
        /// <returns>true unless some code in here decides it needs to abort the replacement</returns>
        public override bool OnRoomServerSceneLoadedForPlayer(NetworkConnectionToClient conn, GameObject roomPlayer, GameObject gamePlayer) {
            DSRoomClient lobbyPlayer = roomPlayer.GetComponent<DSRoomClient>();
            DSIngameClient ingamePlayer = gamePlayer.GetComponent<DSIngameClient>();

            ingamePlayer.SetSyncedClientType(lobbyPlayer.GetClientType());
            ingamePlayer.SetSyncedBackColor(lobbyPlayer.GetBackColor());
            ingamePlayer.SetSyncedFirstTurn(lobbyPlayer.IsFirst());

            DSGameManager.singleton.QueueAddPlayer(ingamePlayer);

            return true;
        }

        public override void OnRoomServerPlayersNotReady() {
            if (SceneManager.GetActiveScene().path == GameplayScene) return;

            //if (AreTwoPlayersReady() && playersReadyCoroutine == null) {
            //OnRoomServerPlayersReady(); // enforce ready validation when 2 players (fuck spectator ready status!)
            //} else
            if (playersReadyCoroutine != null) {
                string msg = $"Cancelled.";
                DSChatManager.singleton.RpcReceiveChatMessage(msg, null);
                DSChatManager.singleton.RpcHeadsup(msg);
                StopCoroutine(playersReadyCoroutine); // stop ready timeout
            }
        }

        public override void OnRoomServerPlayersReady() {
            if (SceneManager.GetActiveScene().path == GameplayScene) return;

            (LobbyValidationResult result, _) = ValidateLobby();

            string err = null;
            if (!result.HasFlag(LobbyValidationResult.PlayerCountGood)) {
                err = "there must be exactly 2 players, all others must be spectators";
            }
            if (!result.HasFlag(LobbyValidationResult.FirstCountGood)) {
                err = "one player must be the first";
            }
            if (!result.HasFlag(LobbyValidationResult.SameColorCountGood)) {
                err = "each color can only be used once";
            }

            LobbyUI.Instance.SetError(err); // set first (might set null, which is fine)
            if (!string.IsNullOrEmpty(err))
                return;

            if (playersReadyCoroutine != null) StopCoroutine(playersReadyCoroutine);
            playersReadyCoroutine = StartCoroutine(TimeoutPlayersReady()); // start gamestart timeout
        }

        public override void OnGUI() {
            // nth (disables builtin ui!)
        }

        [Server]
        internal void OnLobbySettingsChanged() {
            // unready all
            UnreadyAll();

            // trigger refresh for all clients (lock/unlock controls)
            if (LobbyUI.Instance != null)
            {
            	LobbyUI.Instance.OnLobbySettingsChanged();
            }
        }

        /// <summary>
        /// Returns current players with their used options as Tuple
        /// </summary>
        /// <returns>
        ///  - list of players
        ///  - list of firsts
        ///  - a dictionary, with all used back colors as keys, and their correspoding list of users as value
        /// </returns>
        private (List<DSRoomClient>, List<DSRoomClient>, Dictionary<BackColor, List<DSRoomClient>>) GetAllClientsWithAllOptions(DSRoomClient excludedRoomPlayer = null) {
            // validate player settings
            List<DSRoomClient> players = new();
            List<DSRoomClient> firsts = new();
            Dictionary<BackColor, List<DSRoomClient>> usedColors = new();

            foreach (DSRoomClient client in roomSlots.Cast<DSRoomClient>()) {
                if (client.GetClientType().Equals(BetterNetworkClientType.Player) && (!client.Equals(excludedRoomPlayer))) {
                    // add to players
                    players.Add(client);

                    if (client.IsFirst()) {
                        // add to firsts
                        firsts.Add(client);
                    }

                    BackColor backColor = client.GetBackColor();
                    if (!usedColors.ContainsKey(backColor)) {
                        // add back color
                        usedColors.Add(backColor, new() { client });
                    } else if (!usedColors[backColor].Contains(client)) {
                        // add to back coolor
                        usedColors[backColor].Add(client);
                    }
                }
            }
            return (players, firsts, usedColors);
        }

        internal (LobbyValidationResult, List<BackColor>) ValidateLobby(DSRoomClient excludedRoomPlayer = null) {
            LobbyValidationResult result = LobbyValidationResult.Empty;

            List<DSRoomClient> players;
            List<DSRoomClient> firsts;
            Dictionary<BackColor, List<DSRoomClient>> usedColors;

            (players, firsts, usedColors) = GetAllClientsWithAllOptions(excludedRoomPlayer);

            // validate player count
            const int wantedPlayerCount = 2;
            if (players.Count > wantedPlayerCount)
                result |= LobbyValidationResult.PlayerCountTooMany;
            else if (players.Count < wantedPlayerCount)
                result |= LobbyValidationResult.PlayerCountNotEnough;
            else
                result |= LobbyValidationResult.PlayerCountGood;

            // validate first count
            const int wantedFirstCount = 1;
            if (firsts.Count > wantedFirstCount)
                result |= LobbyValidationResult.FirstCountTooMany;
            else if ((firsts.Count < wantedFirstCount))
                result |= LobbyValidationResult.FirstCountNotEnough;
            else
                result |= LobbyValidationResult.FirstCountGood;

            foreach (List<DSRoomClient> colorUsers in usedColors.Values) {
                if (colorUsers.Count > 1) {
                    result |= LobbyValidationResult.SameColorCountTooMany;
                    break;
                }
            }
            if (!result.HasFlag(LobbyValidationResult.SameColorCountTooMany))
                result |= LobbyValidationResult.SameColorCountGood;

            return (result, usedColors.Keys.ToList());
        }

        private IEnumerator TimeoutPlayersReady() {
            float timeLeft = 3f;
            while (timeLeft > 0f) {
                string msg = $"Game starts in {timeLeft}...";
                DSChatManager.singleton.RpcReceiveChatMessage(msg, null);
                DSChatManager.singleton.RpcHeadsup(msg);
                yield return new WaitForSeconds(1f);
                timeLeft -= 1f;
            }
            ServerChangeScene(GameplayScene); // start game
            playersReadyCoroutine = null;
        }

        [Server]
        internal void UnreadyAll() {
            roomSlots.Cast<DSRoomClient>().ToList().ForEach(x => x.RpcSetUnready());
            //LobbyUI.Instance.SetError(null);
        }

        internal static GameObject GetNetworkObject(uint netId) {
            var dict = NetworkServer.active ? NetworkServer.spawned : NetworkClient.spawned;

            if (dict == null && dict.Count == 0) {
                Debug.LogWarning("Requested network object while nothing spawned yet.");
                return null;
            } else if (netId <= 0) {
                Debug.LogWarning("Requested network object 0.");
                return null;
            } else if (!dict.ContainsKey(netId)) {
                Debug.LogWarning($"Requested unspawned network object {netId}. Id not in spawned list yet.");
                return null;
            } else if (dict[netId].gameObject == null) {
                Debug.LogWarning($"Requested unspawned network object {netId}. GameObject is null.");
                return null;
            }

            return dict[netId].gameObject;
        }

        internal static T GetNetworkComponent<T>(uint netId) where T : Component {
            GameObject obj = GetNetworkObject(netId);
            if (obj == null) return null;

            if (!obj.TryGetComponent<T>(out var component)) {
                Debug.LogWarning($"Requested unspawned network component {netId}. Component is null.");
                return null;
            }

            return component;
        }
    }
}