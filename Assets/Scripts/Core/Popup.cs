﻿using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Core {

    public class Popup : MonoBehaviour {
        private VisualElement popup;
        private VisualElement element;
        private Label label;

        /// <summary>
        /// true while running, false before and after.
        /// </summary>
        internal bool alive { get; private set; }

        /// <summary>
        /// only true AFTER has been run
        /// </summary>
        internal bool dead { get; private set; }

        internal void PopUp(VisualElement parent, string text, float delay, float attack, float hold, float decay) {
            if (alive) return;
            alive = true;

            element = Resources.Load<VisualTreeAsset>("Popup").Instantiate();

            popup = element.Q<VisualElement>("Popup");
            popup.style.opacity = 0;

            label = element.Q<Label>("PopupLabel");
            label.text = text;

            parent.Add(element);

            StartCoroutine(LiveLife(parent, delay, attack, hold, decay));
        }

        private IEnumerator LiveLife(VisualElement parent, float delay, float attack, float hold, float decay) {
            // delay
            yield return new WaitForSeconds(delay);

            // attack
            float t = 0f;
            while (t <= 1f) {
                t += Time.deltaTime / attack;
                popup.style.opacity = t;
                yield return null;
            }
            popup.style.opacity = 1;

            // hold
            yield return new WaitForSeconds(hold);

            // decay
            t = 0f;
            while (t <= 1f) {
                t += Time.deltaTime / decay;
                popup.style.opacity = 1 - t;
                yield return null;
            }
            popup.style.opacity = 0;

            // death
            alive = false;
            dead = true;
            parent.Remove(element);
            Destroy(gameObject);
        }
    }
}