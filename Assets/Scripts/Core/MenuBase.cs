using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(UIDocument))]
public class MenuBase : MonoBehaviour {
    public string menuGroupName;

    private VisualElement menuGroup;

    private Enum currentPage = null;

    public virtual void Start() {
        menuGroup = GetComponent<UIDocument>().rootVisualElement.Q<VisualElement>(menuGroupName);
    }

    internal virtual void StartPage() {
        SwitchPage(GetComponent<IMenu>().GetPages().Find(p => p.canShow == null || p.canShow()).page);
    }

    internal void SwitchPage(Enum page) {
        SwitchPage(GetComponent<IMenu>().GetPage(page));
        currentPage = page;
    }

    internal void ReloadPage() {
        if (currentPage != null)
            SwitchPage(currentPage);
    }

    private void SwitchPage(List<IMenuEntry> page) {
        if (menuGroup != null) {
            menuGroup.Clear();
            page?.ForEach(e => {
                if (e != null && e.ShowCondition())
                    menuGroup.Add(e.CreateElement());
            });
        }
    }
}