﻿using Mirror;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

/// <summary>
/// This is a specialized NetworkManager that includes a networked room.
/// </summary>
/// <remarks>
/// <para>The room has slots that track the joined players, and a maximum player count that is enforced. It requires that the BetterNetworkRoomPlayer component be on the room player objects.</para>
/// <para>BetterNetworkRoomManager is derived from NetworkManager, and so it implements many of the virtual functions provided by the NetworkManager class. To avoid accidentally replacing functionality of the BetterNetworkRoomManager, there are new virtual functions on the BetterNetworkRoomManager that begin with "OnRoom". These should be used on classes derived from BetterNetworkRoomManager instead of the virtual functions on NetworkManager.</para>
/// <para>The OnRoom*() functions have empty implementations on the BetterNetworkRoomManager base class, so the base class functions do not have to be called.</para>
/// </remarks>
[AddComponentMenu("Network/Better Network Room Manager")]
[HelpURL("https://mirror-networking.gitbook.io/docs/components/network-room-manager")]
public abstract class BetterNetworkRoomManager : NetworkManager {

    [Header("Room Settings")]
    [FormerlySerializedAs("m_ShowRoomGUI")]
    [Tooltip("This flag controls whether the default UI is shown for the room")]
    public bool showRoomGUI = true;

    [FormerlySerializedAs("m_MinPlayers")]
    [Tooltip("Minimum number of players to perform ready-checks")]
    public int minPlayers = 1;

    public bool allowLateJoin;

    [FormerlySerializedAs("m_RoomPlayerPrefab")]
    [Tooltip("Prefab to use for the Room Client")]
    public BetterNetworkRoomClient roomClientPrefab;

    /// <summary>
    /// The scene to use for the room. This is similar to the offlineScene of the NetworkManager.
    /// </summary>
    [Scene]
    public string RoomScene;

    /// <summary>
    /// The scene to use for the playing the game from the room. This is similar to the onlineScene of the NetworkManager.
    /// </summary>
    [Scene]
    public string GameplayScene;

    /// <summary>
    /// These slots track players that enter the room.
    /// <para>The slotId on players is global to the game - across all players.</para>
    /// </summary>
    [Tooltip("List of Room Client objects")]
    public List<BetterNetworkRoomClient> roomSlots = new();

    // Sequential index used in round-robin deployment of players into instances and score positioning
    public int clientIndex;

    public override void OnValidate() {
        base.OnValidate();

        // always <= maxConnections
        minPlayers = Mathf.Min(minPlayers, maxConnections);

        // always >= 0
        minPlayers = Mathf.Max(minPlayers, 0);

        if (roomClientPrefab != null) {
            if (!roomClientPrefab.TryGetComponent<NetworkIdentity>(out _)) {
                roomClientPrefab = null;
                Debug.LogError("RoomPlayer prefab must have a NetworkIdentity component.");
            }
        }
    }

    private bool wasReady = false;

    public virtual void ReadyStatusChanged() {
        if (!Utils.IsSceneActive(RoomScene))
            return;

        int CurrentPlayers = 0;
        int ReadyPlayers = 0;

        foreach (BetterNetworkRoomClient item in roomSlots) {
            if (item != null && item.GetClientType().Equals(BetterNetworkClientType.Player)) {
                CurrentPlayers++;
                if (item.readyToBegin)
                    ReadyPlayers++;
            }
        }

        bool enoughReadyPlayers = minPlayers <= 0 || ReadyPlayers >= minPlayers;
        if (enoughReadyPlayers) {
            if (CurrentPlayers == ReadyPlayers) {
                if (!wasReady) {
                    wasReady = true;
                    OnRoomServerPlayersReady();
                }
            } else {
                if (wasReady) {
                    wasReady = false;
                    OnRoomServerPlayersNotReady();
                }
            }
        }
    }

    /// <summary>
    /// Called on the server when a client is ready.
    /// <para>The default implementation of this function calls NetworkServer.SetClientReady() to continue the network setup process.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerReady(NetworkConnectionToClient conn) {
        Debug.Log($"BetterNetworkRoomManager OnServerReady {conn}");
        base.OnServerReady(conn);

        if (conn != null && conn.identity != null) {
            GameObject roomPlayer = conn.identity.gameObject;

            // if null or not a room player, don't replace it
            if (roomPlayer != null && roomPlayer.GetComponent<BetterNetworkRoomClient>() != null)
                SceneLoadedForPlayer(conn, roomPlayer);
        }
    }

    private void SceneLoadedForPlayer(NetworkConnectionToClient conn, GameObject roomPlayer) {
        Debug.Log($"NetworkRoom SceneLoadedForPlayer scene: {SceneManager.GetActiveScene().path} {conn}");

        if (Utils.IsSceneActive(RoomScene)) {
            return;
        }

        GameObject gamePlayer = OnRoomServerCreateGamePlayer(conn, roomPlayer);
        if (gamePlayer == null) {
            // get start position from base class
            Transform startPos = GetStartPosition();
            gamePlayer = startPos != null
                ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
                : Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        }

        if (!OnRoomServerSceneLoadedForPlayer(conn, roomPlayer, gamePlayer))
            return;

        // replace room player with game player
        NetworkServer.ReplacePlayerForConnection(conn, gamePlayer, true);
    }

    /// <summary>
    /// Called on the server when a scene is completed loaded, when the scene load was initiated by the server with ServerChangeScene().
    /// </summary>
    /// <param name="sceneName">The name of the new scene.</param>
    public override void OnServerSceneChanged(string sceneName) {
        OnRoomServerSceneChanged(sceneName);
    }

    internal void CallOnClientEnterRoom() {
        OnRoomClientEnter();
        foreach (BetterNetworkRoomClient player in roomSlots)
            if (player != null) {
                player.OnClientEnterRoom();
            }
    }

    internal void CallOnClientExitRoom() {
        OnRoomClientExit();
        foreach (BetterNetworkRoomClient player in roomSlots)
            if (player != null) {
                player.OnClientExitRoom();
            }
    }

    #region server handlers

    /// <summary>
    /// Called on the server when a new client connects.
    /// <para>Unity calls this on the Server when a Client connects to the Server. Use an override to tell the NetworkManager what to do when a client connects to the server.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerConnect(NetworkConnectionToClient conn) {
        if (Utils.IsSceneActive(RoomScene) || allowLateJoin) {
            base.OnServerConnect(conn);
            OnRoomServerConnect(conn);
        } else {
            // joining game in progress is not allowed
            Debug.Log($"Not in Room scene...disconnecting {conn}");
            conn.Disconnect();
        }
    }

    /// <summary>
    /// Called on the server when a client disconnects.
    /// <para>This is called on the Server when a Client disconnects from the Server. Use an override to decide what should happen when a disconnection is detected.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerDisconnect(NetworkConnectionToClient conn) {
        if (conn.identity != null) {
            if (conn.identity.TryGetComponent<BetterNetworkRoomClient>(out var roomPlayer))
                roomSlots.Remove(roomPlayer);

            foreach (NetworkIdentity clientOwnedObject in conn.owned)
                if (clientOwnedObject.TryGetComponent(out roomPlayer))
                    roomSlots.Remove(roomPlayer);

            DSChatManager.singleton.RpcTypingChanged(false, conn.identity.netId);
        }

        ReadyStatusChanged();

        foreach (BetterNetworkRoomClient player in roomSlots)
            if (player != null)
                player.GetComponent<BetterNetworkRoomClient>().readyToBegin = false;

        if (Utils.IsSceneActive(RoomScene))
            RecalculateRoomPlayerIndices();

        OnRoomServerDisconnect(conn);
        base.OnServerDisconnect(conn);

#if UNITY_SERVER
            if (numPlayers < 1)
                StopServer();
#endif
    }

    /// <summary>
    /// Called on the server when a client adds a new player with NetworkClient.AddPlayer.
    /// <para>The default implementation for this function creates a new player object from the playerPrefab.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerAddPlayer(NetworkConnectionToClient conn) {
        // increment the index before adding the player, so first player starts at 1
        clientIndex++;

        bool isRoom = Utils.IsSceneActive(RoomScene);

        if (isRoom) ReadyStatusChanged();

        if (isRoom || allowLateJoin) {
            Debug.Log("BetterNetworkRoomManager.OnServerAddPlayer playerPrefab: {roomPlayerPrefab.name}");

            GameObject newRoomGameObject = OnRoomServerCreateRoomPlayer(conn);
            if (newRoomGameObject == null)
                newRoomGameObject = Instantiate(roomClientPrefab.gameObject, Vector3.zero, Quaternion.identity);

            NetworkServer.AddPlayerForConnection(conn, newRoomGameObject); // assign room player first, even for late joins

            if (!isRoom)
                // directly spawn ingame player
                OnServerReady(conn);
        } else {
            // Late joiners not allowed...should've been kicked by OnServerDisconnect
            Debug.Log($"Not in Room scene...disconnecting {conn}");
            conn.Disconnect();
        }
    }

    [Server]
    public void RecalculateRoomPlayerIndices() {
        if (roomSlots.Count > 0)
            for (int i = 0; i < roomSlots.Count; i++)
                roomSlots[i].index = i;
    }

    /// <summary>
    /// This causes the server to switch scenes and sets the networkSceneName.
    /// <para>Clients that connect to this server will automatically switch to this scene. This is called automatically if onlineScene or offlineScene are set, but it can be called from user code to switch scenes again while the game is in progress. This automatically sets clients to be not-ready. The clients must call NetworkClient.Ready() again to participate in the new scene.</para>
    /// </summary>
    /// <param name="newSceneName"></param>
    public override void ServerChangeScene(string newSceneName) {
        if (newSceneName == RoomScene) {
            foreach (BetterNetworkRoomClient roomPlayer in roomSlots) {
                if (roomPlayer == null)
                    continue;

                // find the game-player object for this connection, and destroy it
                NetworkIdentity identity = roomPlayer.GetComponent<NetworkIdentity>();

                if (NetworkServer.active) {
                    // re-add the room object
                    roomPlayer.GetComponent<BetterNetworkRoomClient>().readyToBegin = false;
                    NetworkServer.ReplacePlayerForConnection(identity.connectionToClient, roomPlayer.gameObject);
                }
            }

            ReadyStatusChanged();
        }

        base.ServerChangeScene(newSceneName);
    }

    /// <summary>
    /// This is invoked when a server is started - including when a host is started.
    /// <para>StartServer has multiple signatures, but they all cause this hook to be called.</para>
    /// </summary>
    public override void OnStartServer() {
        if (string.IsNullOrWhiteSpace(RoomScene)) {
            Debug.LogError("BetterNetworkRoomManager RoomScene is empty. Set the RoomScene in the inspector for the BetterNetworkRoomManager");
            return;
        }

        if (string.IsNullOrWhiteSpace(GameplayScene)) {
            Debug.LogError("BetterNetworkRoomManager PlayScene is empty. Set the PlayScene in the inspector for the BetterNetworkRoomManager");
            return;
        }

        OnRoomStartServer();
    }

    /// <summary>
    /// This is invoked when a host is started.
    /// <para>StartHost has multiple signatures, but they all cause this hook to be called.</para>
    /// </summary>
    public override void OnStartHost() {
        OnRoomStartHost();
    }

    /// <summary>
    /// This is called when a server is stopped - including when a host is stopped.
    /// </summary>
    public override void OnStopServer() {
        roomSlots.Clear();
        OnRoomStopServer();
    }

    /// <summary>
    /// This is called when a host is stopped.
    /// </summary>
    public override void OnStopHost() {
        OnRoomStopHost();
    }

    #endregion server handlers

    #region client handlers

    /// <summary>
    /// This is invoked when the client is started.
    /// </summary>
    public override void OnStartClient() {
        if (roomClientPrefab == null || roomClientPrefab.gameObject == null)
            Debug.LogError("BetterNetworkRoomManager no RoomPlayer prefab is registered. Please add a RoomPlayer prefab.");
        else
            NetworkClient.RegisterPrefab(roomClientPrefab.gameObject);

        if (playerPrefab == null)
            Debug.LogError("BetterNetworkRoomManager no GamePlayer prefab is registered. Please add a GamePlayer prefab.");

        OnRoomStartClient();
    }

    /// <summary>
    /// Called on the client when connected to a server.
    /// <para>The default implementation of this function sets the client as ready and adds a player. Override the function to dictate what happens when the client connects.</para>
    /// </summary>
    public override void OnClientConnect() {
        OnRoomClientConnect();
        base.OnClientConnect();
    }

    /// <summary>
    /// Called on clients when disconnected from a server.
    /// <para>This is called on the client when it disconnects from the server. Override this function to decide what happens when the client disconnects.</para>
    /// </summary>
    public override void OnClientDisconnect() {
        OnRoomClientDisconnect();
        base.OnClientDisconnect();
    }

    /// <summary>
    /// This is called when a client is stopped.
    /// </summary>
    public override void OnStopClient() {
        OnRoomStopClient();
        CallOnClientExitRoom();
        roomSlots.Clear();
    }

    /// <summary>
    /// Called on clients when a scene has completed loaded, when the scene load was initiated by the server.
    /// <para>Scene changes can cause player objects to be destroyed. The default implementation of OnClientSceneChanged in the NetworkManager is to add a player object for the connection if no player object exists.</para>
    /// </summary>
    public override void OnClientSceneChanged() {
        if (Utils.IsSceneActive(RoomScene)) {
            if (NetworkClient.isConnected)
                CallOnClientEnterRoom();
        } else
            CallOnClientExitRoom();

        base.OnClientSceneChanged();
        OnRoomClientSceneChanged();
    }

    #endregion client handlers

    #region room server virtuals

    public virtual void OnRoomServerClientEnter() {
    }

    public virtual void OnRoomServerClientExit() {
    }

    /// <summary>
    /// This is called on the host when a host is started.
    /// </summary>
    public virtual void OnRoomStartHost() { }

    /// <summary>
    /// This is called on the host when the host is stopped.
    /// </summary>
    public virtual void OnRoomStopHost() { }

    /// <summary>
    /// This is called on the server when the server is started - including when a host is started.
    /// </summary>
    public virtual void OnRoomStartServer() { }

    /// <summary>
    /// This is called on the server when the server is started - including when a host is stopped.
    /// </summary>
    public virtual void OnRoomStopServer() { }

    /// <summary>
    /// This is called on the server when a new client connects to the server.
    /// </summary>
    /// <param name="conn">The new connection.</param>
    public virtual void OnRoomServerConnect(NetworkConnectionToClient conn) { }

    /// <summary>
    /// This is called on the server when a client disconnects.
    /// </summary>
    /// <param name="conn">The connection that disconnected.</param>
    public virtual void OnRoomServerDisconnect(NetworkConnectionToClient conn) { }

    /// <summary>
    /// This is called on the server when a networked scene finishes loading.
    /// </summary>
    /// <param name="sceneName">Name of the new scene.</param>
    public virtual void OnRoomServerSceneChanged(string sceneName) { }

    /// <summary>
    /// This allows customization of the creation of the room-player object on the server.
    /// <para>By default the roomPlayerPrefab is used to create the room-player, but this function allows that behaviour to be customized.</para>
    /// </summary>
    /// <param name="conn">The connection the player object is for.</param>
    /// <returns>The new room-player object.</returns>
    public virtual GameObject OnRoomServerCreateRoomPlayer(NetworkConnectionToClient conn) {
        return null;
    }

    /// <summary>
    /// This allows customization of the creation of the GamePlayer object on the server.
    /// <para>By default the gamePlayerPrefab is used to create the game-player, but this function allows that behaviour to be customized. The object returned from the function will be used to replace the room-player on the connection.</para>
    /// </summary>
    /// <param name="conn">The connection the player object is for.</param>
    /// <param name="roomPlayer">The room player object for this connection.</param>
    /// <returns>A new GamePlayer object.</returns>
    public virtual GameObject OnRoomServerCreateGamePlayer(NetworkConnectionToClient conn, GameObject roomPlayer) {
        return null;
    }

    /// <summary>
    /// This allows customization of the creation of the GamePlayer object on the server.
    /// <para>This is only called for subsequent GamePlay scenes after the first one.</para>
    /// <para>See <see cref="OnRoomServerCreateGamePlayer(NetworkConnectionToClient, GameObject)">OnRoomServerCreateGamePlayer(NetworkConnection, GameObject)</see> to customize the player object for the initial GamePlay scene.</para>
    /// </summary>
    /// <param name="conn">The connection the player object is for.</param>
    public virtual void OnRoomServerAddPlayer(NetworkConnectionToClient conn) {
        base.OnServerAddPlayer(conn);
    }

    // for users to apply settings from their room player object to their in-game player object
    /// <summary>
    /// This is called on the server when it is told that a client has finished switching from the room scene to a game player scene.
    /// <para>When switching from the room, the room-player is replaced with a game-player object. This callback function gives an opportunity to apply state from the room-player to the game-player object.</para>
    /// </summary>
    /// <param name="conn">The connection of the player</param>
    /// <param name="roomPlayer">The room player object.</param>
    /// <param name="gamePlayer">The game player object.</param>
    /// <returns>False to not allow this player to replace the room player.</returns>
    public virtual bool OnRoomServerSceneLoadedForPlayer(NetworkConnectionToClient conn, GameObject roomPlayer, GameObject gamePlayer) {
        return true;
    }

    /// <summary>
    /// This is called on the server when all the players in the room are ready.
    /// <para>The default implementation of this function uses ServerChangeScene() to switch to the game player scene. By implementing this callback you can customize what happens when all the players in the room are ready, such as adding a countdown or a confirmation for a group leader.</para>
    /// </summary>
    public virtual void OnRoomServerPlayersReady() {
        // all players are readyToBegin, start the game
        ServerChangeScene(GameplayScene);
    }

    /// <summary>
    /// This is called on the server when CheckReadyToBegin finds that players are not ready
    /// <para>May be called multiple times while not ready players are joining</para>
    /// </summary>
    public virtual void OnRoomServerPlayersNotReady() { }

    #endregion room server virtuals

    #region room client virtuals

    /// <summary>
    /// This is a hook to allow custom behaviour when the game client enters the room.
    /// </summary>
    public virtual void OnRoomClientEnter() { }

    /// <summary>
    /// This is a hook to allow custom behaviour when the game client exits the room.
    /// </summary>
    public virtual void OnRoomClientExit() { }

    /// <summary>
    /// This is called on the client when it connects to server.
    /// </summary>
    public virtual void OnRoomClientConnect() { }

    /// <summary>
    /// This is called on the client when disconnected from a server.
    /// </summary>
    public virtual void OnRoomClientDisconnect() { }

    /// <summary>
    /// This is called on the client when a client is started.
    /// </summary>
    public virtual void OnRoomStartClient() { }

    /// <summary>
    /// This is called on the client when the client stops.
    /// </summary>
    public virtual void OnRoomStopClient() { }

    /// <summary>
    /// This is called on the client when the client is finished loading a new networked scene.
    /// </summary>
    public virtual void OnRoomClientSceneChanged() { }

    #endregion room client virtuals

    #region optional UI

    /// <summary>
    /// virtual so inheriting classes can roll their own
    /// </summary>
    public virtual void OnGUI() {
        if (!showRoomGUI)
            return;

        if (NetworkServer.active && Utils.IsSceneActive(GameplayScene)) {
            GUILayout.BeginArea(new Rect(Screen.width - 150f, 10f, 140f, 30f));
            if (GUILayout.Button("Return to Room"))
                ServerChangeScene(RoomScene);
            GUILayout.EndArea();
        }

        if (Utils.IsSceneActive(RoomScene))
            GUI.Box(new Rect(10f, 180f, 520f, 150f), "PLAYERS");
    }

    #endregion optional UI
}