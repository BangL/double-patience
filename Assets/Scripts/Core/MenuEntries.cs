﻿using System;
using UnityEngine.UIElements;

public interface IMenuEntry {

    VisualElement CreateElement();

    bool ShowCondition();
}

public abstract class MenuEntry : IMenuEntry {
    public Func<bool> canShow = null;

    public MenuEntry(Func<bool> canShow = null) {
        this.canShow = canShow;
    }

    public virtual VisualElement CreateElement() {
        return null;
    }

    public virtual void AddItemBaseClass(VisualElement element) {
        element.AddToClassList("menu-item");
    }

    public bool ShowCondition() {
        return canShow == null || (bool)canShow.DynamicInvoke();
    }
}

internal class MenuSpacer : MenuEntry {

    public override VisualElement CreateElement() {
        VisualElement spacer = new() {
            name = "spacer"
        };

        AddItemBaseClass(spacer);
        spacer.AddToClassList("menu-spacer");

        return spacer;
    }
}

internal class MenuLabel : MenuEntry {
    private readonly string text;

    public MenuLabel(string text) : base(() => !string.IsNullOrEmpty(text)) {
        this.text = text;
    }

    public override VisualElement CreateElement() {
        Label label = new() {
            name = "lbl" + text,
            text = text,
        };

        AddItemBaseClass(label);
        label.AddToClassList("menu-label");

        return label;
    }
}

internal class MenuError : MenuEntry {
    private readonly string text;

    public MenuError(string text) : base(() => !string.IsNullOrEmpty(text)) {
        this.text = text;
    }

    public override VisualElement CreateElement() {
        Label error = new(text) {
            name = "lblError"
        };

        AddItemBaseClass(error);
        error.AddToClassList("menu-error");

        return error;
    }
}

internal class MenuHeader : MenuEntry {
    private readonly string text;

    public MenuHeader(string text, Func<bool> canShow = null) : base(canShow) {
        this.text = text;
    }

    public override VisualElement CreateElement() {
        Label header = new(text) {
            name = "lblHeader"
        };

        AddItemBaseClass(header);
        header.AddToClassList("menu-header");

        return header;
    }
}

internal class MenuButton : MenuEntry {
    private readonly string text;
    private readonly Action onClick;
    private readonly bool isRed;

    public MenuButton(string text, Action onClick, bool isRed = false, Func<bool> canShow = null) : base(canShow) {
        this.text = text;
        this.onClick = onClick;
        this.isRed = isRed;
    }

    public override VisualElement CreateElement() {
        Button button = new(onClick) {
            name = "btn" + text.Replace(" ", string.Empty),
            text = text,
        };

        AddItemBaseClass(button);
        button.AddToClassList("button");
        button.AddToClassList("menu-button");
        if (isRed) button.AddToClassList("red-button");

        return button;
    }
}

internal abstract class MenuFieldEntry<T> : MenuEntry {
    protected string label;
    protected Func<T> get;
    protected EventCallback<ChangeEvent<T>> set;

    public MenuFieldEntry(string label, Func<T> get = null, EventCallback<ChangeEvent<T>> set = null, Func<bool> canShow = null) : base(canShow) {
        this.label = label;
        this.get = get;
        this.set = set;
    }

    public virtual void Bind(INotifyValueChanged<T> element) {
        if (get != null) element.value = get();
        if (set != null) element.RegisterValueChangedCallback(set);
    }
}

internal class MenuTextField : MenuFieldEntry<string> {
    private readonly int maxLength;

    public MenuTextField(
        string label, int maxLength,
        Func<string> get = null, EventCallback<ChangeEvent<string>> set = null, Func<bool> canShow = null
    ) : base(label, get, set, canShow) {
        this.maxLength = maxLength;
    }

    public override VisualElement CreateElement() {
        TextField textField = new(label, maxLength, false, false, char.MinValue) {
            name = "txt" + label.Replace(" ", string.Empty).Replace(":", string.Empty)
        };
        Bind(textField);

        AddItemBaseClass(textField);
        textField.AddToClassList("menu-text-field");

        return textField;
    }
}

internal class MenuSlider : MenuFieldEntry<float> {
    private readonly float start;
    private readonly float end;

    public MenuSlider(
        string label, float start, float end,
        Func<float> get = null, EventCallback<ChangeEvent<float>> set = null, Func<bool> canShow = null
    ) : base(label, get, set, canShow) {
        this.start = start;
        this.end = end;
    }

    public override VisualElement CreateElement() {
        Slider slider = new(label, start, end) {
            name = "slider" + label.Replace(" ", string.Empty).Replace(":", string.Empty),
        };
        Bind(slider);

        AddItemBaseClass(slider);
        slider.AddToClassList("menu-slider");

        return slider;
    }
}

internal class MenuSliderInt : MenuFieldEntry<int> {
    private readonly int start;
    private readonly int end;

    public MenuSliderInt(
        string label, int start, int end,
        Func<int> get = null, EventCallback<ChangeEvent<int>> set = null, Func<bool> canShow = null
    ) : base(label, get, set, canShow) {
        this.start = start;
        this.end = end;
    }

    public override VisualElement CreateElement() {
        SliderInt slider = new(label, start, end) {
            name = "slider" + label.Replace(" ", string.Empty).Replace(":", string.Empty)
        };
        Bind(slider);

        AddItemBaseClass(slider);
        slider.AddToClassList("menu-slider");

        return slider;
    }
}

internal class MenuEnum : MenuFieldEntry<Enum> {

    public MenuEnum(
        string label,
        Func<Enum> get, EventCallback<ChangeEvent<Enum>> set = null, Func<bool> canShow = null
    ) : base(label, get, set, canShow) {
    }

    public override VisualElement CreateElement() {
        EnumField enumField = new(label, get()) {
            name = "enum" + label.Replace(" ", string.Empty).Replace(":", string.Empty)
        };
        Bind(enumField);

        AddItemBaseClass(enumField);
        enumField.AddToClassList("menu-enum");

        return enumField;
    }
}

internal class MenuToggle : MenuFieldEntry<bool> {

    public MenuToggle(
        string label,
        Func<bool> get = null, EventCallback<ChangeEvent<bool>> set = null, Func<bool> canShow = null
    ) : base(label, get, set, canShow) {
    }

    public override VisualElement CreateElement() {
        Toggle toggle = new(label) {
            name = "toggle" + label.Replace(" ", string.Empty).Replace(":", string.Empty),
            text = label
        };
        Bind(toggle);

        AddItemBaseClass(toggle);
        toggle.AddToClassList("menu-toggle");

        return toggle;
    }
}