using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Core {

    public class PopupManager : MonoBehaviour {
        internal static PopupManager Instance;

        public string popupPanelName;
        public GameObject popupPrefab;

        private VisualElement popupPanel;

        private void Awake() {
            if (Instance != null)
                Destroy(Instance.gameObject);
            Instance = this;
        }

        internal void AddPopup(string text, float delay = 0f, float attack = .25f, float hold = 3f, float decay = .5f) {
            popupPanel = GetComponent<UIDocument>().rootVisualElement.Q<VisualElement>(popupPanelName);
            if (popupPanel != null) {
                GameObject popupObj = Instantiate(popupPrefab, transform);
                popupObj.GetComponent<Popup>().PopUp(popupPanel, text, delay, attack, hold, decay);
            }
        }
    }
}