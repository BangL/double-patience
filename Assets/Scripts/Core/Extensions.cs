﻿using System.Collections.Generic;
using UnityEngine;

internal static class Extensions {
    private static System.Random rng = new();

    public static void Shuffle<T>(this IList<T> list) {
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = rng.Next(n + 1);
            (list[n], list[k]) = (list[k], list[n]);
        }
    }

    public static string ToHtml(this Color color) {
        return string.Format("#{0:X2}{1:X2}{2:X2}", Mathf.RoundToInt(color.r * 255), Mathf.RoundToInt(color.g * 255), Mathf.RoundToInt(color.b * 255));
    }

    [RuntimeInitializeOnLoadMethod]
    private static void ResetStatics() {
        rng = new();
    }
}