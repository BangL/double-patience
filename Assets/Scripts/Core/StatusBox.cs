using Assets.Scripts;
using Assets.Scripts.UI.Networked;
using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UIElements;

public class StatusBox : MonoBehaviour {
    public string statusLabelName;
    private Label statusLabel;

    private void Start() {
        statusLabel = GetComponent<UIDocument>().rootVisualElement.Q<Label>(statusLabelName);
    }

    private void Update() {
        //statusGroup.Clear();
        StartDraw();

        // game title / version
        DrawStatusLine($"<b>Double-Solitaire</b> v{DSTweakData.VERSION}");

        // host/server/client mode
        if (NetworkServer.active && NetworkClient.active) {
            // host
            DrawStatusLine($"<b>Host</b> running via {Transport.active}");
        } else if (NetworkServer.active) {
            // server only
            DrawStatusLine($"<b>Server</b> running via {Transport.active}");
        } else if (NetworkClient.isConnected) {
            // client only, connected
            DrawStatusLine($"<b>Client</b> connected to {DSNetworkRoomManager.singleton.networkAddress} via {Transport.active}");
        }

        DSGameManager game = DSGameManager.singleton;
        if (game != null) {
            string gameState = ChatUI.ColorizeText("Locked", Color.gray);
            if (game.IsReady()) {
                DSIngameClient currentPlayer = DSGameManager.GetCurrentPlayer();
                DSIngameClient knockPlayer = DSGameManager.GetOpponentPlayer();
                if (currentPlayer != null && knockPlayer != null) {
                    if (IngameUI.singleton.winner.HasValue) {
                        DSIngameClient winner = DSNetworkRoomManager.GetNetworkComponent<DSIngameClient>(IngameUI.singleton.winner.Value);
                        gameState = ChatUI.ColorizeText($"{winner.GetChatName()} won", winner.GetChatColor());
                    } else if (game.knockMode) {
                        gameState = ChatUI.ColorizeText($"{knockPlayer.GetChatName()} knocking", knockPlayer.GetChatColor());
                    } else {
                        gameState = ChatUI.ColorizeText($"{currentPlayer.GetChatName()}'s turn", currentPlayer.GetChatColor());
                    }
                    //} else {
                    //    gameState = ChatUI.ColorizeText("Waiting for players...", Color.gray);
                }
            }

            DrawSpacer();
            DrawStatusLine($"<b>Status</b>: {gameState}");
            if (game.turnCount > 0) {
                DrawStatusLine($"<b>Moves</b>: {game.GetMoves()}");
                DrawStatusLine($"<b>Turns</b>: {game.turnCount - 1}");

                // players
                DrawSpacer();
                DrawStatusLine($"<b>Players</b>:");
                DSIngameClient player1 = DSGameManager.GetPlayer1();
                DSIngameClient player2 = DSGameManager.GetPlayer2();
                if (player1 != null && player2 != null) {
                    DrawStatusLine(ChatUI.ColorizeText($"{player1.GetChatName()}", player1.GetChatColor()));
                    DrawStatusLine(ChatUI.ColorizeText($"{player2.GetChatName()}", player2.GetChatColor()));
                }

                // spectators
                List<IDSClient> spectators =
                DSNetworkRoomManager.singleton.roomSlots.Select(x => x.GetComponent<IDSClient>()).Where(x => x.GetClientType().Equals(BetterNetworkClientType.Spectator)).ToList();
                if (spectators.Count > 0) {
                    DrawSpacer();
                    DrawStatusLine($"<b>Spectators</b>:");
                    spectators.ForEach(x => {
                        DrawStatusLine(ChatUI.ColorizeText($"{x.GetChatName()}", Color.gray));
                    });
                }
            }
        }

        EndDraw();
    }

    private StringBuilder builder;

    private void StartDraw() {
        builder = new StringBuilder();
    }

    private void EndDraw() {
        statusLabel.text = builder.ToString().TrimEnd(Environment.NewLine.ToCharArray());
    }

    private void DrawStatusLine(string text) {
        builder.AppendLine(text);
    }

    private void DrawSpacer() {
        builder.AppendLine();
    }
}