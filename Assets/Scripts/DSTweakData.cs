﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class DSTweakData : MonoBehaviour {
#if UNITY_EDITOR
    public static Version VERSION = new(UnityEditor.PlayerSettings.bundleVersion);
    public const bool DEBUG = true;
#else
    public static Version VERSION = new(UnityEngine.Application.version);
    public const bool DEBUG = false;
#endif

    public const string LAYER_CARDS = "Cards"; // used to show cards 'always on top' while moving/zooming
    public const string LAYER_HAND = "Hand"; // used to show cards 'always on top' while moving/zooming

    //public const float RETURN_THRESHOLD = .5f; // for drag & drop: used to diff between click and hold

    public const float ANIMATION_SPEED = .2f; // for flip, move and zoom animations timing

    public const float ZOOM_UNFOCUSED = 1f; // for zoom anim
    public const float ZOOM_FOCUSED = 1.3f; // for zoom anim

    public const float HIGHLIGHT_ALPHA = .4f; // for player mouseover highlights
    public const float HAND_ALPHA = .8f;

    public const float X_OFFSET = 2.5f; // for black/red 'game' stacks
    public const float Y_OFFSET = .05f; // for visual depth of stacks

    public const int MENU_WIDTH = 250;
    public const int MENU_HEIGHT = 200;

    public const int STATUS_WIDTH = 250;
    public const int STATUS_HEIGHT = 250;

    public const int BACK_BUTTONS_WIDTH = 100;

    public const int LOBBY_WIDTH = 550;
    public const int LOBBY_HEIGHT = 300;

    public const int CLIENT_BUTTONS_HEIGHT = 30;

    internal static readonly Dictionary<string, CardStackDefinition> STACKS = new() {
        { "Player1_1", new CardStackDefinition() { type = StackType.POwned | StackType.PlayerMain | StackType.P1Owned | StackType.P1Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(-17, -56) } },
        { "Player1_2", new CardStackDefinition() { type = StackType.POwned | StackType.PlayerDrop | StackType.P1Owned | StackType.P1Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(0, -56) } },
        { "Player1_3", new CardStackDefinition() { type = StackType.POwned | StackType.Player13   | StackType.P1Owned | StackType.P1Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(17, -56) } },

        { "Player2_1", new CardStackDefinition() { type = StackType.POwned | StackType.PlayerMain | StackType.P2Owned | StackType.P2Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(17, 56) } },
        { "Player2_2", new CardStackDefinition() { type = StackType.POwned | StackType.PlayerDrop | StackType.P2Owned | StackType.P2Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(0, 56) } },
        { "Player2_3", new CardStackDefinition() { type = StackType.POwned | StackType.Player13   | StackType.P2Owned | StackType.P2Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(-17, 56) } },

        { "Middle1", new CardStackDefinition() { type = StackType.Middle | StackType.P1Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(-8, 33) } },
        { "Middle2", new CardStackDefinition() { type = StackType.Middle | StackType.P2Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(8, 33) } },
        { "Middle3", new CardStackDefinition() { type = StackType.Middle | StackType.P1Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(-8, 11) } },
        { "Middle4", new CardStackDefinition() { type = StackType.Middle | StackType.P2Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(8, 11) } },
        { "Middle5", new CardStackDefinition() { type = StackType.Middle | StackType.P1Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(-8, -11) } },
        { "Middle6", new CardStackDefinition() { type = StackType.Middle | StackType.P2Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(8, -11) } },
        { "Middle7", new CardStackDefinition() { type = StackType.Middle | StackType.P1Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(-8, -33) } },
        { "Middle8", new CardStackDefinition() { type = StackType.Middle | StackType.P2Related, renderOffset = new Vector2(0, Y_OFFSET), position = new Vector2(8, -33) } },

        { "Game1", new CardStackDefinition() { type = StackType.Game | StackType.P1Related, renderOffset = new Vector2(-X_OFFSET, 0), position = new Vector2(-24, 33), isBottomPlayable = true } },
        { "Game2", new CardStackDefinition() { type = StackType.Game | StackType.P2Related, renderOffset = new Vector2(X_OFFSET, 0), position = new Vector2(24, 33), isBottomPlayable = true } },
        { "Game3", new CardStackDefinition() { type = StackType.Game | StackType.P1Related, renderOffset = new Vector2(-X_OFFSET, 0), position = new Vector2(-24, 11), isBottomPlayable = true } },
        { "Game4", new CardStackDefinition() { type = StackType.Game | StackType.P2Related, renderOffset = new Vector2(X_OFFSET, 0), position = new Vector2(24, 11), isBottomPlayable = true } },
        { "Game5", new CardStackDefinition() { type = StackType.Game | StackType.P1Related, renderOffset = new Vector2(-X_OFFSET, 0), position = new Vector2(-24, -11), isBottomPlayable = true } },
        { "Game6", new CardStackDefinition() { type = StackType.Game | StackType.P2Related, renderOffset = new Vector2(X_OFFSET, 0), position = new Vector2(24, -11), isBottomPlayable = true } },
        { "Game7", new CardStackDefinition() { type = StackType.Game | StackType.P1Related, renderOffset = new Vector2(-X_OFFSET, 0), position = new Vector2(-24, -33), isBottomPlayable = true } },
        { "Game8", new CardStackDefinition() { type = StackType.Game | StackType.P2Related, renderOffset = new Vector2(X_OFFSET, 0), position = new Vector2(24, -33), isBottomPlayable = true } },
    };
}