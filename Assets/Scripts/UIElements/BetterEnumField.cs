﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Unity.Properties;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.UIElements
{
    public class BetterEnumField : BaseField<Enum>
    {
        public new class UxmlFactory : UxmlFactory<BetterEnumField, UxmlTraits>
        {
        }

        public new class UxmlTraits : BaseField<Enum>.UxmlTraits
        {
            private UxmlTypeAttributeDescription<Enum> m_Type = EnumFieldHelpers.type;

            private UxmlStringAttributeDescription m_Value = EnumFieldHelpers.value;

            private UxmlBoolAttributeDescription m_IncludeObsoleteValues = EnumFieldHelpers.includeObsoleteValues;

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);
                if (EnumFieldHelpers.ExtractValue(bag, cc, out var resEnumType, out var resEnumValue, out var resIncludeObsoleteValues))
                {
                    BetterEnumField enumField = (BetterEnumField)ve;
                    enumField.Init(resEnumValue, resIncludeObsoleteValues);
                } else if (null != resEnumType)
                {
                    BetterEnumField enumField2 = (BetterEnumField)ve;
                    enumField2.m_EnumType = resEnumType;
                    if (enumField2.m_EnumType != null)
                    {
                        enumField2.PopulateDataFromType(enumField2.m_EnumType);
                    }

                    enumField2.value = null;
                } else
                {
                    BetterEnumField enumField3 = (BetterEnumField)ve;
                    enumField3.m_EnumType = null;
                    enumField3.value = null;
                }
            }
        }

        internal static readonly DataBindingProperty textProperty = "text";

        private Type m_EnumType;

        private bool m_IncludeObsoleteValues;

        private TextElement m_TextElement;

        private VisualElement m_ArrowElement;

        private EnumData m_EnumData;

        public new static readonly string ussClassName = "unity-enum-field";

        public static readonly string textUssClassName = ussClassName + "__text";

        public static readonly string arrowUssClassName = ussClassName + "__arrow";

        public new static readonly string labelUssClassName = ussClassName + "__label";

        public new static readonly string inputUssClassName = ussClassName + "__input";

        internal Type type => m_EnumType;

        internal bool includeObsoleteValues => m_IncludeObsoleteValues;

        [CreateProperty(ReadOnly = true)]
        public string text => m_TextElement.text;

        private void Initialize(Enum defaultValue)
        {
            m_TextElement = new TextElement();
            m_TextElement.AddToClassList(textUssClassName);
            m_TextElement.pickingMode = PickingMode.Ignore;
            visualInput.Add(m_TextElement);
            m_ArrowElement = new VisualElement();
            m_ArrowElement.AddToClassList(arrowUssClassName);
            m_ArrowElement.pickingMode = PickingMode.Ignore;
            visualInput.Add(m_ArrowElement);
            if (defaultValue != null)
            {
                Init(defaultValue);
            }
        }

        public BetterEnumField()
            : this(null, null)
        {
        }

        public BetterEnumField(Enum defaultValue)
            : this(null, defaultValue)
        {
        }

        public BetterEnumField(string label, Enum defaultValue = null)
            : base(label, null)
        {
            AddToClassList(ussClassName);
            labelElement.AddToClassList(labelUssClassName);
            visualInput.AddToClassList(inputUssClassName);
            Initialize(defaultValue);
            RegisterCallback<PointerDownEvent>(OnPointerDownEvent);
            RegisterCallback<PointerMoveEvent>(OnPointerMoveEvent);
            RegisterCallback(delegate (MouseDownEvent e)
            {
                if (e.button == 0)
                {
                    e.StopPropagation();
                }
            });
            RegisterCallback<NavigationSubmitEvent>(OnNavigationSubmit);
        }

        public void Init(Enum defaultValue)
        {
            Init(defaultValue, includeObsoleteValues: false);
        }

        public void Init(Enum defaultValue, bool includeObsoleteValues)
        {
            if (defaultValue == null)
            {
                throw new ArgumentNullException("defaultValue");
            }

            m_IncludeObsoleteValues = includeObsoleteValues;
            PopulateDataFromType(defaultValue.GetType());
            if (!Equals(rawValue, defaultValue))
            {
                SetValueWithoutNotify(defaultValue);
            } else
            {
                UpdateValueLabel(defaultValue);
            }
        }

        internal void PopulateDataFromType(Type enumType)
        {
            m_EnumType = enumType;
            m_EnumData = EnumDataUtility.GetCachedEnumData(m_EnumType, includeObsoleteValues ? EnumDataUtility.CachedType.IncludeObsoleteExceptErrors : EnumDataUtility.CachedType.ExcludeObsolete);
        }

        public override void SetValueWithoutNotify(Enum newValue)
        {
            if (!Equals(rawValue, newValue))
            {
                base.SetValueWithoutNotify(newValue);
                if (!(m_EnumType == null))
                {
                    UpdateValueLabel(newValue);
                }
            }
        }

        private void UpdateValueLabel(Enum value)
        {
            int num = Array.IndexOf(m_EnumData.values, value);
            if ((num >= 0) & (num < m_EnumData.values.Length))
            {
                m_TextElement.text = m_EnumData.displayNames[num];
            } else
            {
                m_TextElement.text = string.Empty;
            }
        }

        private void OnPointerDownEvent(PointerDownEvent evt)
        {
            ProcessPointerDown(evt);
        }

        private void OnPointerMoveEvent(PointerMoveEvent evt)
        {
            if (evt.button == 0 && ((uint)evt.pressedButtons & (true ? 1u : 0u)) != 0)
            {
                ProcessPointerDown(evt);
            }
        }

        private bool ContainsPointer(int pointerId)
        {
            VisualElement topElementUnderPointer = elementPanel.GetTopElementUnderPointer(pointerId);
            return this == topElementUnderPointer || visualInput == topElementUnderPointer;
        }

        private void ProcessPointerDown<T>(PointerEventBase<T> evt) where T : PointerEventBase<T>, new()
        {
            if (evt.button == 0 && ContainsPointer(evt.pointerId))
            {
                ShowMenu();
                evt.StopPropagation();
            }
        }

        private void OnNavigationSubmit(NavigationSubmitEvent evt)
        {
            ShowMenu();
            evt.StopPropagation();
        }

        GenericDropdownMenu genericMenu;

        public void ShowMenu()
        {
            if (m_EnumType == null)
            {
                return;
            }

            genericMenu = new GenericDropdownMenu();
            
            int num = Array.IndexOf(m_EnumData.values, value);
            for (int i = 0; i < m_EnumData.values.Length; i++)
            {
                bool isChecked = num == i;
                Enum value = m_EnumData.values[i];
                if (disabledOptions.Contains(value))
                {
                    genericMenu.AddDisabledItem(m_EnumData.displayNames[i], isChecked);
                }
                else
                {
                    genericMenu.AddItem(m_EnumData.displayNames[i], isChecked, delegate (object contentView)
                    {
                        ChangeValueFromMenu(contentView);
                    }, value);
                }
            }

            genericMenu.DropDown(visualInput.worldBound, this, anchored: true);
        }

        private List<Enum> disabledOptions = new();

        public void SetItemEnabled(Enum value, bool enabled)
        {
            if (enabled)
            {
                if (disabledOptions.Contains(value))
                {
                    disabledOptions.Remove(value);
                }
            }
            else
            {
                if (!disabledOptions.Contains(value))
                {
                    disabledOptions.Add(value);
                }
            }

            // refresh opened dropdown ?
            if (genericMenu != null)
            {
                FieldInfo fi = typeof(GenericDropdownMenu).GetField("m_TargetElement", BindingFlags.NonPublic | BindingFlags.Instance);
                if (fi.GetValue(genericMenu) != null)
                {
                    MethodInfo mi = typeof(GenericDropdownMenu).GetMethod("Hide", BindingFlags.NonPublic | BindingFlags.Instance);
                    mi.Invoke(genericMenu, new object[] { false }); // close old menu
                    ShowMenu();
                }
            }
        }

        private void ChangeValueFromMenu(object menuItem)
        {
            value = menuItem as Enum;
        }

        protected override void UpdateMixedValueContent()
        {
            if (showMixedValue)
            {
                m_TextElement.text = mixedValueString;
            }

            m_TextElement.EnableInClassList(labelUssClassName, showMixedValue);
            m_TextElement.EnableInClassList(mixedValueLabelUssClassName, showMixedValue);
        }
    }
}
